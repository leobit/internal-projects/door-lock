//!*****************************************************************************
//!   @file    buzzer_drv.c
//!   @author  Stages Cycling
//!   @version 1.1.0
//!   @date    22/03/2018
//!   @brief   This C source file for module that controls the hardware buzzer using the PWM hardware peripheral.
//!
//!   @copyright  Copyright (C) Stages Cycling. All rights reserved.
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  22/03/2018 | Dmytro Kryvyi    | New features implementation.
//!  04/04/2018 | Dmytro Kryvyi    | Doxygen style and code updated
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "buzzer_drv.h"
#include "bspConfig.h"
#include "nrf_drv_pwm.h"
#include "app_util_platform.h"

/* Private define section 0 --------------------------------------------------*/
/** PWM souurce frequency */
#define NRF_PWM_FREQ_HZ               (125000u) /* The PWM source clock speed */

/* Private typedef -----------------------------------------------------------*/
/* Private define section 1 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Macros for generating the tone frequency */
#define BUZZER_PWM_MAX_COUTER(frequency)      (NRF_PWM_FREQ_HZ / frequency)
#define BUZZER_PWM_DUTY(frequency)            (BUZZER_PWM_MAX_COUTER(frequency)  / 2u)

/* Private variables ---------------------------------------------------------*/

/* PWM instance */
static const nrf_drv_pwm_t m_pwm_inst = NRF_DRV_PWM_INSTANCE(0);

/** Waveform for HIGH tone. Note: MUST BE LOCATED IN RAM. */
static nrf_pwm_values_wave_form_t seq_value_h[] =
{
  {
    BUZZER_PWM_DUTY(5000u),
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(5000u)
  },
  { /* Last sequence unused */
    0u,
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(5000u)
  }
};

/** Sequence for HIGH tone */
static nrf_pwm_sequence_t const seq_h =
{
  .values.p_wave_form = seq_value_h,
  .length          = NRF_PWM_VALUES_LENGTH(seq_value_h),
  .repeats         = 128u,
  .end_delay       = 20u
};

/** Waveform for LOW tone. Note: MUST BE LOCATED IN RAM. */
static nrf_pwm_values_wave_form_t seq_value_l[] =
{
  {
    BUZZER_PWM_DUTY(2500u),
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(2500u)
  },
  { /* Last sequence unused */
    0u,
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(2500u)
  }
};

/** Sequence for LOW tone */
static nrf_pwm_sequence_t const seq_l =
{
  .values.p_wave_form = seq_value_l,
  .length          = NRF_PWM_VALUES_LENGTH(seq_value_l),
  .repeats         = 64u,
  .end_delay       = 10u
};

/** Waveform for PDA notify tone. Note: MUST BE LOCATED IN RAM. */
static nrf_pwm_values_wave_form_t seq_value_pda[] =
{
  {
    BUZZER_PWM_DUTY(2500u),
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(2500u)
  },
  {
    BUZZER_PWM_DUTY(3500u),
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(3500u)
  },
  {
    BUZZER_PWM_DUTY(4500u),
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(4500u)
  },
  {
    BUZZER_PWM_DUTY(2500u),
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(2500u)
  },
  {
    BUZZER_PWM_DUTY(1500u),
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(1500u)
  },
  {
    0u,
    0u,
    0u,
    BUZZER_PWM_MAX_COUTER(2500u)
  }
};

/* Sequence for PDA notify tone */
static nrf_pwm_sequence_t const seq_pda =
{
  .values.p_wave_form = seq_value_pda,
  .length          = NRF_PWM_VALUES_LENGTH(seq_value_pda),
  .repeats         = 80u,
  .end_delay       = 20u
};
/* Private functions ---------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**@brief Initialize the hardware buzzer.
 *
 * @param  None.
 *
 * @retval NRF_SUCCESS              Initialization successful.
 * @retval NRF_ERROR_INVALID_STATE  If the driver was already initialized.
 */
ret_code_t bsp_buzzer_init(void)
{
  /* Static and const PWM config */
  static const nrf_drv_pwm_config_t config =
  {
    .output_pins =
    {
      BSP_PIN_BUZZER, // Buzzer pin assigned to channel 0
      NRF_DRV_PWM_PIN_NOT_USED,
      NRF_DRV_PWM_PIN_NOT_USED,
      NRF_DRV_PWM_PIN_NOT_USED
    },
    .irq_priority = APP_IRQ_PRIORITY_LOW,
    .base_clock   = NRF_PWM_CLK_125kHz,
    .count_mode   = NRF_PWM_MODE_UP,
    .top_value    = 0u,
    .load_mode    = NRF_PWM_LOAD_WAVE_FORM,
    .step_mode    = NRF_PWM_STEP_AUTO
  };

  /* Configure the PWM peripheral */
  return nrf_drv_pwm_init(&m_pwm_inst, &config, NULL);
}

/**@brief Stop and uninitialize the hardware buzzer.
 *
 * @param  None.
 *
 * @retval None.
 */
void bsp_buzzer_uninit(void)
{
  nrf_drv_pwm_stop(&m_pwm_inst, true);
  nrf_drv_pwm_uninit(&m_pwm_inst);
}

/**@brief Starts plaing the tone.
 *
 * @param[in] tone - specified tone @ref buzzer_tone_t.
 *
 * @retval None.
 */
void bsp_buzzer_play_tone(buzzer_tone_t tone)
{
  switch(tone)
  {
  case BUZZ_TONE_CONFIRM:
    {
      /* Confirm/Success: low-high */
      (void)nrf_drv_pwm_complex_playback(&m_pwm_inst, &seq_l, &seq_h, 1u, NRF_DRV_PWM_FLAG_STOP);
      break;
    }
  case BUZZ_TONE_CANCEL:
    {
      /* Cancel/Dismiss/Error: high-low */
      (void)nrf_drv_pwm_complex_playback(&m_pwm_inst, &seq_h, &seq_l, 1u, NRF_DRV_PWM_FLAG_STOP);
      break;
    }
  case BUZZ_TONE_NOTIFY:
    {
      /* Notification: high-high */
      (void)nrf_drv_pwm_simple_playback(&m_pwm_inst, &seq_h, 2u, NRF_DRV_PWM_FLAG_STOP);
      break;
    }
  case BUZZ_TONE_LAP:
    {
      /* Lap: low-low */
      (void)nrf_drv_pwm_simple_playback(&m_pwm_inst, &seq_l, 2u, NRF_DRV_PWM_FLAG_STOP);
      break;
    }
  case BUZZ_TONE_NOTIFY_PDA:
    {
      /* Notification: custom PDA */
      (void)nrf_drv_pwm_simple_playback(&m_pwm_inst, &seq_pda, 1u, NRF_DRV_PWM_FLAG_STOP);
      break;
    }
  case BUZZ_TONE_BTNPRESS:
  default:
    {
      /* Button press: low */
      (void)nrf_drv_pwm_simple_playback(&m_pwm_inst, &seq_l, 1u, NRF_DRV_PWM_FLAG_STOP);
      break;
    }
  }
}

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
