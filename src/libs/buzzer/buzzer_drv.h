//!*****************************************************************************
//!   @file    buzzer_drv.h
//!   @author  Stages Cycling
//!   @version 1.1.0
//!   @date    22/03/2018
//!   @brief   This C header file for module that controls the hardware buzzer using the PWM hardware peripheral.
//!
//!   @copyright  Copyright (C) Stages Cycling. All rights reserved.
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  22/03/2018 | Dmytro Kryvyi    | New features implementation.
//!  04/04/2018 | Dmytro Kryvyi    | Doxygen style and code updated
//!
//!*****************************************************************************

#ifndef _BUZZER_DRV_H_
#define _BUZZER_DRV_H_

/* Includes ------------------------------------------------------------------*/
#include "sdk_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/

/* Enumeration with available tones for playing */
typedef enum
{
  BUZZ_TONE_CONFIRM,    /* Confirm/Success: low-high */
  BUZZ_TONE_CANCEL,     /* Cancel/Dismiss/Error: high-low */
  BUZZ_TONE_NOTIFY,     /* Notification: high-high */
  BUZZ_TONE_LAP,        /* Lap: low-low */
  BUZZ_TONE_NOTIFY_PDA, /* Notification: custom PDA */
  BUZZ_TONE_BTNPRESS    /* Button press: low */
} buzzer_tone_t;

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/* Basic functions */
ret_code_t bsp_buzzer_init(void);
void bsp_buzzer_uninit(void);

/* Player function */
void bsp_buzzer_play_tone(buzzer_tone_t tone);

#ifdef __cplusplus
}
#endif

#endif /* _BUZZER_DRV_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
