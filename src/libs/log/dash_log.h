//!*****************************************************************************
//!   @file    dash_log.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    11/04/2018
//!   @brief   This is C header file for leo log implementation.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  11/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#ifndef _LEO_LOG_H_
#define _LEO_LOG_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf_log.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/* Basic functions */
void dl_initialize(void);

#ifdef DEBUG
void dl_cpu_usage(void);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _LEO_LOG_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
