//!*****************************************************************************
//!   @file    dash_log.c
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    11/04/2018
//!   @brief   This is C source file for leo log implementation.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  11/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "dash_log.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "nrf_sdm.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "cpu_utils.h"

#include "timems.h"

#ifdef DEBUG
#include "oled_display.h"
#include "easy_gl.h"
#include "nrf_delay.h"
#endif

/* Private define section 0 --------------------------------------------------*/

/* The name of this module for debugger */
#define MODULE_NAME "LOG"

/** Value used as error code on stack dump, can be used to identify stack location on stack unwind. */
#define DEAD_BEEF                           0xDEADBEEF

/* Private typedef -----------------------------------------------------------*/
/* Private define section 1 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/** Definition of Logger thread. */
static TaskHandle_t m_logger_thread;

/* Private functions prototypes ----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**@brief Callback function for asserts in the SoftDevice.
*
* @details This function will be called in case of an assert in the SoftDevice.
*
* @warning This handler is an example only and does not fit a final product. You need to analyze
*          how your product is supposed to react in case of Assert.
* @warning On assert from the SoftDevice, the system can only recover on reset.
*
* @param[in]   line_num   Line number of the failing ASSERT call.
* @param[in]   file_name  File name of the failing ASSERT call.
*/
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
  app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for initializing the nrf log module.
*/
static void log_init(void)
{
  ret_code_t err_code = NRF_LOG_INIT(NULL);
  APP_ERROR_CHECK(err_code);

  NRF_LOG_DEFAULT_BACKENDS_INIT();
}

/**@brief Thread for handling the logger.
*
* @details This thread is responsible for processing log entries if logs are deferred.
*          Thread flushes all log entries and suspends. It is resumed by idle task hook.
*
* @param[in]   arg   Pointer used for passing some arbitrary information (context) from the
*                    osThreadCreate() call to the thread.
*/
static void logger_thread(void * arg)
{
  UNUSED_PARAMETER(arg);
  uint32_t i = timems_get_systemup_time();

  while (true)
  {
    NRF_LOG_FLUSH();
    vTaskDelay(1);
    if ((i + 1000) < timems_get_systemup_time())
    {
      i = timems_get_systemup_time();
      dl_cpu_usage();
    }
  }
}

/**@brief A function which is hooked to idle task.
* @note Idle hook must be enabled in FreeRTOS configuration (configUSE_IDLE_HOOK).
*/
void vApplicationIdleHook( void )
{
  vApplicationIdleHook_cpu();
}

/* Exported functions ------------------------------------------------------- */

/**
 *@brief Log initialize function
 */
void dl_initialize(void)
{
#ifdef DEBUG
  /* Initialize the OLED display */
  oled_ioInit();
  easyGL_initialize(&font6x8);
  easyGL_setOrientation(DEVICE_ORIENT_LANDSCAPE_TOP);

  easyGL_clearFrame();
  easyGL_print("Dash 2", 20, 24, &font14x20);
  easyGL_updateDisplay();

  oled_setvert_scrol(0u, 12u);
  oled_setscrollleft(0u, 127u, OLED_SCROL_2FRAME);
  oled_startscroll();
#endif
  /* Initialize the logger peripheral and thread */
  log_init();

  // Create logger thread
  if (pdPASS != xTaskCreate(logger_thread, "LOGGER", 256, NULL, 1u, &m_logger_thread))
  {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }
}

#ifdef DEBUG
/**
 *@brief Display CPU usage on OLED
 */
void dl_cpu_usage(void)
{
  static uint8_t load_array[128];
  static bool first_call = true;

  if (first_call == true)
  {
    oled_stopscroll();
    memset(load_array, easyGL_getHeight() - 1u, sizeof(load_array));
    first_call = false;
  }

  uint16_t cpu_usage = osGetCPUUsage();
  memcpy(load_array, (load_array + 1u), sizeof(load_array) - 1u);
  load_array[sizeof(load_array) - 1u] = easyGL_getHeight() - ((cpu_usage * 41) / 100u) - 1;

  easyGL_clearFrame();
  static char arr[32];
  sprintf(arr, "CPU: %3d%% UP: %d", cpu_usage, timems_get_systemup_time() / 1000u);
  easyGL_print(arr, 0, 0, &font6x8);

  sprintf(arr, "MEM: %5d of %5d", configTOTAL_HEAP_SIZE - xPortGetFreeHeapSize(), configTOTAL_HEAP_SIZE);
  easyGL_print(arr, 0, 9, &font6x8);

  easyGL_drawHLine(0, easyGL_getHeight() - 44u, easyGL_getWidth(), 2);
  for (uint8_t x = 0; x < (sizeof(load_array) - 1); x++)
  {
    easyGL_drawLine(x, load_array[x], x + 1u, load_array[x + 1u], 1);
  }
  easyGL_updateDisplay();
}

/**
 * Function is custom application error handler when needed
 */
void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
#ifndef DEBUG
  bsp_buzzer_init();
  bsp_buzzer_play_tone(BUZZ_TONE_CANCEL);
  oled_stopscroll();
  easyGL_clearFrame();
  easyGL_print("Fatal ERROR!", 0, 28, &font11x16);
  easyGL_updateDisplay();
  nrf_delay_ms(1000u);

  NRF_LOG_FINAL_FLUSH();
  NRF_LOG_ERROR("Fatal error");
#else
  switch (id)
  {
#if defined(SOFTDEVICE_PRESENT) && SOFTDEVICE_PRESENT
  case NRF_FAULT_ID_SD_ASSERT:
    NRF_LOG_ERROR("SOFTDEVICE: ASSERTION FAILED");
    break;
  case NRF_FAULT_ID_APP_MEMACC:
    NRF_LOG_ERROR("SOFTDEVICE: INVALID MEMORY ACCESS");
    break;
#endif
  case NRF_FAULT_ID_SDK_ASSERT:
    {
      assert_info_t * p_info = (assert_info_t *)info;
      NRF_LOG_ERROR("ASSERTION FAILED at %s:%u",
                    p_info->p_file_name,
                    p_info->line_num);
      break;
    }
  case NRF_FAULT_ID_SDK_ERROR:
    {
      error_info_t * p_info = (error_info_t *)info;
      NRF_LOG_ERROR("ERROR %u [%s] at %s:%u",
                    p_info->err_code,
                    nrf_strerror_get(p_info->err_code),
                    p_info->p_file_name,
                    p_info->line_num);
      /* Prints on the OLED display */
      easyGL_clearFrame();
      char string[128];
      sprintf(string,"Fatal ERROR #%du!",p_info->err_code);
      easyGL_print(string, 2u, 0u, &font7x12);

      sprintf(string,"[%s]\n\r Line: %d \n\r%s",
              nrf_strerror_get(p_info->err_code),
              p_info->line_num, p_info->p_file_name);

      easyGL_print(string, 0u, 13u, &font6x8);
      oled_stopscroll();
      easyGL_updateDisplay();
      break;
    }
  default:
    NRF_LOG_ERROR("UNKNOWN FAULT at 0x%08X", pc);
    break;
  }
#endif

  NRF_LOG_FLUSH();

  NRF_BREAKPOINT_COND;
  // On assert, the system can only recover with a reset.

#ifndef DEBUG
  NRF_LOG_WARNING("System reset");
  NVIC_SystemReset();
#else
  app_error_save_and_stop(id, pc, info);
#endif // DEBUG
}
#endif

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
