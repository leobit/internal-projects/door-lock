//!*****************************************************************************
//!   @file    osa_layer.c
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    06/03/2018
//!   @brief   This is C source file of OS abstraction layer library
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  06/03/2018 | Dmytro Kryvyi    | Initial draft
//!  13/03/2018 | Dmytro Kryvyi    | Beta
//!  16/03/2018 | Dmytro Kryvyi    | Code refactoring
//!  04/04/2018 | Dmytro Kryvyi    | Doxygen style and code updated
//!  06/04/2018 | Dmytro Kryvyi    | New features, heap manager outside
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "osa_layer.h"
#include <string.h>

#ifdef FREERTOS
/* For FREERTOS only */
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "osa_layer_freertos_config.h"

#else

/* For Linux OS */
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include "osa_layer_linux_config.h"

#endif  // FREERTOS

/* Private define section 0 --------------------------------------------------*/

#if (ENABLE_OSAL_LOG == 1u)

/* The name of this module for debugger */
#define MODULE_NAME "OSAL"

#else
#undef UTIL_DBG_INFO
#define UTIL_DBG_INFO(...)
#undef UTIL_DBG_WARN
#define UTIL_DBG_WARN(...)
#undef UTIL_DBG_FLUSH
#define UTIL_DBG_FLUSH()
#endif

/* Private typedef -----------------------------------------------------------*/
#ifdef FREERTOS
/* For FREERTOS only */

/** OS abstraction thread handleInstance */
typedef struct
{
  osa_threadArg_t threadArgs;     ///< Passed arg to the thread.
} osa_threadInstance_t;

#else
/* For Linux OS */

/** OS abstraction thread argument typedef */
typedef struct
{
  pthread_t pThreadInstance;		///< Thread handle.
  pthread_attr_t attr;				///< Thread attributes.
  osa_threadArg_t threadArgs;       ///< Passed arg to the thread.
} osa_threadInstance_t;

#endif  // FREERTOS

/* Queues settings */
#if (USE_FREERTOS_Q != 0u)
/* Use FREERTOS queue implementation */
#include "queue.h"

/** OSA queue node typedef */
typedef struct
{
#ifndef DISABLE_FREERTOS_Q_PRIO
  uint32_t prio;                ///< Msg prio.
#endif
  size_t datSize;               ///< Msg data size.
  uint8_t datArray[];           ///< Data.
} osa_qNode;

/** FreeRTOS based OSA queue structure typedef without prio. */
typedef struct
{
  QueueHandle_t q_handle;         ///< Queue handle.
  osa_QAttr q_attr;               ///< Queue attribute.
  const char * q_name;            ///< Queue name pointer.
} osa_queue_struct_t;

#else
/* Use OSA queue implementation */

/** OSA queue node typedef */
typedef struct
{
  void * nextNode;               ///< Next node pointer.
  uint32_t prio;                 ///< Msg prio, just for receiver.
  size_t datSize;                ///< Data size.
  uint8_t datArray[];            ///< Data array.
} osa_qNode;

/** OSA queue structure typedef */
typedef struct
{
  osa_qNode * nodeForSend;     ///< Pointer to last node that will be used to as prev to send the data.
  osa_qNode * nodeForRecv;     ///< Pointer to last node that will be used to receive data.
  osa_MHandle_t qAccessMux;    ///< Mutex to thread safe implementation.
  osa_MHandle_t qDataMux;      ///< Mutex for more energy eficient MCU usage.
  osa_QAttr q_attr;            ///< Queue attributes.
  const char * q_name;         ///< Queue name pointer.
} osa_queue_struct_t;

#endif  // (USE_FREERTOS_Q != 0)

/* Private define section 1 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/** CRC8 table for fast CRC8 */
static uint8_t crc_table[256u];

/* Private function prototypes -----------------------------------------------*/

#ifdef FREERTOS
/* For FREERTOS only */

static uint32_t inHandlerMode(void);

#else
/* For Linux OS */

#endif  // FREERTOS

/* Private functions ---------------------------------------------------------*/

#ifdef FREERTOS
/* For FREERTOS only */

/**
 * @brief  Determine whether we are in thread mode or handler mode.
 *
 * @param  None.
 *
 * @retval true if MCU in handler mode (ISR).
 */
static uint32_t inHandlerMode(void)
{
  return __get_IPSR();
}

#else
/* For Linux OS */

#endif  // FREERTOS

/**
 * @brief	Function for generating the CRC8 table.
 *
 * @note Attribute constructor used, but some compilers may skip this!
 * @param	None.
 *
 * @retval	None.
 */
__attribute__((constructor)) void osa_uCRC8_initialize(void)
{
  const uint8_t poly = CRC8_POLYNOM;
  const uint8_t msbit = 0x80u;
  crc_table[0u] = 0u;
  register uint8_t t = msbit;
  for (uint32_t i = 1u; i < sizeof(crc_table); i *= 2u)
  {
    t = (t << 1u) ^ (t & msbit ? poly : 0u);
    for (uint32_t j = 0u; j < i; ++j)
    {
      crc_table[i + j] = crc_table[j] ^ t;
    }
  }
}

/* Exported functions ------------------------------------------------------- */

/**
 * @brief	Function for calculating crc8.
 *
 * @param[in]	pData	- pointer to data that will be calculated in CRC function.
 * @param[in]	length -  size of data in bytes.
 *
 * @retval	uint8_t crc8 value.
 */
uint8_t osa_uCRC8(uint8_t * pData, size_t length)
{
  register uint8_t _crc = 0u;
  while (length--)
  {
    _crc = crc_table[(_crc ^ *pData++) & 0xFFu];
  }
  return _crc;
}

/**
 * @brief	Function for creating the generic osa queues.
 *
 * @param[out]	pQueueHandle - pointer to static queue handle.
 * @param[in]	name - the name of queue.
 * @param[in]	pQ_attrs - the queue's attributes.
 *
 * @retval	RET_CODE_ERROR_NO_MEM	If no available space in HEAP.
 * @retval	RET_CODE_ERROR_NULL		If queue handle or queue attrs pointed to NULL.
 */
ret_code_t osa_Qcreate(osa_QHandle_t * pQueueHandle, const char * name, osa_QAttr * pQ_attrs)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if ((pQ_attrs != (osa_QAttr *) NULL) && (pQueueHandle != (osa_QHandle_t * ) NULL))
  {
    /* Set retcode and queue handler */
    ret_code = RET_CODE_ERROR_NO_MEM;
    *pQueueHandle = (osa_QHandle_t) NULL;
    osa_queue_struct_t * pQueue = (osa_queue_struct_t * ) NULL;
    MALLOC(osa_queue_struct_t, pQueue, sizeof(osa_queue_struct_t));

    if (pQueue != (osa_queue_struct_t * ) NULL)
    {
      if (name != (const char *) NULL)
      {
        pQueue->q_name = name;
      }
      else
      {
        pQueue->q_name = "Generic queue";
      }

      pQueue->q_attr = *pQ_attrs;
      pQueue->q_attr.q_msgs_avail = 0u;
      pQueue->q_attr.q_msgs_free_nodes = pQ_attrs->q_max_hold_msgs_num;

#if (USE_FREERTOS_Q != 0u)
/* Use FREERTOS queue implementation */

      // Create a queue capable of containing pQ_attrs->q_max_hold_msgs_num of sizeof( void * ).
      pQueue->q_handle = xQueueCreate(pQ_attrs->q_max_hold_msgs_num,	// The number of items the queue can hold.
                                      sizeof( void * ));	// The size of each item in the queue
      if (pQueue->q_handle == (QueueHandle_t) NULL)
      {
        UTIL_DBG_WARN("Failed to create queue!");
        FREE(osa_queue_struct_t, pQueue);
      }
      else
      {
        ret_code = RET_CODE_SUCCESS;
        *pQueueHandle = (osa_QHandle_t) pQueue;

#ifdef DEBUG
#if (configQUEUE_REGISTRY_SIZE > 0u)
        vQueueAddToRegistry(pQueue->q_handle, name); /* Add queue to be available for debugger */
#endif // (configQUEUE_REGISTRY_SIZE > 0u)
#endif // DEBUG
      }
#else
/* OSA queue implementation */
      pQueue->nodeForRecv = (osa_qNode *) NULL;
      pQueue->nodeForSend = (osa_qNode *) NULL;

      /* Create access mutex for this queue*/
      if (osa_Mcreate(&pQueue->qAccessMux, false) != RET_CODE_SUCCESS)
      {
        UTIL_DBG_WARN("Failed to create queue!");
        FREE(osa_queue_struct_t, pQueue);
      }
      else
      {
        /* Create access mutex for more efficient energy usage */
        if (osa_Mcreate(&pQueue->qDataMux, true) != RET_CODE_SUCCESS)
        {
          UTIL_DBG_WARN("Failed to create queue!");
          osa_Mdelete(&pQueue->qAccessMux);
          FREE(osa_queue_struct_t, pQueue);
        }
        else
        {
          ret_code = RET_CODE_SUCCESS;
          *pQueueHandle = (osa_QHandle_t) pQueue;
        }
      }
#endif  // FREERTOS
    }
  }
  return ret_code;
}

/**
 * @brief	Function for deleting the generic osa queue and taken HEAP.
 *
 * @param[out]	pQueueHandle - pointer to queue handle.
 *
 * @retval	RET_CODE_ERROR_NULL if null pointer to queue.
 * @retval	RET_CODE_ERROR_TIMEOUT If failed to take the mutex.
 */
ret_code_t osa_Qdelete(osa_QHandle_t * pQueueHandle)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (pQueueHandle != (osa_QHandle_t *) NULL)
  {
    if (*pQueueHandle != (osa_QHandle_t) NULL)
    {
      osa_queue_struct_t * pQueue = (osa_queue_struct_t *) (*pQueueHandle);

#if (USE_FREERTOS_Q != 0u)
/* Use FREERTOS queue implementation */

      /* Checks the size of buffer that will be used for writing */
      while (uxQueueMessagesWaiting(pQueue->q_handle) != 0u)
      {
        osa_qNode * pDataNode = (osa_qNode *) NULL;
        if(inHandlerMode() != 0)
        {
          portBASE_TYPE taskWoken = pdFALSE;
          xQueueReceiveFromISR(pQueue->q_handle, (void *)&pDataNode, &taskWoken);
          portEND_SWITCHING_ISR(taskWoken);
        }
        else
        {
          xQueueReceive(pQueue->q_handle, (void *)&pDataNode, 1u);
        }

        if (pDataNode != (osa_qNode *) NULL)
        {
          /* Free taken memory */
          FREE(osa_qNode, pDataNode);
        }
        else
        {
          UTIL_DBG_WARN("Warning at cleaning!");
        }
      }

#ifdef DEBUG
#if (configQUEUE_REGISTRY_SIZE > 0u)
      vQueueUnregisterQueue(pQueue->q_handle); /* Add queue to be available for debugger */
#endif // (configQUEUE_REGISTRY_SIZE > 0u)
#endif // DEBUG
      /* Delete the queue */
      vQueueDelete(pQueue->q_handle);
      ret_code = RET_CODE_SUCCESS;
#else
/* OSA queue implementation */
      if(osa_Mlock(pQueue->qAccessMux, OSA_INFINITE_WAIT) == RET_CODE_SUCCESS)
      {
        while (pQueue->q_attr.q_msgs_avail != 0u)
        {
          if(pQueue->nodeForRecv != (osa_qNode *) NULL)
          {
            /* Remember the next node */
            osa_qNode * nextNode = (osa_qNode *) pQueue->nodeForRecv->nextNode;

            FREE(osa_qNode, pQueue->nodeForRecv);

            /* LINK NODES: if node for receiving eq to node for transmit (One element in queue that we read) */
            if(pQueue->nodeForRecv == pQueue->nodeForSend)
            {
              pQueue->nodeForRecv = pQueue->nodeForSend = (osa_qNode *) NULL;
            }
            else
            {
              /* Set node that we remember as next */
              pQueue->nodeForRecv = nextNode;
            }

            pQueue->q_attr.q_msgs_avail--;
            pQueue->q_attr.q_msgs_free_nodes++;
          }
          else
          {
            /* Node for receiving must never contain NULL pointer at this state */
            VALIDATE_CRITIC((pQueue->nodeForRecv == (osa_qNode *) NULL), RET_CODE_ERROR_INVALID_DATA, "NULL recv node, queue invalid!");
          }
        }
        osa_Munlock(pQueue->qAccessMux);
        osa_Mdelete(&pQueue->qAccessMux);
        osa_Mdelete(&pQueue->qDataMux);
        ret_code = RET_CODE_SUCCESS;
      }
      else
      {
        ret_code = RET_CODE_ERROR_TIMEOUT;
        UTIL_DBG_WARN("Failed to take the mutex!");
      }
#endif  // (USE_FREERTOS_Q != 0u)

      /* If queue elenent has been deleted - delete queue */
      if (ret_code == RET_CODE_SUCCESS)
      {
        /* Free taken memory and set NULL pointer*/
        FREE(osa_queue_struct_t, pQueue);
        *pQueueHandle = (osa_QHandle_t) NULL;
      }
    }
  }
  return ret_code;
}

/**
 * @brief	Function for get state of current queue.
 *
 * @param[in]	queueHandle - queue handle.
 *
 * @retval	osa_QAttr structure.
 */
osa_QAttr osa_QgetState(osa_QHandle_t queueHandle)
{
  osa_QAttr retv_attr;
  retv_attr.q_msgs_avail = 0u;
  retv_attr.q_max_hold_msgs_num = 0u;
  retv_attr.q_max_msg_size = 0u;
  retv_attr.q_msgs_free_nodes = 0u;

  if (queueHandle != (osa_QHandle_t) NULL)
  {
    osa_queue_struct_t * pQueue = (osa_queue_struct_t *) queueHandle;
#if (USE_FREERTOS_Q != 0u)
/* Use FREERTOS queue implementation */
    retv_attr.q_max_hold_msgs_num = pQueue->q_attr.q_max_hold_msgs_num;
    retv_attr.q_max_msg_size = pQueue->q_attr.q_max_msg_size;
    retv_attr.q_msgs_avail = uxQueueMessagesWaiting(pQueue->q_handle);
    retv_attr.q_msgs_free_nodes = uxQueueSpacesAvailable(pQueue->q_handle);
    retv_attr.q_name = pQueue->q_name;
#else
/* OSA queue implementation */
    if(osa_Mlock(pQueue->qAccessMux, OSA_INFINITE_WAIT) == RET_CODE_SUCCESS)
    {
      retv_attr = pQueue->q_attr;
      osa_Munlock(pQueue->qAccessMux);
    }
#endif  // USE_FREERTOS_Q
  }
  return retv_attr;
}

/**
 *  @brief	Function for send message to queue.
 *
 * @param[in]	queueHandle - queue handle.
 * @param[in]	pMsg - pointer to message.
 * @param[in]	msg_len - message length.
 * @param[in]	msg_prio - message priority.
 * @param[in]	timeout - the max timeout for waiting for push the message.
 *
 * @retval	RET_CODE_ERROR_NULL if null pointer to queue.
 * @retval	RET_CODE_ERROR_NO_MEM if no memory in HEAP.
 * @retval	RET_CODE_ERROR_TIMEOUT	If data not enqueued to the FIFO due to timeout.
 * @retval	RET_CODE_ERROR_INVALID_LENGTH	If data length bigger than queue max element size.
 *
 */
ret_code_t osa_Qsend(osa_QHandle_t queueHandle, const void * pMsg, size_t msg_len, uint32_t msg_prio, uint32_t timeout)
{
  ret_code_t ret_code = RET_CODE_ERROR_NO_MEM;

  if (queueHandle != (osa_QHandle_t) NULL)
  {
    osa_queue_struct_t * pQueue = (osa_queue_struct_t *) queueHandle;

    /* Check the size */
    if (pQueue->q_attr.q_max_msg_size >= msg_len)
    {
#if (USE_FREERTOS_Q != 0u)
      /* Use FREERTOS queue implementation */

      osa_qNode * pDataNode = (osa_qNode *) NULL;
      MALLOC(osa_qNode, pDataNode, sizeof(osa_qNode) + msg_len);
      if (pDataNode != (osa_qNode *) NULL)
      {

#if (USE_FREERTOS_Q_PRIO != 0u)
        pDataNode->prio = msg_prio;
#else
        /* Prevent compiler warnings */
        (void) msg_prio;
#endif // (USE_FREERTOS_Q_PRIO != 0u)
        pDataNode->datSize = msg_len;

        /* Memory allocated - copy */
        memcpy(pDataNode->datArray, pMsg, msg_len);

        /* PUSH to queue */
        if(inHandlerMode() != 0)
        {
          portBASE_TYPE taskWoken = pdFALSE;
          if (xQueueSendFromISR(pQueue->q_handle, &pDataNode, &taskWoken) != pdTRUE)
          {
            ret_code = RET_CODE_ERROR_NO_MEM;
            FREE(osa_qNode, pDataNode);
            UTIL_DBG_WARN("Can't enque element!");
          }
          else
          {
            portEND_SWITCHING_ISR(taskWoken);
          }
        }
        else
        {
          TickType_t ticks = ((timeout == OSA_INFINITE_WAIT) ? portMAX_DELAY : ((TickType_t)timeout / portTICK_PERIOD_MS));
          if (xQueueSend(pQueue->q_handle, &pDataNode, ticks) != pdTRUE)
          {
            ret_code = RET_CODE_ERROR_TIMEOUT;
            FREE(osa_qNode, pDataNode);
            UTIL_DBG_WARN("Can't enque element!");
          }
          else
          {
            ret_code = RET_CODE_SUCCESS;
          }
        }
      }
#else
      /* OSA queue implementation */
      do
      {
        if(osa_Mlock(pQueue->qAccessMux, DEFAULT_MUX_ACCESS_TIME_MS) == RET_CODE_SUCCESS)
        {
          if (pQueue->q_attr.q_msgs_avail < pQueue->q_attr.q_max_hold_msgs_num)
          {
            osa_qNode * newNode = (osa_qNode *) NULL;
            MALLOC(osa_qNode, newNode, sizeof(osa_qNode) + msg_len);
            if (newNode != (osa_qNode *) NULL)
            {
              /* Memory allocated - copy and add link*/
              newNode->nextNode = (void *) NULL;
              newNode->prio = msg_prio;
              newNode->datSize = msg_len;
              memcpy(newNode->datArray, pMsg, msg_len);

              /* Link nodes */
              if (pQueue->nodeForSend == (osa_qNode *) NULL)
              {
                if (pQueue->nodeForRecv == (osa_qNode *) NULL)
                {
                  /* Set new node to be available for reading */
                  pQueue->nodeForRecv = newNode;
                }
                else
                {
                  /* Node for receiving never contain non NULL pointer at this state */
                  VALIDATE_CRITIC((pQueue->nodeForRecv != (osa_qNode *) NULL), RET_CODE_ERROR_INVALID_DATA,
                                  "Non NULL recv node, queue invalid!");
                }
                /* Set new node to be available for linking */
                pQueue->nodeForSend = newNode;
              }
              else
              {
                if (pQueue->nodeForRecv == (osa_qNode *) NULL)
                {
                  /* Node for receiving never contain NULL pointer at this state */
                  VALIDATE_CRITIC((pQueue->nodeForRecv == (osa_qNode *) NULL),RET_CODE_ERROR_INVALID_DATA,
                                  "NULL recv node, queue invalid!");
                }
                /* Set link for prev node to new node */
                pQueue->nodeForSend->nextNode = (void *) newNode;
                /* Set new node on top */
                pQueue->nodeForSend = newNode;
              }

              pQueue->q_attr.q_msgs_avail++;
              pQueue->q_attr.q_msgs_free_nodes--;
              /* Unlock the data available mutex */
              osa_Munlock(pQueue->qDataMux);
              ret_code = RET_CODE_SUCCESS;
            }
          }
          osa_Munlock(pQueue->qAccessMux);
        }

        if (ret_code != RET_CODE_SUCCESS)
        {
          osa_TsleepMS(1u);
          if (timeout != OSA_INFINITE_WAIT)
          {
            /* Just dec timeout and wait for a while */
            if(timeout != 0u)
            {
              timeout--;
            }
            else
            {
              if (ret_code != RET_CODE_SUCCESS)
              {
                ret_code = RET_CODE_ERROR_TIMEOUT;
              }
              break;
            }
          }
        }
      } while (ret_code != RET_CODE_SUCCESS);

#endif  // (USE_FREERTOS_Q != 0u)

    }
    else
    {
      ret_code = RET_CODE_ERROR_INVALID_LENGTH;
    }
  }
  else
  {
    /* NULL pointer passed */
    ret_code = RET_CODE_ERROR_NULL;
  }
  return ret_code;
}

/**
 * @brief	Function for get message from queue.
 *
 * @param[in]	queueHandle - queue handle.
 * @param[out]	pMsg - pointer to buffer that will be written by message (May be NULL).
 * @param[in]	msg_len - buffer length.
 * @param[out]	pMsg_prio - pointer to variable that will be written by message priority (May be NULL).
 * @param[in]	timeout - the max timeout for waiting for arrived the message.
 *
 * @retval	-1 if no elements in queue or NULL pointer to queue handle else received size.
 */
ssize_t osa_Qreceive(osa_QHandle_t queueHandle, void * pMsg, size_t msg_len, uint32_t * pMsg_prio, uint32_t timeout)
{
  ssize_t retsize = -1;
  if (queueHandle != (osa_QHandle_t) NULL)
  {
    osa_queue_struct_t * pQueue = (osa_queue_struct_t *) queueHandle;

#if (USE_FREERTOS_Q != 0u)
/* Use FREERTOS queue implementation */

    /* Checks the size of buffer that will be used for writing */
    if (pQueue->q_attr.q_max_msg_size <= msg_len)
    {
      BaseType_t xReturn = pdFAIL;
      osa_qNode * pDataNode = (osa_qNode *) NULL;

      if(inHandlerMode() != 0)
      {
        /* Checks the available elements */
        if (uxQueueMessagesWaiting(pQueue->q_handle) != 0)
        {
          portBASE_TYPE taskWoken = pdFALSE;
          xReturn = xQueueReceiveFromISR(pQueue->q_handle, (void *)&pDataNode, &taskWoken);
          portEND_SWITCHING_ISR(taskWoken);
        }
      }
      else
      {
        TickType_t ticks = ((timeout == OSA_INFINITE_WAIT) ? portMAX_DELAY : ((TickType_t)timeout / portTICK_PERIOD_MS));
        xReturn = xQueueReceive(pQueue->q_handle, (void *)&pDataNode, ticks);
      }
      if ((pDataNode != (osa_qNode *) NULL) && (xReturn == pdPASS))
      {
        if (pMsg_prio != (uint32_t *) NULL)
        {
#if (USE_FREERTOS_Q_PRIO != 0u)
          *pMsg_prio = pDataNode->prio;
#else
          *pMsg_prio = 0u;
#endif // (USE_FREERTOS_Q_PRIO != 0u)
        }
        /* Data received set size to output */
        retsize = (ssize_t) pDataNode->datSize;

        if(pMsg != (void *) NULL)
        {
          /* Copy the data to buffer */
          memcpy(pMsg, pDataNode->datArray, pDataNode->datSize);
        }
        /* Free taken memory */
        FREE(osa_qNode, pDataNode);
      }
#ifdef DEBUG
      else
      {
        if (pDataNode != (osa_qNode *) NULL)
        {
          UTIL_DBG_WARN("Warning at receiving!");
        }
      }
#endif
    }
#else
/* OSA queue implementation */
    do
    {
      if (pQueue->q_attr.q_max_msg_size <= msg_len)
      {
        /* Try to get data from queue */
        if(osa_Mlock(pQueue->qAccessMux, DEFAULT_MUX_ACCESS_TIME_MS) == RET_CODE_SUCCESS)
        {
          if (pQueue->q_attr.q_msgs_avail != 0u)
          {
            if (pQueue->nodeForRecv != (osa_qNode *) NULL)
            {
              retsize = (ssize_t) pQueue->nodeForRecv->datSize;
              /* Remember the next node */
              osa_qNode * nextNode = (osa_qNode *) pQueue->nodeForRecv->nextNode;

              if (pMsg_prio != (uint32_t *)NULL)
              {
                *pMsg_prio = pQueue->nodeForRecv->prio;
              }
              if (pMsg != (void *)NULL)
              {
                /* Memory allocated - copy and add link*/
                memcpy(pMsg, pQueue->nodeForRecv->datArray, pQueue->nodeForRecv->datSize);
              }

              FREE(osa_qNode, pQueue->nodeForRecv);

              /* lINK NODES: if node for receving eq to node for transmit (One element in queue that we read) */
              if (pQueue->nodeForRecv == pQueue->nodeForSend)
              {
                pQueue->nodeForRecv = pQueue->nodeForSend = (osa_qNode *) NULL;
              }
              else
              {
                /* Set node that we remember as next */
                pQueue->nodeForRecv = nextNode;
              }

              pQueue->q_attr.q_msgs_avail--;
              pQueue->q_attr.q_msgs_free_nodes++;
            }
            else
            {
              /* Node for receiving must never contain NULL pointer at this state */
              VALIDATE_CRITIC((pQueue->nodeForRecv == (osa_qNode *) NULL), RET_CODE_ERROR_INVALID_DATA, "NULL recv node, queue invalid!");
            }
            osa_Munlock(pQueue->qAccessMux);
          }
          else
          {
            osa_Munlock(pQueue->qAccessMux);
            /* Wait for new data in queue by using mutex */
            if (osa_Mlock(pQueue->qDataMux, timeout) != RET_CODE_SUCCESS)
            {
              break;
            }
          }
        }
      }
      else
      {
        break;
      }
    } while (retsize == -1);
#endif  // (USE_FREERTOS_Q != 0u)
  }
  return retsize;
}

/**
 * @brief	Function for get message from queue.
 *
 * @param[in]	queueHandle - queue handle.
 * @param[out]	pPMsg - pointer to pointer that will be written by pointer to message.
 * @param[out]	pMsg_len - pointer to variable that will be written by message length.
 * @param[out]	pMsg_prio - pointer to variable that will be written by message priority(May be NULL).
 * @param[in]	timeout - the max timeout for waiting for arrived the message.
 *
 * @retval	RET_CODE_SUCCESS				If message peeked.
 * @retval	RET_CODE_ERROR_TIMEOUT			If message not peeked from the FIFO due to timeout.
 * @retval	RET_CODE_ERROR_NOT_FOUND		If message not peeked from the FIFO due to no available elements in FIFO.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer (Queue handler or pointer to void pointer) has been passed to function.
 */
ret_code_t osa_Qpeek(osa_QHandle_t queueHandle, void ** pPMsg, size_t * pMsg_len, uint32_t * pMsg_prio, uint32_t timeout)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if ((queueHandle != (osa_QHandle_t) NULL) && (pPMsg != (void **) NULL))
  {
    osa_queue_struct_t * pQueue = (osa_queue_struct_t *) queueHandle;
    *pPMsg = (void *) NULL;
    ret_code = RET_CODE_ERROR_NOT_FOUND;

#if (USE_FREERTOS_Q != 0u)
    /* Use FREERTOS queue implementation */

    BaseType_t xReturn = pdFAIL;
    osa_qNode * pDataNode = (osa_qNode *) NULL;

    if (inHandlerMode() != 0)
    {
      /* Checks the queue messages count */
      if (uxQueueMessagesWaiting(pQueue->q_handle) != 0)
      {
        xReturn = xQueuePeekFromISR(pQueue->q_handle, (void *) &pDataNode);
      }
    }
    else
    {
      TickType_t ticks = ((timeout == OSA_INFINITE_WAIT) ? portMAX_DELAY : ((TickType_t)timeout / portTICK_PERIOD_MS));
      xReturn = xQueuePeek(pQueue->q_handle, (void *)&pDataNode, ticks);
    }
    if ((pDataNode != (osa_qNode *) NULL) && (xReturn == pdPASS))
    {
      ret_code = RET_CODE_SUCCESS;
      if (pMsg_prio != (uint32_t *) NULL)
      {
#if (USE_FREERTOS_Q_PRIO != 0u)
        *pMsg_prio = pDataNode->prio;
#else
        *pMsg_prio = 0;
#endif  // (USE_FREERTOS_Q_PRIO != 0u)
      }
      *pPMsg = pDataNode->datArray;
      if (pMsg_len != (size_t *) NULL)
      {
        *pMsg_len = pDataNode->datSize;
      }
    }
    else
    {
      /* No elements in FIFO */
      ret_code = RET_CODE_ERROR_NOT_FOUND;
#ifdef DEBUG
      if (pDataNode != (osa_qNode *) NULL)
      {
        UTIL_DBG_WARN("Warning at peeking!");
      }
#endif
    }
#else
/* OSA queue implementation */
    do
    {
      /* Try to peek the data from quque */
      if(osa_Mlock(pQueue->qAccessMux, DEFAULT_MUX_ACCESS_TIME_MS) == RET_CODE_SUCCESS)
      {
        if (pQueue->q_attr.q_msgs_avail != 0u)
        {
          if(pQueue->nodeForRecv != (osa_qNode *) NULL)
          {
            if(pMsg_prio != (uint32_t *) NULL)
            {
              *pMsg_prio = pQueue->nodeForRecv->prio;
            }

            *pPMsg = (void *) pQueue->nodeForRecv->datArray;
            if (pMsg_len != (size_t *) NULL)
            {
              *pMsg_len = pQueue->nodeForRecv->datSize;
            }
            ret_code = RET_CODE_SUCCESS;
          }
          else
          {
            /* Node for receiving must never contain NULL pointer at this state */
            VALIDATE_CRITIC((pQueue->nodeForRecv == (osa_qNode *) NULL), RET_CODE_ERROR_NULL, "NULL recv node, queue invalid!");
          }
          osa_Munlock(pQueue->qAccessMux);
        }
        else
        {
          osa_Munlock(pQueue->qAccessMux);
          /* Wait for new data in queue by using mutex */
          if (osa_Mlock(pQueue->qDataMux, timeout) != RET_CODE_SUCCESS)
          {
            break;
          }
        }
      }
    } while (ret_code != RET_CODE_SUCCESS);
#endif  // (USE_FREERTOS_Q != 0u)
  }
  return ret_code;
}

/**
 * @brief	Function for create the mutex.
 *
 * @param[out]	pMuxHandle - pointer to static mutex handle.
 * @param[in]	lock - lock mutex after creation.
 *
 * @retval	RET_CODE_SUCCESS				If mutex created.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer to mutex handle has been passed to function.
 * @retval	RET_CODE_ERROR_INTERNAL			Internal error.
 */
ret_code_t osa_Mcreate(osa_MHandle_t * pMuxHandle, bool lock)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (pMuxHandle != (osa_MHandle_t *) NULL)
  {
#ifdef FREERTOS
/* For FREERTOS only */
    /* Create binary semaphore (Mutex) */
    *pMuxHandle = (osa_MHandle_t) xSemaphoreCreateBinary();
    if (*pMuxHandle != (osa_MHandle_t) NULL)
    {
      if (lock == false)
      {
        /* Give the semaphore if this must be unlocked */
        xSemaphoreGive((SemaphoreHandle_t)*pMuxHandle);
      }
      ret_code = RET_CODE_SUCCESS;
    }
    else
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
#else
/* For Linux OS */
    ret_code = RET_CODE_ERROR_INTERNAL;
    pthread_mutex_t * pMutex = (pthread_mutex_t *) NULL;
    MALLOC(pthread_mutex_t, pMutex, sizeof(pthread_mutex_t));
    if (pMutex != (pthread_mutex_t *) NULL)
    {
      /* Create mutex */
      pthread_mutexattr_t mta;
      int rc = pthread_mutexattr_init(&mta);
      if (rc == 0)
      {
        rc = pthread_mutex_init(pMutex, &mta);
        if(rc == 0)
        {
          ret_code = RET_CODE_SUCCESS;
          *pMuxHandle = (osa_MHandle_t) pMutex;
          if (lock == true)
          {
            pthread_mutex_lock(pMutex);
          }
        }
      }
      if (rc != 0)
      {
        *pMuxHandle = (osa_MHandle_t) NULL;
        FREE(pthread_mutex_t, pMutex);
      }
    }
#endif  // FREERTOS
  }
  return ret_code;
}

/**
 * @brief	Function for lock the mutex.
 *
 * @param[in]	muxHandle - mutex handle.
 * @param[in]	timeout - time in ms to wait mutex.
 *
 * @retval	RET_CODE_SUCCESS				If mutex locked.
 * @retval	RET_CODE_ERROR_TIMEOUT			If mutex not available due to timeout.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer muxHandle has been passed to function.
 */
ret_code_t osa_Mlock(osa_MHandle_t muxHandle, uint32_t timeout)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (muxHandle != (osa_MHandle_t) NULL)
  {
    ret_code = RET_CODE_SUCCESS;
#ifdef FREERTOS
/* For FREERTOS only */
    if (inHandlerMode() != 0)
    {
      /* Take the mutex in ISR mode */
      portBASE_TYPE taskWoken = pdFALSE;
      if(xSemaphoreTakeFromISR((SemaphoreHandle_t) muxHandle, &taskWoken) != pdTRUE)
      {
        UTIL_DBG_WARN("Can't take semaphore in ISR.");
        ret_code = RET_CODE_ERROR_TIMEOUT;
      }
      portEND_SWITCHING_ISR(taskWoken);
    }
    else
    {
      TickType_t ticks = ((timeout == OSA_INFINITE_WAIT) ? portMAX_DELAY : ((TickType_t)timeout / portTICK_PERIOD_MS));
      /* Take the mutex */
      if (xSemaphoreTake((SemaphoreHandle_t) muxHandle, ticks) != pdTRUE)
      {
        ret_code = RET_CODE_ERROR_TIMEOUT;
      }
    }
#else
/* For Linux OS */
    if (timeout == OSA_INFINITE_WAIT)
    {
      /* Take the mutex */
      if (pthread_mutex_lock((pthread_mutex_t *) muxHandle) != 0)
      {
        ret_code = RET_CODE_ERROR_TIMEOUT;
      }
    }
    else
    {
      struct timespec  abstime;
      if (timeout != 0)
      {
        abstime.tv_nsec = timeout * 1000;
      }
      else
      {
        abstime.tv_nsec = 1;
      }
      if(pthread_mutex_timedlock((pthread_mutex_t *) muxHandle, &abstime) != 0)
      {
        ret_code = RET_CODE_ERROR_TIMEOUT;
      }
    }
#endif  // FREERTOS
  }
  return ret_code;
}

/**
 * @brief	Function for unlock the mutex.
 *
 * @param[in]	muxHandle - mutex handle.
 *
 * @retval	RET_CODE_SUCCESS				If mutex locked.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal error.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer muxHandle has been passed to function.
 */
ret_code_t osa_Munlock(osa_MHandle_t muxHandle)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (muxHandle != (osa_MHandle_t) NULL)
  {
    ret_code = RET_CODE_SUCCESS;
#ifdef FREERTOS
/* For FREERTOS only */
    if (inHandlerMode() != 0)
    {
      /* Give the mutex in ISR mode */
      portBASE_TYPE taskWoken = pdFALSE;
      if(xSemaphoreGiveFromISR((SemaphoreHandle_t) muxHandle, &taskWoken) != pdTRUE)
      {
        UTIL_DBG_WARN("Can't give semaphore (");
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
      portEND_SWITCHING_ISR(taskWoken);
    }
    else
    {
      /* Give the mutex */
      if (xSemaphoreGive((SemaphoreHandle_t) muxHandle) != pdTRUE)
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
    }
#else
/* For Linux OS */
    /* Give the mutex */
    if (pthread_mutex_unlock((pthread_mutex_t *) muxHandle) != 0)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
#endif  // FREERTOS
  }
  return ret_code;
}

/**
 * @brief	Function for delete the mutex.
 *
 * @param[out]	muxHandle - pointer to mutex handle.
 *
 * @retval	RET_CODE_SUCCESS				If mutex successfully deleted.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal error.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer muxHandle has been passed to function.
 */
ret_code_t osa_Mdelete(osa_MHandle_t * muxHandle)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (muxHandle != (osa_MHandle_t *) NULL)
  {
    if (*muxHandle != (osa_MHandle_t) NULL)
    {
      ret_code = RET_CODE_SUCCESS;
#ifdef FREERTOS
/* For FREERTOS only */
      /* Delete the mutex */
      vSemaphoreDelete((SemaphoreHandle_t) (*muxHandle));
      *muxHandle = (osa_MHandle_t) NULL;
#else
/* For Linux OS */
      /* Delete the mutex */
      if (pthread_mutex_destroy((pthread_mutex_t *) (*muxHandle)) != 0)
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
      else
      {
        FREE(osa_MHandle_t, *muxHandle);
        *muxHandle = (osa_MHandle_t) NULL;
      }
#endif  // FREERTOS
    }
  }
  return ret_code;
}

/**
 * @brief	Function for create the thread.
 *
 * @param[io]	pThreadHandle - pointer to thread handle.
 * @param[in]	threadFunc - pointer to executable function.
 * @param[in]	pArg - pointer that will be passed to executable function.
 * @param[in]	pThreadName - pointer to the constant name string variable.
 * @param[in]	stackSize - stack size.
 * @param[in]	tPrio - thread priority.
 *
 * @retval	RET_CODE_SUCCESS				If task has been created.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal error.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer to pThreadHandle has been passed to function.
 * @retval	RET_CODE_ERROR_NO_MEM			If no available memory in HEAP
 */
ret_code_t osa_Tcreate(osa_THandle_t * pThreadHandle, osa_thread_callback_t threadFunc, void * pArg, const char * pThreadName, uint32_t stackSize, uint32_t tPrio)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (pThreadHandle != (osa_THandle_t *) NULL)
  {
    osa_threadInstance_t * threadInst = (osa_threadInstance_t *) NULL;
    MALLOC(osa_threadInstance_t, threadInst, sizeof(osa_threadInstance_t));
    if (threadInst != (osa_threadInstance_t *) NULL)
    {
      threadInst->threadArgs.threadTerminateFlag = false;
      threadInst->threadArgs.threadTerminatedFlag = false;
      threadInst->threadArgs.threadEnteredFlag = false;
      threadInst->threadArgs.threadSuspendedFlag = false;
      threadInst->threadArgs.pPassedArg = pArg;
      threadInst->threadArgs.t_name = pThreadName;

#ifdef FREERTOS
/* For FREERTOS only */
      if(stackSize == 0u)
      {
        stackSize = configMINIMAL_STACK_SIZE;
      }
      /* Create the thread (In FREERTOS we able create only task) */
      if (xTaskCreate((TaskFunction_t)threadFunc,                   /* Function that implements the task. */
                      pThreadName,                                  /* Text name for the task. */
                      stackSize,                                    /* Number of indexes in the xStack array. */
                      (void *) 		&threadInst->threadArgs,        /* Parameter passed into the task. */
                      tPrio,                                        /* Priority at which the task is created. */
                      (TaskHandle_t*)	&threadInst->threadArgs.pThreadInstance) == pdTRUE)	/* Pointer to task handle. */
      {
        ret_code = RET_CODE_SUCCESS;
      }
      else
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
#else
/* For Linux OS */
      ret_code = RET_CODE_ERROR_INTERNAL;
      int res = 0;

      /* Initialize thread creation attributes */
      res = pthread_attr_init(&threadInst->attr);
      if (res == 0)
      {
        if(stackSize != 0u)
        {
          pthread_attr_setstacksize(&threadInst->attr, stackSize);
        }
        if (res == 0)
        {
          res = pthread_create(&threadInst->pThreadInstance,          /* Pointer to task handle. */
                               &threadInst->attr,                     /* Task attributes. */
                               (void*(*)(void*))threadFunc,           /* Function that implements the task. */
                               (void *) &threadInst->threadArgs);     /* Parameter passed into the task. */
          if (res == 0)
          {
            threadInst->threadArgs.pThreadInstance = (void *) &threadInst->pThreadInstance;
            ret_code = RET_CODE_SUCCESS;
          }
        }
      }
#endif  // FREERTOS
      if (ret_code != RET_CODE_SUCCESS)
      {
        *pThreadHandle = (osa_THandle_t) NULL;
        FREE(osa_threadInstance_t, threadInst);
      }
      else
      {
        *pThreadHandle = (osa_THandle_t) threadInst;
      }
    }
    else
    {
      ret_code = RET_CODE_ERROR_NO_MEM;
    }
  }
  return ret_code;
}

/**
 * @brief	Function for suspend the thread.
 *
 * @param[in]	theadHandle - thread handle.
 *
 * @retval	RET_CODE_SUCCESS				If thread has been suspended.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal error.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer to theadHandle has been passed to function.
 */
ret_code_t osa_Tsuspend(osa_THandle_t theadHandle)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (theadHandle != (osa_THandle_t) NULL)
  {
    osa_threadInstance_t * threadInst = (osa_threadInstance_t *) theadHandle;
#ifdef FREERTOS
/* For FREERTOS only */

    /* Suspend the thread */
    vTaskSuspend((TaskHandle_t*) threadInst->threadArgs.pThreadInstance);
#else
/* For Linux OS */

    /* There are no native support suspend function on most of Unix */

#endif  // FREERTOS

    threadInst->threadArgs.threadSuspendedFlag = true;
    ret_code = RET_CODE_SUCCESS;
  }
  return ret_code;
}

/**
 * @brief	Function for resume the thread.
 *
 * @param[in]	theadHandle - pointer to thread handle.
 *
 * @retval	RET_CODE_SUCCESS				If thread has been resumed.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer to theadHandle has been passed to function.
 */
ret_code_t osa_Tresume(osa_THandle_t theadHandle)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (theadHandle != (osa_THandle_t) NULL)
  {
    osa_threadInstance_t * threadInst = (osa_threadInstance_t *) theadHandle;

#ifdef FREERTOS
/* For FREERTOS only */

    /* Resume the thread */
    vTaskResume((TaskHandle_t*) threadInst->threadArgs.pThreadInstance);
#else
/* For Linux OS */

    /* There are no native support resume function by most of Unix */

#endif  // FREERTOS

    threadInst->threadArgs.threadSuspendedFlag = false;
    ret_code = RET_CODE_SUCCESS;
  }
  return ret_code;
}

/**
 *  @brief	Function for yield from the thread.
 *
 * @param	None.
 *
 * @retval	None
 */
void osa_Tyield(void)
{
#ifdef FREERTOS
  /* For FREERTOS only */

  taskYIELD();

#else

/* For Linux OS */
#ifdef __USE_GNU
  /* If GNU we able to use yield function */
  pthread_yield();
#else
  /* In other case just wait minimum time */
  usleep(1u);
#endif  // __USE_GNU

#endif  // FREERTOS
}

/**
 * @brief	Function for yield from the thread.
 *
 * @param	None.
 *
 * @retval	None
 */
void osa_Texit(void * pRetCode)
{
#ifdef FREERTOS
  (void) pRetCode;
/* For FREERTOS only */
  vTaskSuspend(NULL);
#else
/* For Linux OS */
  pthread_exit(pRetCode);
#endif  // FREERTOS
}

/**
 * @brief	Function for wait some time in the thread.
 *
 * @param[in]	ms - milliseconds (if used 0 then thread will daly the minimum time).
 *
 * @retval	None
 */
void osa_TsleepMS(uint32_t ms)
{
#ifdef FREERTOS
/* For FREERTOS only */
  if (ms != 0u)
  {
    vTaskDelay(ms);
  }
  else
  {
    taskYIELD();
  }
#else
/* For Linux OS */
  if (ms != 0u)
  {
    usleep(ms * 1000u);
  }
  else
  {
    usleep(1u);
  }
#endif  // FREERTOS
}

/**
 * @brief	Function for delete the thread.
 *
 * @param[out]	theadHandle - pointer to thread handle.
 * @param[in]	maxResponseTimeMS - the time in MS, during this time thread should be closed in safe way.
 *
 * @retval	RET_CODE_SUCCESS				If thread has been deleted.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal error.
 * @retval	RET_CODE_ERROR_NULL				If NULL pointer to theadHandle has been passed to function.
 */
ret_code_t osa_Tdelete(osa_THandle_t * theadHandle, uint32_t maxResponseTimeMS)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (theadHandle != (osa_THandle_t *) NULL)
  {
    if (*theadHandle != (osa_THandle_t) NULL)
    {
      osa_threadInstance_t * threadInst = (osa_threadInstance_t *) (*theadHandle);
#ifdef FREERTOS
/* For FREERTOS only */
      /* If thread suspended - resume to safe exit */
      if (threadInst->threadArgs.threadSuspendedFlag == true)
      {
        /* Resume the thread */
        vTaskResume((TaskHandle_t*) threadInst->threadArgs.pThreadInstance);
      }
      /* Try to close thread in safe mode */
      threadInst->threadArgs.threadTerminateFlag = true;
      for (uint32_t i = 0u; i < maxResponseTimeMS; i++)
      {
        osa_TsleepMS(1u);
        if (threadInst->threadArgs.threadTerminatedFlag == true)
        {
          break;
        }
      }
      /* Delete the thread */
      vTaskDelete((TaskHandle_t*) threadInst->threadArgs.pThreadInstance);
      if ((threadInst->threadArgs.threadEnteredFlag == true) && (threadInst->threadArgs.threadTerminatedFlag == true))
      {
        ret_code = RET_CODE_SUCCESS;
      }
      else
      {
        ret_code = RET_CODE_ERROR_INVALID_STATE;
      }
      /* Free taken memory */
      FREE(osa_threadInstance_t, threadInst);
#else
/* For Linux OS */
      /* Try to close thread in safe mode */
      threadInst->threadArgs.threadTerminateFlag = true;
      for (uint32_t i = 0u; i < maxResponseTimeMS; i++)
      {
        osa_TsleepMS(1u);
        if (threadInst->threadArgs.threadTerminatedFlag == true)
        {
          ret_code = RET_CODE_SUCCESS;
          /* Free taken memory */
          FREE(osa_threadInstance_t, threadInst);
          break;
        }
      }
      /* Delete the thread */
      if (threadInst->threadArgs.threadTerminatedFlag != true)
      {
        if (pthread_cancel(threadInst->pThreadInstance) == 0)
        {
          /* Destroy the attribute */
          pthread_attr_destroy(&threadInst->attr);
          ret_code = RET_CODE_SUCCESS;

          /* Free taken memory */
          FREE(osa_threadInstance_t, threadInst);
        }
        else
        {
          ret_code = RET_CODE_ERROR_INVALID_STATE;
        }
      }
#endif  // FREERTOS
      if (ret_code == RET_CODE_SUCCESS)
      {
        /* Set handle to NULL */
        *theadHandle = (osa_THandle_t) NULL;
      }
      else
      {
        UTIL_DBG_WARN("Task delete bad response!");
      }
    }
  }
  return ret_code;
}

/**
 * @brief	Enter to the atomic section.
 *
 * @param	None.
 *
 * @retval	None.
 */
void osa_Aenter(void)
{
#ifdef FREERTOS
  /* For FREERTOS only */
  if(inHandlerMode() != 0)
  {
    /* If in ISR just disable interrupts */
    portDISABLE_INTERRUPTS();
  }
  else
  {
    portENTER_CRITICAL();
  }
#else
  /* For Linux OS */
#endif  // FREERTOS
}

/**
 * @brief	Exit from the atomic section.
 *
 * @param	None.
 *
 * @retval	None.
 */
void osa_Aexit(void)
{
#ifdef FREERTOS
  /* For FREERTOS only */
  if(inHandlerMode() != 0)
  {
    /* If in ISR just enable interrupts */
    portENABLE_INTERRUPTS();
  }
  else
  {
    portEXIT_CRITICAL();
  }
#else
  /* For Linux OS */
#endif  // FREERTOS
}

/**
 * @brief	Djb2-based hash calculate function with length extension.
 *
 * @param[in]	pData - pointer to data.
 * @param[in]	length - data length.
 *
 * @retval	Hash code.
 */
uint32_t osa_uHash(uint8_t * pData, size_t length)
{
  uint32_t hash = 5381;
  uint32_t c = 0;
  while(length--)
  {
    c = (uint32_t)*pData++;
    hash = ((hash << 5) + hash) + c; /* hash * 32 + c */
  }
  return hash;
}

/*--------------------------------------- TESTING MODULE ---------------------------------------------*/
#ifdef DEBUG
/* Unit tests implementation */

/* Defines */
#define MAX_Q_MSG_LEN 100u
#define MAX_Q_MSGES   100u

/* Variables */

/* Queue handles */
static osa_QHandle_t osa_UQ1 = (osa_QHandle_t) NULL;
static osa_QHandle_t osa_UQ2 = (osa_QHandle_t) NULL;

/* Mutex handles */
static osa_MHandle_t osa_UM1 = (osa_MHandle_t) NULL;
static uint32_t osa_sharedVar1 = 0u;
static uint32_t osa_sharedVar2 = 0u;

/* Thread handles */
static osa_THandle_t osa_UT1 = (osa_THandle_t) NULL;
static osa_THandle_t osa_UT2 = (osa_THandle_t) NULL;

static uint32_t osa_atomicVar1 = 0u;
static uint32_t osa_atomicVar2 = 0u;

/* Unittest thread 1*/
static void * osa_thread1_callback(osa_threadArg_t * pArg)
{
  pArg->threadEnteredFlag = true;

  uint32_t messages_recv = 0u;
  uint32_t messages_peeked = 0u;
  uint32_t messages_send = 0u;

  UTIL_DBG_INFO("Thread with name '%s' started.", pArg->t_name);
  UTIL_DBG_INFO("Passed string: %s", ((char *)pArg->pPassedArg));

  while (pArg->threadTerminateFlag == false)
  {
    /* User code there */
    if (osa_QgetState(osa_UQ1).q_msgs_free_nodes != 0u)
    {
      if (osa_Qsend(osa_UQ1, (const void *)"const void * pMsg", strlen("const void * pMsg"), 0u, 1u) != RET_CODE_SUCCESS)
      {
        UTIL_DBG_WARN("Failed to send message from thread #1!");
      }
      else
      {
        messages_send++;
      }
    }

    uint8_t recv_array[MAX_Q_MSG_LEN] = { 0u };

    if (osa_QgetState(osa_UQ2).q_msgs_avail != 0u)
    {
      void * rmsg = (void *) NULL;
      size_t pMsg_len;
      uint32_t pMsg_prio;

      if (osa_Qpeek(osa_UQ2, &rmsg, &pMsg_len, &pMsg_prio, 1u) != RET_CODE_SUCCESS)
      {
        UTIL_DBG_WARN("Failed to peek message from thread #1!");
      }
      else
      {
        messages_peeked++;
      }

      if (osa_Qreceive(osa_UQ2, recv_array, MAX_Q_MSG_LEN, &pMsg_prio, 1u) == -1)
      {
        UTIL_DBG_WARN("Failed to receive message from thread #1!");
      }
      else
      {
        messages_recv++;
      }
    }

    if (osa_Mlock(osa_UM1 , 10u) == RET_CODE_SUCCESS)
    {
      osa_sharedVar1++;

      osa_Munlock(osa_UM1);
    }

    osa_TsleepMS(10u);

    osa_Aenter();
    osa_atomicVar1++;
    osa_Aexit();

    /* OSA suspend mechanism */
    while (pArg->threadSuspendedFlag)
    {
      osa_TsleepMS(1u);
      if (pArg->threadTerminateFlag == true)
      {
        break;
      }
    }
  }

  if (messages_peeked != messages_recv)
  {
    UTIL_DBG_WARN("Queue #2 error!");
  }
  osa_TsleepMS(1u);
  UTIL_DBG_INFO("The thread #1 finished!");
  UTIL_DBG_INFO("The shared var #1 was used: %d times over mutex! ", osa_sharedVar1);
  UTIL_DBG_INFO("The atomic #1 was used: %d times! ", osa_atomicVar1);
  UTIL_DBG_INFO("Thread sent %d messages and received %d! ", messages_send, messages_recv);
  osa_TsleepMS(1u);

  pArg->threadTerminatedFlag = true;
  osa_Texit(NULL);
  return NULL;
}

/* Unit test thread 2 */
static void * osa_thread2_callback(osa_threadArg_t * pArg)
{
  pArg->threadEnteredFlag = true;

  uint32_t messages_recv = 0u;
  uint32_t messages_peeked = 0u;
  uint32_t messages_send = 0u;

  UTIL_DBG_INFO("Thread with name '%s' started.", pArg->t_name);
  UTIL_DBG_INFO("Passed value: %d", *((uint32_t*)pArg->pPassedArg));

  while (pArg->threadTerminateFlag == false)
  {
    /* User code there */
    if (osa_QgetState(osa_UQ2).q_msgs_free_nodes != 0u)
    {
      if (osa_Qsend(osa_UQ2, (const void *)"const void * pMsg", strlen("const void * pMsg"), 0u, 1u) != RET_CODE_SUCCESS)
      {
        UTIL_DBG_WARN("Failed to send message from thread #2!");
      }
      else
      {
        messages_send++;
      }
    }

    uint8_t recv_array[MAX_Q_MSG_LEN] = { 0u };

    if (osa_QgetState(osa_UQ1).q_msgs_avail != 0u)
    {
      void * rmsg = NULL;
      size_t pMsg_len;
      uint32_t pMsg_prio;

      if (osa_Qpeek(osa_UQ1, &rmsg, &pMsg_len, &pMsg_prio, 1u) != RET_CODE_SUCCESS)
      {
        UTIL_DBG_WARN("Failed to peek message from thread #1!");
      }
      else
      {
        messages_peeked++;
      }

      if (osa_Qreceive(osa_UQ1, recv_array, MAX_Q_MSG_LEN, &pMsg_prio, 1u) == -1)
      {
        UTIL_DBG_WARN("Failed to receive message from thread #1!");
      }
      else
      {
        messages_recv++;
      }
    }

    if (osa_Mlock(osa_UM1 , 10u) == RET_CODE_SUCCESS)
    {
      osa_sharedVar2++;

      osa_Munlock(osa_UM1);
    }

    osa_TsleepMS(10u);

    osa_Aenter();
    osa_atomicVar2++;
    osa_Aexit();

    /* OSA suspend mechanism */
    while (pArg->threadSuspendedFlag)
    {
      osa_TsleepMS(1u);
      if (pArg->threadTerminateFlag == true)
      {
        break;
      }
    }
  }

  osa_TsleepMS(1u);
  if (messages_peeked != messages_recv)
  {
    UTIL_DBG_WARN("Queue #1 error!");
  }
  UTIL_DBG_INFO("The thread #2 finished!");
  UTIL_DBG_INFO("The shared var #2 was used: %d times over mutex! ", osa_sharedVar2);
  UTIL_DBG_INFO("The atomic #2 was used: %d times! ", osa_atomicVar2);
  UTIL_DBG_INFO("Thread sent %d messages and received %d! ", messages_send, messages_recv);
  osa_TsleepMS(1u);

  pArg->threadTerminatedFlag = true;
  osa_Texit(NULL);
  return NULL;
}

/**
* @brief	Function to test the functionality of this library.
*
* @param	times - time in seconds during testing will be active.
*
* @retval	None.
*/
void osa_Unittest(uint32_t times)
{
  UTIL_DBG_INFO("\n");
  UTIL_DBG_INFO("The test of OS Abstraction level begin for %d seconds!", times);
  UTIL_DBG_INFO("The hash code of \"Hello world!\" == 0x%08X", osa_uHash((uint8_t *)"Hello world!", strlen("Hello world!")));
  UTIL_DBG_INFO("The hash code of \"!dlrow olleH\" == 0x%08X", osa_uHash((uint8_t *)"!dlrow olleH", strlen("!dlrow olleH")));

  /* Initialize CRC lib*/
  osa_uCRC8_initialize();

  UTIL_DBG_INFO("The crc8 code of \"Hello world!\" == 0x%02X", osa_uCRC8((uint8_t *)"Hello world!", strlen("Hello world!")));

  /* For dynamic memory checking: if memAddr at start eq to memAddr at end than we not lose any memory */
  void * memPtr1 = (void *) NULL;
  MALLOC(void, memPtr1, sizeof(void *));
  FREE(void, memPtr1);
  VALIDATE_CRITIC((memPtr1 == (void *) NULL), 2, "Memory allocation invalid!");

  /* Threads */
  static uint32_t err_count = 0u;

  osa_QAttr qAtrr;
  qAtrr.q_max_hold_msgs_num = MAX_Q_MSGES;
  qAtrr.q_max_msg_size = MAX_Q_MSG_LEN;

  if (osa_Qcreate(&osa_UQ1, "Queue 1", &qAtrr) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to create queue #1!");
    err_count++;
  }

  if (osa_Qcreate(&osa_UQ2, "Queue 2", &qAtrr) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to create queue #2!");
    err_count++;
  }

  if (osa_Mcreate(&osa_UM1, false) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to create mutex!");
    err_count++;
  }

  /* Create thread with automatic stack depth */
  if (osa_Tcreate(&osa_UT1, osa_thread1_callback, "Hello world!", "Thread #1", 0u, 2u) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to create thread #1!");
    err_count++;
  }

  /* Create thread with custom stack depth */
  if (osa_Tcreate(&osa_UT2, osa_thread2_callback, &err_count, "Thread #2", 512u, 4u) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to create thread #2!");
    err_count++;
  }

  VALIDATE_CRITIC((err_count != 0u), 1, "There are many critical errors!");

  osa_TsleepMS(100u);

  if (osa_Tsuspend(osa_UT2) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to suspend thread #2!");
    err_count++;
  }

  osa_TsleepMS(100u);

  if (osa_Tsuspend(osa_UT1) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to suspend thread #1!");
    err_count++;
  }

  osa_TsleepMS(100u);

  if (osa_Tresume(osa_UT2) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to resume thread #2!");
    err_count++;
  }

  osa_TsleepMS(100u);

  if (osa_Tresume(osa_UT1) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to resume thread #1!");
    err_count++;
  }

  if (osa_Tresume(osa_UT2) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to resume thread #2!");
    err_count++;
  }

  osa_TsleepMS(1000u * times);

  if (osa_Tsuspend(osa_UT2) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to suspend thread #2!");
    err_count++;
  }

  if (osa_Tdelete(&osa_UT2, 100u) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to delete thread #2!");
    err_count++;
  }

  if (osa_Tdelete(&osa_UT1, 100u) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to delete thread #1!");
    err_count++;
  }

  if (osa_Qdelete(&osa_UQ1) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to delete queue #1!");
    err_count++;
  }

  if (osa_Qdelete(&osa_UQ2) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to delete queue #2!");
    err_count++;
  }

  if (osa_Mdelete(&osa_UM1) != RET_CODE_SUCCESS)
  {
    UTIL_DBG_WARN("Failed to delete mutex!");
    err_count++;
  }

  void * memPtr2 = (void *)NULL;
  MALLOC(void, memPtr2, sizeof(void *));
  FREE(void ,memPtr2);

  if (memPtr1 != memPtr2)
  {
    UTIL_DBG_WARN("There was memory leakage!");
    MEM_VALIDATE();
  }
  if (err_count == 0u)
  {
    UTIL_DBG_INFO("Test successfully finished. ");
  }
  else
  {
    UTIL_DBG_INFO("Test finished with error count: %d\n", err_count);
  }
}

#endif

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
