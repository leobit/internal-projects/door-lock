//!*****************************************************************************
//!   @file    osa_layer_freertos_config.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    10/04/2018
//!   @brief   This is C header file that defines symbols and constants for FreeRTOS bases osa_layer lib.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  10/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#ifndef _OSA_LAYER_FREERTOS_CONFIG_H_
#define _OSA_LAYER_FREERTOS_CONFIG_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define #0 --------------------------------------------------------*/

/** Set this to 1u if you prefer to use FreeRTOS queues or to 0u if you prefer
  * to use OSA queues implementation.
  */
#define USE_FREERTOS_Q 1u

/** Set this to 1u if you would like to use msg priorities additional field or
  * to 0u to skip this field.
  */
#define USE_FREERTOS_Q_PRIO 1u

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported define #1---------------------------------------------------------*/

/** The default mutex access time */
#define DEFAULT_MUX_ACCESS_TIME_MS 2u

/* Exported functions ------------------------------------------------------- */


#ifdef __cplusplus
}
#endif

#endif /* _OSA_LAYER_FREERTOS_CONFIG_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
