//!*****************************************************************************
//!   @file    osa_layer.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    06/03/2018
//!   @brief   This is C header file of OS abstraction layer library
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  06/03/2018 | Dmytro Kryvyi    | Initial draft
//!  13/03/2018 | Dmytro Kryvyi    | Beta
//!  16/03/2018 | Dmytro Kryvyi    | Code refactoring
//!  04/04/2018 | Dmytro Kryvyi    | Doxygen style and code updated
//!  06/04/2018 | Dmytro Kryvyi    | New features, heap manager outside
//!
//!*****************************************************************************

#ifndef _OSA_LAYER_H_
#define _OSA_LAYER_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "generic_errors.h"
#include "osa_heap.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/

/** CRC8 polynom */
#define CRC8_POLYNOM 0x1Du

/** Definition that allows infinite wait for time based functions */
#define OSA_INFINITE_WAIT 0xFFFFFFFFU

/* Exported typedef ----------------------------------------------------------*/

/** OS abstraction queue handle typedef */
typedef void * osa_QHandle_t;

/** OS abstraction queue attributed typedef */
typedef struct
{
  size_t q_max_hold_msgs_num;     ///< Maximum number of messages.
  size_t q_max_msg_size;        ///< Maximum message size.
  size_t q_msgs_avail;          ///< Number of messages currently queued.
  size_t q_msgs_free_nodes;     ///< Number of free ceil in queue.
  const char * q_name;          ///< Pointer to the queue name.
} osa_QAttr;

/** OS abstraction mutex handle typedef */
typedef void * osa_MHandle_t;

/** OS abstraction thread handle typedef */
typedef void * osa_THandle_t;

/** OS abstraction thread argument typedef */
typedef struct
{
	void * pThreadInstance;				///< Pointer to thread instance.
	void * pPassedArg;					///< Argument that passed to thread.
    const char * t_name;                ///< Thread name.
	volatile bool threadTerminateFlag;	///< By using this flag programmer may realise right way to terminate thread.
	volatile bool threadTerminatedFlag;	///< Set this flag when program counter exit from while(true).
	volatile bool threadEnteredFlag;	///< Set this flag when thread started.
	volatile bool threadSuspendedFlag;	///< You may use this flag to implement custom suspend mechanism.
} osa_threadArg_t;

/** Typedef of thread entry function */
typedef void * (*osa_thread_callback_t)(osa_threadArg_t * pArg);

/* In some system ssize_t may not be defined */
#ifndef __ssize_t_defined  /* EWARM & Some GCC*/
#ifndef _SSIZE_T_DECLARED  /* GCC */

typedef int32_t ssize_t;

#endif  // _SSIZE_T_DECLARED
#endif  // __ssize_t_defined

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/** Macro to write CRC to memory */
#define CRC_WRITE_TO_MEM(pMem, pData, datLen, type)	(*((type *) pMem)) = \
                                                   (type)osa_uCRC8(pData, datLen)

/* Exported functions ------------------------------------------------------- */

/* Queues implementation */
ret_code_t	osa_Qcreate(osa_QHandle_t * pQueueHandle, const char * name,
                        osa_QAttr * pQ_attrs);

ret_code_t	osa_Qdelete(osa_QHandle_t * pQueueHandle);
osa_QAttr	osa_QgetState(osa_QHandle_t queueHandle);
ret_code_t	osa_Qsend(osa_QHandle_t queueHandle, const void * pMsg,
                      size_t msg_len, uint32_t msg_prio, uint32_t timeout);

ssize_t		osa_Qreceive(osa_QHandle_t queueHandle, void * pMsg, size_t msg_len,
                         uint32_t * pMsg_prio, uint32_t timeout);

ret_code_t	osa_Qpeek(osa_QHandle_t queueHandle, void ** pPMsg, size_t * pMsg_len,
                      uint32_t * pMsg_prio, uint32_t timeout);

/* Mutexes implementation */
ret_code_t osa_Mcreate(osa_MHandle_t * pMuxHandle, bool lock);
ret_code_t osa_Mlock(osa_MHandle_t muxHandle, uint32_t timeout);
ret_code_t osa_Munlock(osa_MHandle_t muxHandle);
ret_code_t osa_Mdelete(osa_MHandle_t * muxHandle);

/* Tasks implementation */
ret_code_t	osa_Tcreate(osa_THandle_t * pThreadHandle,
                        osa_thread_callback_t threadFunc, void * pArg,
                        const char * pThreadName, uint32_t stackSize,
                        uint32_t tPrio);

ret_code_t	osa_Tsuspend(osa_THandle_t theadHandle);
ret_code_t	osa_Tresume(osa_THandle_t theadHandle);
void		osa_TsleepMS(uint32_t ms);
void		osa_Tyield(void);
void		osa_Texit(void * pRetCode);
ret_code_t	osa_Tdelete(osa_THandle_t * theadHandle, uint32_t maxResponseTimeMS);

/* Atomic implementation */
void osa_Aenter(void);
void osa_Aexit(void);

/* Utilities */
uint32_t	osa_uHash(uint8_t * pData, size_t length);
uint8_t		osa_uCRC8(uint8_t * pData, size_t length);
void 		osa_uCRC8_initialize(void);

#ifdef DEBUG
/* Unit test */
void osa_Unittest(uint32_t times);
#endif // DEBUG

#ifdef __cplusplus
}
#endif

#endif  /* _OSA_LAYER_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
