//!*****************************************************************************
//!   @file    osa_heap.c
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    06/04/2018
//!   @brief   This is C source file of OS abstraction layer heap manager
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  06/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "osa_heap.h"
#include "osa_layer.h"
#include <string.h>

#ifdef FREERTOS
/* For FREERTOS only */

#include "FreeRTOS.h"

#else

/* For other OS */
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#endif  // FREERTOS

/* Private define section 0 --------------------------------------------------*/

#if (ENABLE_OSAH_LOG == 1u)

/* The name of this module for debugger */
#define MODULE_NAME "OSAH"

#else
#undef UTIL_DBG_INFO
#define UTIL_DBG_INFO(...)
#undef UTIL_DBG_WARN
#define UTIL_DBG_WARN(...)
#undef UTIL_DBG_FLUSH
#define UTIL_DBG_FLUSH()
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private define section 1 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#ifdef DEBUG

#ifndef FREERTOS
/* Create mutex to disable many calls in same time */
static pthread_mutex_t staticMux = PTHREAD_MUTEX_INITIALIZER;
#endif

/* Mem allocation control when debugging */
static volatile uint32_t __mallocCounts = 0;
static volatile uint32_t __freeCounts = 0;

/* Debug malloc*/
void * osad_malloc(size_t size)
{
#ifdef FREERTOS
  /* Use FreeRTOS alloc */
  void * mallocated = pvPortMalloc(size);
  if(mallocated != NULL)
  {
    osa_Aenter();
    __mallocCounts++;
    osa_Aexit();
  }
#else
  /* Use mutex, different threads may discard counter */
  pthread_mutex_lock(&staticMux);
  void * mallocated = malloc(size);
  if(mallocated != NULL)
  {
    __mallocCounts++;
  }
  pthread_mutex_unlock(&staticMux);
#endif
  return mallocated;
}

/* Debug free*/
void osad_free(void * pMem)
{
#ifdef FREERTOS
  /* Use FreeRTOS free */
  osa_Aenter();
  __freeCounts++;
  osa_Aexit();
  vPortFree(pMem);
#else
  /* Use mutex, different threads may discard counter */
  pthread_mutex_lock(&staticMux);
  free(pMem);
  __freeCounts++;
  pthread_mutex_unlock(&staticMux);
#endif
}

/* Memory validater */
void osad_memValidate(void)
{
  UTIL_DBG_INFO("Memory allocated: %u times.", __mallocCounts);
  UTIL_DBG_INFO("Memory freed: %u times.", __freeCounts);
  VALIDATE_CRITIC((__mallocCounts != __freeCounts), __mallocCounts - __freeCounts ,\
                  "There is memory lackage! Malloc and Free counts not eq!");
}

#endif

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
