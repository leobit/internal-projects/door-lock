//!*****************************************************************************
//!   @file    osa_layer_linux_config.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    10/04/2018
//!   @brief   This is C header file that defines symbols and constants for Linux based osa_layer lib.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  10/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#ifndef _OSA_LAYER_LINUX_CONFIG_H_
#define _OSA_LAYER_LINUX_CONFIG_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define #0 --------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported define #1---------------------------------------------------------*/

/** The default mutex access time */
#define DEFAULT_MUX_ACCESS_TIME_MS 1u

/* Exported functions ------------------------------------------------------- */


#ifdef __cplusplus
}
#endif

#endif /* _OSA_LAYER_LINUX_CONFIG_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
