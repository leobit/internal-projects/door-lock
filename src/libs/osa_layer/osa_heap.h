//!*****************************************************************************
//!   @file    osa_heap.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    06/04/2018
//!   @brief   This is C header file of OS abstraction layer heap manager
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  06/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#ifndef _OSA_HEAP_H_
#define _OSA_HEAP_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "generic_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/* For debug purpose */
#ifdef DEBUG

/* While using stdlib we are not able to known about used space, implement custom function to count UP*/
void * osad_malloc(size_t size);
void osad_free(void * pMem);
void osad_memValidate(void);

#define MEM_VALIDATE() osad_memValidate()

/* Custom MALLOC macros */
#define MALLOC(type, pVar, bytesAmount)           \
do                                                \
{                                                 \
  void * __mem_ptr = osad_malloc(bytesAmount);    \
  pVar = (type *) __mem_ptr;                      \
} while (0u)

/* FREE macros */
#define FREE(type, pVar)           	 \
do                               	 \
{                                	 \
  if(pVar != (type *) NULL)        	 \
  {                              	 \
	  osad_free((void *) pVar);  	 \
  }                              	 \
} while (0u)

#else

#ifdef FREERTOS
/* For FREERTOS only */

#include "FreeRTOS.h"

/* MALLOC macros */
#define MALLOC(type, pVar, bytesAmount)           \
do                                                \
{                                                 \
  void * __mem_ptr = pvPortMalloc(bytesAmount);   \
  pVar = (type *) __mem_ptr;                      \
} while (0u)

/* FREE macros */
#define FREE(type, pVar)           	 \
do                               	 \
{                                	 \
  if(pVar != (type *) NULL)        	 \
  {                              	 \
    vPortFree((void *) pVar);      	 \
  }                              	 \
} while (0u)

#else

/* MALLOC macros */
#define MALLOC(type, pVar, bytesAmount)           \
do                                                \
{                                                 \
  void * __mem_ptr = malloc(bytesAmount);         \
  pVar = (type *) __mem_ptr;                      \
} while (0u)

/* FREE macros */
#define FREE(type, pVar)           	 \
do                               	 \
{                                	 \
  if(pVar != (type *) NULL)        	 \
  {                              	 \
    free((void *) pVar);         	 \
  }                              	 \
} while (0u)

#endif /*  FREERTOS */

#endif

/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
}
#endif

#endif  /* _OSA_HEAP_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
