//!*****************************************************************************
//!   @file    uart_drv.c
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    23/04/2018
//!   @brief   This C source file for intelligent hardware UARTE peripheral driver.
//!
//!   @copyright  Copyright (C) 2018
//!
//!   @detail The idea: Use UARTE, PPI and TIMER to offload the CPU.
//!   When byte come  to UARTE peripheral event RXDRDY initiated, when this event initiated - PPI invokes TIMER clear & start task,
//!   every income byte clear and restart timer, so we able to use interrupt that occured to handle received data. Now library use
//!   strstr STD function to findout line escape characters like "\n\r" and then store the data.
//!
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  23/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "bspConfig.h"
#include "app_util_platform.h"
#include "nrf_drv_uart.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_ppi.h"
#include <string.h>

/* FreeRTOS headers */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Private define section 0 --------------------------------------------------*/

/* The name of this module for debugger */
#define MODULE_NAME "UARTE"

/** Uarte RX buffer length */
#define UARTE_RX_BUFF_LEN  128u

/** Uarte TX buffer length */
#define UARTE_TX_BUFF_LEN  128u

/** Line end characters. Cpmment to skip. */
//#define LINE_END_CHARACTERS  "\r\n"

/** Maximum interval between frames in microseconds */
#define MAX_INTERFRAME_INTERVAL_US  200000u

/** Maximum UARTE tx timeout in system ticks */
#define MAX_UARTE_TX_TIMEOUT_MS    ((TickType_t)100u / portTICK_PERIOD_MS)

/** RX buffer ceils count */
#define UARTE_RX_BUF_CEILS 5u

/* Private typedef -----------------------------------------------------------*/
/* Private define section 1 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/** UARTE instance. */
static const nrf_drv_uart_t m_uart_instance = NRF_DRV_UART_INSTANCE(BSP_GPS_UARTE_INDEX);

/** TIMER instance. */
static const nrf_drv_timer_t m_tim_instance = NRF_DRV_TIMER_INSTANCE(BSP_GPS_TIM_INDEX);

/** PPI clear and start timer channel number. */
static nrf_ppi_channel_t ppi_clearStart_channel_number = (nrf_ppi_channel_t)0u; /* 0u never used to channels */

/** Static RX buffer with last zero character */
static uint8_t uarte_rx_buffer[UARTE_RX_BUFF_LEN + 1u];

/** Static TX buffer */
static uint8_t uarte_tx_buffer[UARTE_TX_BUFF_LEN];

/** Handle for UARTE TX done Semaphore */
static SemaphoreHandle_t uarte_tx_done_sem = (SemaphoreHandle_t) NULL;

/** Handle for UARTE RX done Semaphore */
static SemaphoreHandle_t uarte_rx_done_sem = (SemaphoreHandle_t) NULL;

/** UARTE circular buffer control structure */
static struct
{
  uint8_t data[UARTE_RX_BUF_CEILS][UARTE_RX_BUFF_LEN];  /*!< Data buffer */
  volatile uint8_t data_sizes[UARTE_RX_BUF_CEILS];      /*!< Buffer ceil data sizes */
  volatile uint8_t rx_w_index;                          /*!< Write index*/
  volatile uint8_t rx_r_index;                          /*!< Read index */
  volatile uint8_t rx_elements;                         /*!< Elemets received */
} uarte_circular_buffer_control;

/* Private functions ---------------------------------------------------------*/

/**
 * @brief UART interrupt event handler.
 *
 * @param[in] p_event    Pointer to event structure. Event is allocated on the stack so it is available
 *                       only within the context of the event handler.
 * @param[in] p_context  Context passed to interrupt handler, set on initialization.
 */
void nrf_uart_event_handler(nrf_drv_uart_event_t * p_event, void * p_context)
{
  UNUSED_VARIABLE(p_context);
  static portBASE_TYPE taskWoken = pdFALSE;

  switch(p_event->type)
  {
  case NRF_DRV_UART_EVT_TX_DONE: // Requested TX transfer completed.
    {
      if(xSemaphoreGiveFromISR(uarte_tx_done_sem, &taskWoken) != pdTRUE)
      {
        UTIL_DBG_WARN("TX Semaphore not taken before!");
      }
      portEND_SWITCHING_ISR(taskWoken);

      break;
    }
  case NRF_DRV_UART_EVT_RX_DONE: // Requested RX transfer completed.
    {
      /* Pause timer */
      nrf_drv_timer_pause(&m_tim_instance);
#ifdef LINE_END_CHARACTERS
      if (strstr((const char *)uarte_rx_buffer, LINE_END_CHARACTERS) != NULL)
      {
#endif
        /* End string received */

        /* Handle buffer and save it to cirbuff:
        * no sense to use critical region as it running under IRQ
        */
        /* Get indexes */
        uint8_t buff_w_index = uarte_circular_buffer_control.rx_w_index;
        uint8_t buff_rx_elements = uarte_circular_buffer_control.rx_elements;

        /* If indexes different */
        if (buff_rx_elements < UARTE_RX_BUF_CEILS)
        {
          uint8_t rx_amount = p_event->data.rxtx.bytes;
          memcpy(uarte_circular_buffer_control.data[buff_w_index], uarte_rx_buffer, rx_amount);
          uarte_circular_buffer_control.data_sizes[buff_w_index] = rx_amount;
          buff_w_index++;
          buff_rx_elements++;

          /* We out of array? */
          if (buff_w_index == UARTE_RX_BUF_CEILS)
          {
            buff_w_index = 0u;
          }
          uarte_circular_buffer_control.rx_w_index = buff_w_index;
          uarte_circular_buffer_control.rx_elements = buff_rx_elements;
        }
        else
        {
          UTIL_DBG_WARN("UART element lost!");
        }

        if(xSemaphoreGiveFromISR(uarte_rx_done_sem, &taskWoken) != pdTRUE)
        {
          UTIL_DBG_WARN("RX semaphore not taken before!");
        }

        portEND_SWITCHING_ISR(taskWoken);
      }
      /* Clear buffer and start RX again: the same as at error */
#ifdef LINE_END_CHARACTERS
    }
#endif
  case NRF_DRV_UART_EVT_ERROR:   // Error reported by UART peripheral.
    {
      /* Error: Start receiving again */
      memset(uarte_rx_buffer, 0, UARTE_RX_BUFF_LEN);
      nrf_drv_uart_rx(&m_uart_instance, uarte_rx_buffer, UARTE_RX_BUFF_LEN);
      break;
    }

  default:
    break;
  }
}

/**
 * @brief Timer driver event handler.
 *
 * @param[in] event_type Timer event.
 * @param[in] p_context  General purpose parameter set during initialization of
 *                       the timer. This parameter can be used to pass
 *                       additional information to the handler function, for
 *                       example, the timer ID.
 */
void nrf_timer_event_handler(nrf_timer_event_t event_type, void * p_context)
{
  UNUSED_VARIABLE(p_context);
  switch(event_type)
  {
  case NRF_TIMER_EVENT_COMPARE0:
    {
#ifdef LINE_END_CHARACTERS
      if (strstr((const char *)uarte_rx_buffer, LINE_END_CHARACTERS) != NULL)
      {
#endif
        /* End string received: Pause timer & abort receiving */
        nrf_drv_timer_pause(&m_tim_instance);
        nrf_drv_uart_rx_abort(&m_uart_instance);
#ifdef LINE_END_CHARACTERS
      }
#endif
    } break;
  default:
    break;
  }
}

/* Exported functions ------------------------------------------------------- */

/**@brief Initialize the hardware UARTE peripheral.
 *
 * @param  None.
 *
 * @retval NRF_SUCCESS         Initialization successful.
 * @retval NRF_ERROR_INTERNAL  If error occurred during initialization.
 */
ret_code_t bsp_uarte_init(void)
{
  ret_code_t retcode = NRF_ERROR_INTERNAL;

  /* Set last buffer element to zero to use strstr in safe way */
  uarte_rx_buffer[UARTE_RX_BUFF_LEN] = '\0';

  /* Circular buffer for uart */
  uarte_circular_buffer_control.rx_r_index = 0u;
  uarte_circular_buffer_control.rx_w_index = 0u;
  uarte_circular_buffer_control.rx_elements = 0u;

  /* Create RX & TX semaphores */
  uarte_tx_done_sem = xSemaphoreCreateBinary();
  uarte_rx_done_sem = xSemaphoreCreateBinary();
  if ((uarte_tx_done_sem != (SemaphoreHandle_t) NULL)
      && (uarte_rx_done_sem != (SemaphoreHandle_t) NULL))
  {

    /* Initialize UARTE peripheral */
    const nrf_drv_uart_config_t config =
    {
      .pseltxd            = BSP_PIN_GPS_TXD,
      .pselrxd            = BSP_PIN_GPS_RXD,
      .pselcts            = NRF_UART_PSEL_DISCONNECTED,
      .pselrts            = NRF_UART_PSEL_DISCONNECTED,
      .p_context          = NULL,
      .hwfc               = NRF_UART_HWFC_DISABLED,
      .parity             = NRF_UART_PARITY_EXCLUDED,
      .baudrate           = NRF_UART_BAUDRATE_115200,
      .interrupt_priority = APP_IRQ_PRIORITY_LOW,
      .use_easy_dma       = true
    };

    retcode = nrf_drv_uart_init(&m_uart_instance, &config, nrf_uart_event_handler);

    /* Initialize TIMER peripheral */
    if (retcode == NRF_SUCCESS)
    {
      const nrf_drv_timer_config_t tim_config =
      {
        .frequency          = NRF_TIMER_FREQ_1MHz,
        .mode               = NRF_TIMER_MODE_TIMER,
        .bit_width          = NRF_TIMER_BIT_WIDTH_32,
        .interrupt_priority = APP_IRQ_PRIORITY_LOW,
        .p_context          = NULL
      };
      retcode = nrf_drv_timer_init(&m_tim_instance, &tim_config, nrf_timer_event_handler);
    }

    /* Set PPI clear/start timer channel */
    if (retcode == NRF_SUCCESS)
    {
      retcode = nrf_drv_ppi_channel_alloc(&ppi_clearStart_channel_number);
      if (retcode == NRF_SUCCESS)
      {
        retcode = nrf_drv_ppi_channel_assign(ppi_clearStart_channel_number,
                                             nrf_drv_uart_event_address_get(&m_uart_instance, NRF_UART_EVENT_RXDRDY),
                                             nrf_drv_timer_task_address_get(&m_tim_instance, NRF_TIMER_TASK_CLEAR));
        if (retcode == NRF_SUCCESS)
        {
          retcode = nrf_drv_ppi_channel_fork_assign(ppi_clearStart_channel_number,
                                                    nrf_drv_timer_task_address_get(&m_tim_instance, NRF_TIMER_TASK_START));
        }
      }
    }

    if (retcode == NRF_SUCCESS)
    {
      /* Set timer C0 channel to 250 ms */
      nrf_drv_timer_compare(&m_tim_instance, NRF_TIMER_CC_CHANNEL0, MAX_INTERFRAME_INTERVAL_US, true);

      /* Set timer shorts: COMPARE0 to stop timer */
      nrf_timer_shorts_enable(m_tim_instance.p_reg, NRF_TIMER_SHORT_COMPARE0_STOP_MASK);

      /* Start receiving */
      nrf_drv_uart_rx(&m_uart_instance, uarte_rx_buffer, UARTE_RX_BUFF_LEN);

      retcode = nrf_drv_ppi_channel_enable(ppi_clearStart_channel_number);
    }
  }
  else
  {
    if (uarte_tx_done_sem != (SemaphoreHandle_t) NULL)
    {
      /* Delete TX done semaphore */
      vSemaphoreDelete(uarte_tx_done_sem);
    }
    if (uarte_rx_done_sem != (SemaphoreHandle_t) NULL)
    {
      /* Delete RX done semaphore */
      vSemaphoreDelete(uarte_rx_done_sem);
    }
  }

  return retcode;
}

/**@brief Uninitialize the hardware UARTE peripheral.
 *
 * @param  None.
 *
 * @retval NRF_SUCCESS         De-initialization successful.
 * @retval NRF_ERROR_INTERNAL  If error occurred during de-initialization.
 */
ret_code_t bsp_uarte_uninit(void)
{
  /* De-initialize hardware PPI, channel will be automaticaly disabled */
  ret_code_t retcode = nrf_drv_ppi_channel_free(ppi_clearStart_channel_number);

  if (retcode == NRF_SUCCESS)
  {
    /* De-initialize hardware UARTE */
    nrf_drv_uart_uninit(&m_uart_instance);

    /* De-initialize hardware TIMER */
    nrf_drv_timer_uninit(&m_tim_instance);

    /* Delete TX done semaphore */
    vSemaphoreDelete(uarte_tx_done_sem);

    /* Delete RX done semaphore */
    vSemaphoreDelete(uarte_rx_done_sem);
  }
  else
  {
    retcode = NRF_ERROR_INTERNAL;
  }

  return retcode;
}

/**@brief Send the packet.
 *
 * @param[in]   pData pointer to data.
 * @param[in]   dataSize data size.
 *
 * @retval NRF_SUCCESS               Data is set to UARTE DMA.
 * @retval NRF_ERROR_INVALID_LENGTH  Invalid data length.
 * @retval NRF_ERROR_BUSY            UARTE currently is unavailable (TX in progress).
 */
ret_code_t bsp_uarte_send(uint8_t * pData, uint8_t dataSize)
{
  ret_code_t retcode = NRF_ERROR_INVALID_LENGTH;
  if (dataSize <=  UARTE_TX_BUFF_LEN)
  {
    /* Size OK: get UARTE TX state */
    bool in_progress = nrf_drv_uart_tx_in_progress(&m_uart_instance);

    if (in_progress == false)
    {
      memcpy(uarte_tx_buffer, pData, dataSize);
      nrf_drv_uart_tx(&m_uart_instance, uarte_tx_buffer, dataSize);
      retcode = NRF_SUCCESS;
    }
    else
    {
      retcode = NRF_ERROR_BUSY;
    }
  }
  return retcode;
}

/**@brief Send the packet in blocking mode.
 *
 * @param[in]   pData pointer to data.
 * @param[in]   dataSize data size.
 * @param[in]   blockTime the maximum time for this data sending.
 *
 * @retval NRF_SUCCESS               Data is set to UARTE DMA.
 * @retval NRF_ERROR_INVALID_LENGTH  Invalid data length.
 * @retval NRF_ERROR_BUSY            UARTE currently is unavailable (TX in progress).
 */
ret_code_t bsp_uarte_sendBlocking(uint8_t * pData, uint8_t dataSize, uint32_t blockTime)
{
  ret_code_t retcode = NRF_ERROR_INVALID_LENGTH;
  if (dataSize <=  UARTE_TX_BUFF_LEN)
  {
    /* Size OK */
    /* Clear semaphore */
    xSemaphoreTake(uarte_tx_done_sem, (TickType_t) 0u);

    /* Get UARTE TX state */
    bool in_progress = nrf_drv_uart_tx_in_progress(&m_uart_instance);

    if (in_progress == true)
    {
      /* Wait until prev TX done */
      if (xSemaphoreTake(uarte_tx_done_sem, MAX_UARTE_TX_TIMEOUT_MS) == pdTRUE)
      {
        memcpy(uarte_tx_buffer, pData, dataSize);
        nrf_drv_uart_tx(&m_uart_instance, uarte_tx_buffer, dataSize);
        /* Wait until this transfer done */
        xSemaphoreTake(uarte_tx_done_sem, ((TickType_t)blockTime / portTICK_PERIOD_MS));
        retcode = NRF_SUCCESS;
      }
      else
      {
        retcode = NRF_ERROR_BUSY;
      }
    }
    else
    {
      memcpy(uarte_tx_buffer, pData, dataSize);
      nrf_drv_uart_tx(&m_uart_instance, uarte_tx_buffer, dataSize);
      /* Wait until this transfer done */
      xSemaphoreTake(uarte_tx_done_sem, ((TickType_t)blockTime / portTICK_PERIOD_MS));
      retcode = NRF_SUCCESS;
    }
  }
  return retcode;
}

/**@brief Peek the packet from fifo.
 *
 * @param[out]   pOutData pointer to pointer to data variable that will be overwritten.
 * @param[out]   pOutDataSize pointer to variable that will be overwritten by data size.
 *
 * @retval NRF_SUCCESS         Packet peeked.
 * @retval NRF_ERROR_NULL      Invalid pointer passed length.
 * @retval NRF_ERROR_NOT_FOUND No elements in queue.
 */
ret_code_t bsp_uarte_peek(uint8_t * * pOutData, uint8_t * pOutDataSize)
{
  ret_code_t retcode = NRF_ERROR_NULL;
  if ((pOutDataSize != (uint8_t *) NULL) && (pOutData != (uint8_t * *) NULL))
  {
    /* Critical region */
    taskENTER_CRITICAL();

    /* Get indexes */
    uint8_t buff_elements = uarte_circular_buffer_control.rx_elements;
    uint8_t buff_r_index = uarte_circular_buffer_control.rx_r_index;

    /* If read index not eq to the UARTE_RX_BUF_CEILS (initial) */
    if (buff_elements != 0u)
    {
      *pOutData = uarte_circular_buffer_control.data[buff_r_index];
      *pOutDataSize = uarte_circular_buffer_control.data_sizes[buff_r_index];
      retcode = NRF_SUCCESS;
    }
    else
    {
      retcode = NRF_ERROR_NOT_FOUND;
    }
    taskEXIT_CRITICAL();
  }
  return retcode;
}

/**@brief Get the size of available packets.
 *
 * @param   None.
 *
 * @retval The available packets in the fifo.
 */
size_t bsp_uarte_avails(void)
{
  size_t ret_size = 0u;
  /* Critical region */
  taskENTER_CRITICAL();

  /* Get size of elements */
  ret_size = uarte_circular_buffer_control.rx_elements;

  taskEXIT_CRITICAL();
  return ret_size;
}

/**@brief Get the packet from fifo.
 *
 * @param[out]   pBuff pointer to buffer that will be owerwritten with data.
 * @param[out]   pOutDataSize pointer to variable that will be overwritten by data size.
 * @param[in]    maxBuffDataSize the size of buffer that passed as argument.
 *
 * @retval NRF_SUCCESS         The packet has been written to buffer.
 * @retval NRF_ERROR_NULL      Invalid pointer or passed buffer length.
 * @retval NRF_ERROR_NOT_FOUND No elements in queue.
 */
ret_code_t bsp_uarte_get(uint8_t * pBuff, uint8_t * pOutDataSize, uint8_t maxBuffDataSize)
{
  ret_code_t retcode = NRF_ERROR_NULL;
  if ((pOutDataSize != (uint8_t *) NULL) && (pBuff != (uint8_t *) NULL) && (maxBuffDataSize >= UARTE_RX_BUFF_LEN))
  {
    /* Critical region */
    taskENTER_CRITICAL();

    /* Get the elements count */
    uint8_t q_rx_elements = uarte_circular_buffer_control.rx_elements;
    if (q_rx_elements != 0)
    {
      /* Get indexes and sizes*/
      uint8_t buff_r_index = uarte_circular_buffer_control.rx_r_index;
      uint8_t buff_r_dsize = uarte_circular_buffer_control.data_sizes[buff_r_index];
      memcpy(pBuff, uarte_circular_buffer_control.data[buff_r_index], buff_r_dsize);
      *pOutDataSize = buff_r_dsize;

      /* Increment index and decrement avail count */
      buff_r_index++;
      q_rx_elements--;

      if (buff_r_index == UARTE_RX_BUF_CEILS)
      {
        buff_r_index = 0u;
      }

      /* Write back */
      uarte_circular_buffer_control.rx_r_index = buff_r_index;
      uarte_circular_buffer_control.rx_elements = q_rx_elements;
      retcode = NRF_SUCCESS;
    }
    else
    {
      retcode = NRF_ERROR_NOT_FOUND;
    }
    taskEXIT_CRITICAL();
  }
  return retcode;
}

/**@brief Purge the first element in FIFO.
 *
 * @param   None.
 *
 * @retval NRF_SUCCESS         The packet has been purged.
 * @retval NRF_ERROR_NOT_FOUND No elements in queue.
 */
ret_code_t bsp_uarte_purgeFirst(void)
{
  ret_code_t retcode = NRF_ERROR_NOT_FOUND;

  /* Critical region */
  taskENTER_CRITICAL();

  /* Get the elements count */
  uint8_t q_rx_elements = uarte_circular_buffer_control.rx_elements;
  if (q_rx_elements != 0)
  {
    /* Get indexes and sizes*/
    uint8_t buff_r_index = uarte_circular_buffer_control.rx_r_index;

    /* Increment index and decrement avail count */
    buff_r_index++;
    q_rx_elements--;

    if (buff_r_index == UARTE_RX_BUF_CEILS)
    {
      buff_r_index = 0u;
    }

    /* Write back */
    uarte_circular_buffer_control.rx_r_index = buff_r_index;
    uarte_circular_buffer_control.rx_elements = q_rx_elements;
    retcode = NRF_SUCCESS;
    taskEXIT_CRITICAL();
  }
  return retcode;
}

/**@brief Get the packet from fifo.
 *
 * @param[out]   pBuff pointer to buffer that will be owerwritten with data.
 * @param[out]   pOutDataSize pointer to variable that will be overwritten by data size.
 * @param[in]    maxBuffDataSize the size of buffer that passed as argument.
 * @param[in]    blockTime the maximum time for this data sending.
 *
 * @retval NRF_SUCCESS         The packet has been written to buffer.
 * @retval NRF_ERROR_NULL      Invalid pointer or passed buffer length.
 * @retval NRF_ERROR_NOT_FOUND No elements in queue.
 */
ret_code_t bsp_uarte_getBlocking(uint8_t * pBuff, uint8_t * pOutDataSize, uint8_t maxBuffDataSize, uint32_t blockTime)
{
  /* Get avail elements */
  size_t avail = bsp_uarte_avails();

  if (avail == 0u)
  {
    /* Clear semaphore */
    xSemaphoreTake(uarte_rx_done_sem, (TickType_t) 0u);

    /* Wait for income data */
    xSemaphoreTake(uarte_rx_done_sem, ((TickType_t)blockTime / portTICK_PERIOD_MS));
  }

  ret_code_t retcode = bsp_uarte_get(pBuff, pOutDataSize, maxBuffDataSize);
  return retcode;
}

/**@brief Peek the packet from fifo in bloking mode.
 *
 * @param[out]   pOutData pointer to pointer to data variable that will be overwritten.
 * @param[out]   pOutDataSize pointer to variable that will be overwritten by data size.
 * @param[in]    blockTime the maximum time for this data sending.
 *
 * @retval NRF_SUCCESS         Packet peeked.
 * @retval NRF_ERROR_NULL      Invalid pointer passed length.
 * @retval NRF_ERROR_NOT_FOUND No elements in queue.
 */
ret_code_t bsp_uarte_peekBlocking(uint8_t * * pOutData, uint8_t * pOutDataSize, uint32_t blockTime)
{
  /* Get avail elements */
  size_t avail = bsp_uarte_avails();

  if (avail == 0u)
  {
    /* Clear semaphore */
    xSemaphoreTake(uarte_rx_done_sem, (TickType_t) 0u);

    /* Wait for income data */
    xSemaphoreTake(uarte_rx_done_sem, ((TickType_t)blockTime / portTICK_PERIOD_MS));
  }

  ret_code_t retcode = bsp_uarte_peek(pOutData, pOutDataSize);

  return retcode;
}

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
