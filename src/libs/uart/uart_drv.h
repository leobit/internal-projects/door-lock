//!*****************************************************************************
//!   @file    uart_drv.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    23/04/2018
//!   @brief   This C header file for intelligent hardware UARTE peripheral driver.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  23/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#ifndef _IUARTE_DRV_H_
#define _IUARTE_DRV_H_

/* Includes ------------------------------------------------------------------*/
#include "sdk_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/* Basic functions */
ret_code_t bsp_uarte_init(void);
ret_code_t bsp_uarte_uninit(void);

ret_code_t bsp_uarte_purgeFirst(void);
size_t bsp_uarte_avails(void);

/* TX APIs */
ret_code_t bsp_uarte_send(uint8_t * pData, uint8_t dataSize);
ret_code_t bsp_uarte_sendBlocking(uint8_t * pData, uint8_t dataSize, uint32_t blockTime);

/* RX APIs*/
ret_code_t bsp_uarte_peek(uint8_t * * pOutData, uint8_t * pOutDataSize);
ret_code_t bsp_uarte_get(uint8_t * pBuff, uint8_t * pOutDataSize, uint8_t maxBuffDataSize);

ret_code_t bsp_uarte_peekBlocking(uint8_t * * pOutData, uint8_t * pOutDataSize, uint32_t blockTime);
ret_code_t bsp_uarte_getBlocking(uint8_t * pBuff, uint8_t * pOutDataSize, uint8_t maxBuffDataSize, uint32_t blockTime);

#ifdef __cplusplus
}
#endif

#endif /* _MEMS_DRV_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
