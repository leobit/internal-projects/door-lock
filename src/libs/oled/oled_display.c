//******************************************************************************
//!  File description:
//!   @file    oled_display.c
//!   @author  Dmytro Kryvyi
//!   @version V1.0.0
//!   @date    26/06/2016
//!   @brief   This file includes the OLED driver.
//!
//!                     Copyright (C) 2016
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | ----------------
//!  24/07/2016 | Dmytro Kryvyi    | Initial draft
//!  02/08/2016 | Dmytro Kryvyi    | Beta
//!  30/08/2017 | Dmytro Kryvyi    | Bug fixes
//
//******************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "oled_display.h"
#include "twi_sw_master.h"
#include "app_util_platform.h"
#include "string.h"

/* Private typedef -----------------------------------------------------------*/
/* OLED information and control*/
typedef struct
{
  uint8_t oled_height;
  uint8_t oled_width;

  uint8_t oledWasInit;
  uint8_t current_contrast;
  device_orientation_t current_orientation;

} oled_settings;

/* Private define ------------------------------------------------------------*/

/* OLED I2C adress  */
#define SSD1306_I2C_ADDRESS   (0x78)

#define OLED_MIRROR_IMAGE   0

/* OLED resolution */

#define OLED_DEPTH_H    64
#define OLED_DEPTH_W    128

/* SSD1306 OLED control cmds */
/*            ABR                      |      Reg id     | Size    */
/* Fundamental commands */
#define SSD1306_SETCONTRAST                   0x81        /*  1b   */
#define SSD1306_DISPLAYALLON_RAM              0xA4        /*  0b   */
#define SSD1306_DISPLAYALLON_ENTRY            0xA5        /*  0b   */
#define SSD1306_NORMALDISPLAY                 0xA6        /*  0b   */
#define SSD1306_INVERTDISPLAY                 0xA7        /*  0b   */
#define SSD1306_DISPLAYOFF                    0xAE        /*  0b   */
#define SSD1306_DISPLAYON                     0xAF        /*  0b   */

/* Addressing Setting*/
#define SSD1306_SETLOWCOLUMN                  0u        /* 0b 0u-0x0F*/
#define SSD1306_SETHIGHCOLUMN                 0x10        /* 0b 0x10-0x1F*/
#define SSD1306_MEMORYMODE                    0x20        /* 1b 0u-0x03 (pdf)*/
#define SSD1306_COLUMNADDR                    0x21        /* 2b 0u-0x7F, 0u-0x7F (pdf)*/
#define SSD1306_PAGEADDR                      0x22        /* 2b 0u-0x07, 0u-0x07 (pdf)*/
#define SSD1306_PAGESTARTADDR                 0xB0        /* 0b 0xB0-0xB7 (pdf)*/

/* Configuration panel (resolution, layout)*/
#define SSD1306_SETSTARTLINE                  0x40        /* 0b 0x40-0x7F (pdf)*/
#define SSD1306_SEGREMAP                      0xA0        /* 0b 0xA0-0xA1 (pdf)*/
#define SSD1306_SETMULTIPLEX                  0xA8        /* 1b 0x10-0x3F (pdf)*/
#define SSD1306_COMSCANINC                    0xC0        /*  0b   */
#define SSD1306_COMSCANDEC                    0xC8        /*  0b   */
#define SSD1306_SETDISPLAYOFFSET              0xD3        /* 1b 0u-0x3F (pdf)*/
#define SSD1306_SETCOMPINS                    0xDA        /* 1b (pdf)*/
#define SSD1306_SETCOMPINS_ARG1_MSK           0x02        /* 1b 0u-0x3F (pdf)*/

/* Timing driving scheme */
#define SSD1306_CHARGE_PUMP                   0x8D        /* 1b en/dis ch pump(pdf)*/
#define SSD1306_CHARGE_PUMP_ARG_1             0x10
#define SSD1306_SETDISPLAYCLOCKDIV            0xD5        /* 1b 2 fields: 0:3 divider, 4:7 osc freq(pdf)*/
#define SSD1306_SETPRECHARGE                  0xD9        /* 1b 2 fields: 0:3 period 1 0x01-0x0F, 4:7 period 2 0x01 - 0x0F(pdf)*/
#define SSD1306_SETVCOMDESELECT               0xDB        /* 1b 2 fields: 4:6 deselect lev 0u-0x03 (pdf)*/
#define SSD1306_NOP                           0xE3        /*  0b   */

/* Scrolling */
#define SSD1306_RIGHT_COUNT_HORIZONTAL_SCROLL 0x26        /* 6b (pdf)*/
#define SSD1306_LEFT_COUNT_HORIZONTAL_SCROLL  0x27        /* 6b (pdf)*/
#define SSD1306_DEACTIVATE_SCROLL             0x2E
#define SSD1306_ACTIVATE_SCROLL               0x2F
#define SSD1306_SET_VERTICAL_SCROLL_AREA      0xA3
/* END OLED SS1306 control registers */

#define DELAY_MS(x)        (void)(x)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* OLED settings. */
static oled_settings current_oled;

/* OLED buffers. */
static uint8_t display_buffer [OLED_DEPTH_H * OLED_DEPTH_W / 8u];
static uint8_t display_buffer_fade [OLED_DEPTH_H * OLED_DEPTH_W / 8u];

/* TWI instance. */
//static const nrf_drv_twi_t m_twi_oled = NRF_DRV_TWI_INSTANCE(1);
static twi_sw_control_struct_t m_twi_oled =
{
  .scl_pin = 27u,
  .sda_pin = 26u,
  .initialized = false
};

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
* @brief TWI write specific adress.
*/
static void oled_i2c_write(uint8_t sl_adr, uint8_t reg, uint8_t * p_dat, uint32_t size)
{
  twi_master_write_reg(&m_twi_oled, sl_adr, reg, p_dat, size, true);
#if 0
  /* Workaround due to new I2C driver. */
  static uint8_t tempbuff[256u];
  uint32_t totalTranferBytes = (sizeof(reg) + size);
  if(totalTranferBytes <=  256u)
  {
    tempbuff[0u] = reg;
    if (size > 1u)
    {
      /* If multiple writing. */
      memcpy((void *)&tempbuff[1], p_dat, size);
    }
    else
    {
      tempbuff[1] = *p_dat;
    }

    nrf_drv_twi_tx(&m_twi_oled, sl_adr, (const uint8_t *) tempbuff, totalTranferBytes, true);
  }
#endif
}

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

/**
* @brief OLED IO initalizaton
*
* @param None
* @retval None
*/
void oled_ioInit(void)
{
  #if 0
  const nrf_drv_twi_config_t config =
  {
    .scl                = 27u,
    .sda                = 26u,
    .frequency          = NRF_TWI_FREQ_400K,
    .interrupt_priority = APP_IRQ_PRIORITY_LOW,
    .clear_bus_init     = true
  };

  ret_code_t err_code = nrf_drv_twi_init(&m_twi_oled, &config, NULL, NULL);
  APP_ERROR_CHECK(err_code);

  nrf_drv_twi_enable(&m_twi_oled);
  #endif

  twi_master_init(&m_twi_oled);
}

/**
* @brief Send OLED command
*
* @param  cmd - command
* @retval None
*/
static void oled_command(uint8_t cmd)
{
  oled_i2c_write(SSD1306_I2C_ADDRESS, 0u, &cmd, 1u);
}

/**
* @brief Upload buffer to OLED
*
* @param  None
* @retval None
*/
void oled_update(void)
{
  oled_command(SSD1306_COLUMNADDR);
  oled_command(0u);
  oled_command(current_oled.oled_width - 1u);

  oled_command(SSD1306_PAGEADDR);
  oled_command(0u);
  oled_command((current_oled.oled_height / 8u) - 1u);

  uint8_t * disp_pointer = display_buffer;
  uint8_t i = current_oled.oled_height / 8u;

  /* Page Start default address. */
  uint8_t cmdPS = SSD1306_PAGESTARTADDR;
  while(i--)
  {
    /* Set Page Start and increment */
    oled_command(cmdPS++);
    oled_i2c_write(SSD1306_I2C_ADDRESS, 0x40, disp_pointer, current_oled.oled_width);
    disp_pointer += current_oled.oled_width;
  }
}

/**
* @brief Upload buffer to OLED
*
* @param  flag - true: invert, false: normal
* @retval None
*/
void oled_invertDisplay(bool flag)
{
  if(flag == true)
  {
    oled_command(SSD1306_INVERTDISPLAY);
  }
  else
  {
    oled_command(SSD1306_NORMALDISPLAY);
  }
}

/**
* @brief Set left handed scroll for rows
*
* @param  start - start row, end - end row, oled_scrol_frame - frame speed
* @retval None
*/
void oled_setscrollright(uint8_t start, uint8_t end, oled_scrol_frame_t frames)
{
  oled_command(SSD1306_RIGHT_COUNT_HORIZONTAL_SCROLL);
  oled_command(0u);
  oled_command(start);
  oled_command(frames);
  oled_command(end);
  oled_command(0u);
  oled_command(0xFF);
}

/**
* @brief Set right handed scroll for rows
*
* @param  start - start row, end - end row, oled_scrol_frame - frame speed
* @retval None
*/
void oled_setscrollleft(uint8_t start,  uint8_t end, oled_scrol_frame_t frames)
{
  oled_command(SSD1306_LEFT_COUNT_HORIZONTAL_SCROLL);
  oled_command(0u);
  oled_command(start);
  oled_command(frames);
  oled_command(end);
  oled_command(0u);
  oled_command(0xFF);
}

/**
* @brief Set vertical scroll
*
* @param  start - start row, end - end row
* @retval None
*/
void oled_setvert_scrol(uint8_t start, uint8_t end)
{
  oled_command(SSD1306_SET_VERTICAL_SCROLL_AREA);
  oled_command(start);
  oled_command(end);
}

/**
* @brief Stop scroling
*
* @param  None
* @retval None
*/
void oled_stopscroll(void)
{
  oled_command(SSD1306_DEACTIVATE_SCROLL);
}

/**
* @brief Start scrolling
*
* @param  None
* @retval None
*/
void oled_startscroll(void)
{
  oled_command(SSD1306_ACTIVATE_SCROLL);
}

/**
* @brief LCD initalizaton
*
* @param font - font that will be use
* @retval 1: if succes
*/
bool oled_init(void)
{
  current_oled.oledWasInit = true;
  current_oled.oled_height = OLED_DEPTH_H;
  current_oled.oled_width = OLED_DEPTH_W;
  current_oled.current_orientation = DEVICE_ORIENT_LANDSCAPE_TOP;
  current_oled.current_contrast = 0xFF;

  oled_command(SSD1306_DISPLAYOFF);  /* Power off */

  /* Timing & Driving*/
  oled_command(SSD1306_SETDISPLAYCLOCKDIV); /* Clock divide/Ratio */
  oled_command(0x80);

  oled_command(SSD1306_SETPRECHARGE);  /* Pre charge period */
  oled_command(0x44);

  oled_command(SSD1306_SETVCOMDESELECT);  /* Deselect level */
  oled_command(0u);

  /* Hardware config */
  oled_command(SSD1306_SETSTARTLINE | 0u); /* Display Start line */
  oled_command(SSD1306_SEGREMAP | 1u); /* Segment remap */

  oled_command(SSD1306_SETMULTIPLEX); /* Multiplex Ratio */
  oled_command(current_oled.oled_height - 1);

#if (OLED_MIRROR_IMAGE != 0)
  oled_command(SSD1306_COMSCANINC); /* Set common increment */
#else
  oled_command(SSD1306_COMSCANDEC); /* Set common decrement*/
#endif

  oled_command(SSD1306_SETDISPLAYOFFSET); /* Display offset */
  oled_command(0u);

  oled_command(SSD1306_SETCOMPINS); /* Set COM Pins Hardware Configuration*/
  oled_command(SSD1306_SETCOMPINS_ARG1_MSK | 0u);

  oled_command(SSD1306_SETCOMPINS); /* Set COM Pins Hardware Configuration*/
  oled_command(SSD1306_SETCOMPINS_ARG1_MSK | 0x12);

  /* Adressing mode*/
  oled_command((SSD1306_SETLOWCOLUMN | 0u)); /* Set Lower Column Start Address for Page Addressing Mode */
  oled_command(SSD1306_SETHIGHCOLUMN | current_oled.oled_height / 8); /* Set Higher Column Start Address for Page Addressing Mode */

  oled_command(SSD1306_MEMORYMODE); /* Set addressing mode */
  oled_command(0x02); /* Page addr*/

  /* Fundamental CMD */
  oled_command(SSD1306_SETCONTRAST);  /* Set contrast */
  oled_command(current_oled.current_contrast);

  oled_command(SSD1306_DISPLAYALLON_RAM);  /* Entrie display ON */
  oled_command(SSD1306_NORMALDISPLAY);  /* Normal display/Inverse */

  oled_clear();
  oled_update();

  oled_command(SSD1306_CHARGE_PUMP); /* Charge pump enable*/
  oled_command(SSD1306_CHARGE_PUMP_ARG_1 | 4u);

  oled_command(SSD1306_DISPLAYON);  /* Power on */

  return true;
}

/**
* @brief LCD power off
*
* @param None
* @retval None
*/
void oled_deinitialize(void)
{
  oled_command(SSD1306_DISPLAYOFF);  /* Power off */
 // DELAY_MS(10);
 // nrf_drv_twi_uninit(&m_twi_oled);
}

/**
* @brief LCD power off
*
* @param None
* @retval None
*/
void oled_standby(void)
{
  oled_command(SSD1306_DISPLAYOFF);  /* Power off */
}

/**
* @brief Set oled contrast
*
* @param oled contrast
* @retval None
*/
void oled_setContrast(uint8_t contrast)
{
  oled_command(SSD1306_SETCONTRAST);  /* Set contrast */
  oled_command(contrast);
  current_oled.current_contrast = contrast;
}

/**
* @brief Enter to sleep mode
*
* @param  None
* @retval None
*/
void oled_setSleepMode(void)
{
  oled_command(SSD1306_DISPLAYOFF);  /* Power off */
}

/**
* @brief Wake up from sleep mode
*
* @param  None
* @retval None
*/
void oled_setActiveMode(void)
{
  oled_command(SSD1306_DISPLAYON);  /* Power on */
}

/**
* @brief Put pixel to OLED buffer
*
* @param  x and y coordinates, color
* @retval None
*/
void oled_putPix(uint8_t x, uint8_t y, oled_cl_t color)
{
  uint32_t oled_index = ((current_oled.oled_width * (y / 8)) + x);
  if((oled_index < sizeof(display_buffer)) && (current_oled.oled_width > x))
  {
    uint8_t mask = (1 << (y % 8));

    switch(color)
    {
    case OLED_COLOR_BLACK:
      display_buffer[oled_index] =  display_buffer[oled_index] & (~(unsigned)mask);
      break;

    case OLED_COLOR_INVERSE:
      display_buffer[oled_index] ^= mask;
      break;

    default:
      display_buffer[oled_index] |= mask;
    }
  }
}

/**
* @brief Get pixel to OLED buffer
*
* @param  x and y coordinates
* @retval Pixel color
*/
oled_cl_t oled_getPix(uint8_t x, uint8_t y)
{
  uint32_t oled_index = ((current_oled.oled_width * (y / 8u)) + x);
  if((oled_index < sizeof(display_buffer)) && (current_oled.oled_width > x))
  {
    uint8_t mask = (1 << (y % 8));

    if((display_buffer[oled_index] & mask) != 0u)
    {
      return OLED_COLOR_WHITE;
    }
  }
  return OLED_COLOR_BLACK;
}

/**
* @brief OLED clear buffer
*
* @param  None
* @retval None
*/
void oled_clear(void)
{
  memset((void *) display_buffer, 0, sizeof(display_buffer));
}

/**
* @brief Upload buffer to OLED
*
* @param  None
* @retval None
*/
void oled_write_direct_buffer(uint8_t xs, uint8_t xe, uint8_t ys, uint8_t ye, uint8_t * buff_ptr)
{
  xs = (xs >  current_oled.oled_width ? 0 : xs);
  xe = (xe >  current_oled.oled_width ?  current_oled.oled_width : xe);
  ys = ((ys > current_oled.oled_height ? 0 : ys) / 8u);
  if((xs < xe)  && (ys < ye))
  {
    oled_command(SSD1306_COLUMNADDR);
    oled_command(xs);
    oled_command(xe - 1);
    oled_command(SSD1306_PAGEADDR);
    oled_command(ys);
    uint8_t yend = ye / 8;
    if(yend > ((current_oled.oled_height / 8) - 1))
    {
      yend = ((current_oled.oled_height / 8) - 1);
    }
    oled_command(yend);

    uint8_t * disp_pointer;

    if(ys < 8u)
    {
      disp_pointer = buff_ptr + xs;
      oled_command(SSD1306_PAGESTARTADDR); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer, xe - xs);
    }

    if((ys < 16u) && (ye >= 8u))
    {
      disp_pointer = buff_ptr +  current_oled.oled_width + xs;
      oled_command(SSD1306_PAGESTARTADDR | 1u); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer,  xe - xs);
    }
#if ( OLED_DEPTH_H > 16u)
    if((ys < 24u) && (ye >= 16u))
    {
      disp_pointer = buff_ptr + ( current_oled.oled_width * 2u) + xs;
      oled_command(SSD1306_PAGESTARTADDR | 2u); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer,  xe - xs);
    }

    if((ys < 32u) && (ye >= 24u))
    {
      disp_pointer = buff_ptr + ( current_oled.oled_width * 3u) + xs;
      oled_command(SSD1306_PAGESTARTADDR | 3u); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer,  xe - xs);
    }
#endif

#if ( OLED_DEPTH_H > 32u)
    if((ys < 40u) && (ye >= 32u))
    {
      disp_pointer = buff_ptr + ( current_oled.oled_width * 4u) + xs;
      oled_command(SSD1306_PAGESTARTADDR | 4u); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer,  xe - xs);
    }

    if((ys < 48u) && (ye >= 40u))
    {
      disp_pointer = buff_ptr + ( current_oled.oled_width * 5u) + xs;
      oled_command(SSD1306_PAGESTARTADDR | 5u); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer,  xe - xs);
    }

    if((ys < 56u) && (ye >= 48u))
    {
      disp_pointer = buff_ptr + ( current_oled.oled_width * 6u) + xs;
      oled_command(SSD1306_PAGESTARTADDR | 6u); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer,  xe - xs);
    }

    if((ys < 64u) && (ye >= 56u))
    {
      disp_pointer = buff_ptr + ( current_oled.oled_width * 7u) + xs;
      oled_command(SSD1306_PAGESTARTADDR | 7u); /* Set Page Start */
      oled_i2c_write(SSD1306_I2C_ADDRESS,0x40, disp_pointer,  xe - xs);
    }
#endif
  }
}

/**
* @brief draw filled rectangle by come color
*
* @param position(x,y), offset(_offs, y_offs) and color
* @retval None
*/
void oled_fill_rect(uint8_t x, uint8_t y, uint8_t x_offs, uint8_t y_offs, oled_cl_t color)
{
  if((current_oled.oled_height > y) && (current_oled.oled_width > x))
  {
    if(current_oled.oled_height < (y + y_offs))
    {
      y_offs = (current_oled.oled_height - y);
    }

    if(current_oled.oled_width < (x + x_offs))
    {
      x_offs = (current_oled.oled_width - x);
    }

    uint8_t state_flag = 0u;
    uint8_t mask;
    while(y_offs)
    {
      if(y % 8u)
      {
	mask = (uint8_t) ~((1u << (y % 8u)) - 1u);
	if((y_offs + (y % 8u)) <= 8u) // if y_offs + y not going throw row
	{
	  mask = (mask & ((uint8_t)((1u << (y_offs + (y % 8u))) - 1u)));
	}
	state_flag = 0u;
      }
      else
      {
	if(((y % 8u) == 0u) && (y_offs > 7u))
	{
	  mask = 0xFF;
	  state_flag = 1u;
	}
	else
	{
	  if( y_offs % 8u)
	  {
	    mask = (uint8_t) ((1u << (y_offs % 8u)) - 1u);
	    state_flag = 2u;
	  }
	}
      }


      uint32_t oled_index = (current_oled.oled_width * (y / 8u));

      if((uint32_t)(oled_index) > sizeof(display_buffer))
      {
	break;
      }

      switch(color)
      {
      case OLED_COLOR_BLACK:
	for(uint32_t i = x; (i < (uint32_t)(x + x_offs)) && (i < (uint32_t)current_oled.oled_width); i++)
	{
	  display_buffer[oled_index + i] = display_buffer[oled_index + i] & (~(unsigned)mask);
	}
	break;

      case OLED_COLOR_INVERSE:
	for(uint32_t i = x; (i < (uint32_t)(x + x_offs)) && (i < (uint32_t)current_oled.oled_width); i++)
	{
	  display_buffer[oled_index + i] ^= mask;
	}
	break;

      default:
	for(uint32_t i = x; (i < (uint32_t)(x + x_offs)) && (i < (uint32_t)current_oled.oled_width); i++)
	{
	  display_buffer[oled_index + i] |= mask;
	}
      }

      switch(state_flag)
      {
      case 0u:
	{
	  if(y_offs >= (8u - (y % 8u)))
	  {
	    y_offs -= (8u - (y % 8u));
	  }
	  else
	  {
	    y_offs = 0u;
	  }
	  y += (8u - (y % 8u));
	  break;
	}

      case 1u:
	{
	  y_offs -= 8u;
	  y += 8u;
	  break;
	}
      default:
	{
	  y_offs = 0u;
	  break;
	}
      }
    }
  }
}

/**
* @brief return oled height
*
* @param None
* @retval Height
*/
uint8_t oled_getHeight(void)
{
  switch(current_oled.current_orientation)
  {
  case DEVICE_ORIENT_PORTRAIT_RIGHT:
  case DEVICE_ORIENT_PORTRAIT_LEFT:
    return current_oled.oled_width;
  default:
    return current_oled.oled_height;
  }
}

/**
* @brief return oled width
*
* @param None
* @retval oled Width
*/
uint8_t oled_getWidth(void)
{
  switch(current_oled.current_orientation)
  {
  case DEVICE_ORIENT_PORTRAIT_RIGHT:
  case DEVICE_ORIENT_PORTRAIT_LEFT:
    return current_oled.oled_height;
  default:
    return current_oled.oled_width;
  }
}

/**
* @brief Set current oled orientation
*
* @param oled orientation
* @retval None
*/
bool oled_setOrientation(device_orientation_t newOrientation)
{
  (void) newOrientation;
  current_oled.current_orientation = DEVICE_ORIENT_LANDSCAPE_TOP;
  return false;
}

/**
* @brief return current oled orientation
*
* @param None
* @retval oled orientation
*/
device_orientation_t oled_getOrientation(void)
{
  return current_oled.current_orientation;
}

/**
* @brief Buferize OLED buffer
*
* @param  None
* @retval None
*/
void oled_bufferize(void)
{
  memcpy((void*) display_buffer_fade, (const void*) display_buffer, sizeof(display_buffer_fade));
}

/**
* @brief Update display area with coordinates
*
* @param position(x,y), offset(_offs, y_offs)
* @retval None
*/
void oled_update_area(uint8_t x, uint8_t y, uint8_t x_offs, uint8_t y_offs)
{
  oled_write_direct_buffer(x, x + x_offs, y, y + y_offs, display_buffer);
}

/**
* @brief Drag in oled capture
*
* @param  None
* @retval None
*/
void oled_drag_in(oled_fade_type ftype, uint8_t pixels)
{
  if(pixels != 0)
  {
    switch(ftype)
    {
    case OLED_DRAG_LEFT:
      {
        if(pixels >  current_oled.oled_width)
        {
          break;
        }
        for(uint8_t x = 0u; x <  current_oled.oled_width; x += ((pixels << 2u) > (current_oled.oled_width - x) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels))
        {
          oled_write_direct_buffer(( current_oled.oled_width - 1u) - x, current_oled.oled_width - (x / ( current_oled.oled_width / 10u)), 0u,current_oled.oled_height, display_buffer);
        }
        break;
      }
    case OLED_DRAG_LEFT_SCR:
      {
        if(pixels >  current_oled.oled_width)
        {
          break;
        }
        for(uint8_t x = 0u; x <  current_oled.oled_width; x +=  ((pixels << 2u) > (current_oled.oled_width - x) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels))
        {
          for(uint8_t y = 0u; y < (current_oled.oled_height / 8u); y++)
          {
            memcpy((void*)(display_buffer_fade + ( current_oled.oled_width * y) + ( current_oled.oled_width - x)),(const void*)(display_buffer + ( current_oled.oled_width * y)),x);
          }
          oled_write_direct_buffer(( current_oled.oled_width - 1u) - x, current_oled.oled_width, 0u, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }
    case OLED_DRAG_LEFT_SWITCH:
      {
        if(pixels >  current_oled.oled_width)
        {
          break;
        }

        for(uint8_t x = 0u; x < current_oled.oled_width; x += ((pixels << 2u) > (current_oled.oled_width - x) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels))
        {
          for(uint8_t y = 0u; y < (current_oled.oled_height / 8u); y++)
          {
            uint8_t * base_row_pointer = (display_buffer_fade + ( current_oled.oled_width * y));
            memcpy((void*)base_row_pointer, (const void*) (base_row_pointer + pixels), current_oled.oled_width - pixels);
            memcpy((void*)(base_row_pointer + (current_oled.oled_width - x)),(const void*)(display_buffer + ( current_oled.oled_width * y)), x);
          }
          oled_write_direct_buffer(0u, current_oled.oled_width, 0u, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }
    case OLED_DRAG_UP:
      {
        if(pixels >  current_oled.oled_height)
        {
          break;
        }

      for(uint8_t y_d = pixels; y_d <= current_oled.oled_height; y_d += ((pixels << 2u) > (current_oled.oled_height - y_d) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels)) // All display
        {
          for(uint8_t x_d = 0u; x_d < current_oled.oled_width; x_d++)
          {
            /* Drag image to fade buffer from display buffer */
            for(uint8_t y_l = 0u; y_l < y_d; y_l++)
            {
              uint32_t oled_get_index = ((current_oled.oled_width * (y_l / 8u)) + x_d);
              uint8_t get_mask = (1u << ( y_l % 8u));
              if(display_buffer[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_get_index] |= get_mask;
              }
              else
              {
                display_buffer_fade[oled_get_index] = display_buffer_fade[oled_get_index] & (~(unsigned)get_mask);
              }
            }
          }
          oled_write_direct_buffer(0u, current_oled.oled_width, y_d, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }

    case OLED_DRAG_UP_SCR:
      {
        if(pixels >  current_oled.oled_height)
        {
          break;
        }

        for(uint8_t y_d = pixels; y_d <= current_oled.oled_height; y_d += ((pixels << 2u) > (current_oled.oled_height - y_d) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels)) // All display
        {
          for(uint8_t x_d = 0u; x_d < current_oled.oled_width; x_d++)
          {
            /* Drag image to fade buffer from display buffer */
            for(uint8_t y_l = 0u; y_l < y_d; y_l++)
            {
              uint32_t oled_get_index = ((current_oled.oled_width * (((current_oled.oled_height - y_d) + y_l) / 8u)) + x_d);
              uint8_t get_mask = (1u << (((current_oled.oled_height - y_d) + y_l) % 8u));

              uint32_t oled_index_put = ((current_oled.oled_width * (y_l / 8u)) + x_d);
              uint8_t mask_put = (1u << (y_l % 8u));

              if(display_buffer[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_index_put] |= mask_put;
              }
              else
              {
                display_buffer_fade[oled_index_put] =  display_buffer_fade[oled_index_put] & (~(unsigned)mask_put);
              }
            }
          }
          oled_write_direct_buffer(0u,  current_oled.oled_width, y_d, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }

    case OLED_DRAG_UP_SWITCH:
      {
        if(pixels >  current_oled.oled_height)
        {
          break;
        }

        for(uint8_t y_d = pixels; y_d <= current_oled.oled_height; y_d += ((pixels << 2u) > (current_oled.oled_height - y_d) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels)) // All display
        {
          for(uint8_t x_d = 0u; x_d < current_oled.oled_width; x_d++)
          {
            /* Drag image on fade buffer */
            for(uint8_t y_l = (current_oled.oled_height - 1u); y_l >= y_d; y_l--)
            {
              uint32_t oled_get_index =  ((current_oled.oled_width * ((y_l - pixels) / 8u)) + x_d);
              uint8_t get_mask = (1u << ((y_l - pixels) % 8u));

              uint32_t oled_index_put = ((current_oled.oled_width * ( y_l / 8u)) + x_d);
              uint8_t mask_put = (1u << ( y_l % 8u));

              if(display_buffer_fade[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_index_put] |= mask_put;
              }
              else
              {
                display_buffer_fade[oled_index_put] =  display_buffer_fade[oled_index_put] & (~(unsigned)mask_put);
              }
            }

            /* Drag image to fade buffer from display buffer */
            for(uint8_t y_l = 0u; y_l < y_d; y_l++)
            {
              uint32_t oled_get_index = ((current_oled.oled_width * (((current_oled.oled_height - y_d) + y_l) / 8u)) + x_d);
              uint8_t get_mask = (1u << (((current_oled.oled_height - y_d) + y_l) % 8u));

              uint32_t oled_index_put = ((current_oled.oled_width * (y_l / 8u)) + x_d);
              uint8_t mask_put = (1u << (y_l % 8u));

              if(display_buffer[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_index_put] |= mask_put;
              }
              else
              {
                display_buffer_fade[oled_index_put] =  display_buffer_fade[oled_index_put] & (~(unsigned)mask_put);
              }
            }
          }
          oled_write_direct_buffer(0u, current_oled.oled_width, 0u, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }

    case OLED_DRAG_DOWN:
      {
        if(pixels >  current_oled.oled_height)
        {
          break;
        }

        for(uint8_t y_d = pixels; y_d <= current_oled.oled_height; y_d += ((pixels << 2u) > (current_oled.oled_height - y_d) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels)) // All display
        {
          for(uint8_t x_d = 0u; x_d < current_oled.oled_width; x_d++)
          {
            /* Drag image to fade buffer from display buffer */
            for(uint8_t y_l = (current_oled.oled_height - y_d); y_l < current_oled.oled_height; y_l++)
            {
              uint32_t oled_get_index = ((current_oled.oled_width * ( y_l / 8u)) + x_d);
              uint8_t get_mask = (1u << (y_l % 8u));

              if(display_buffer[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_get_index] |= get_mask;
              }
              else
              {
                display_buffer_fade[oled_get_index] =  display_buffer_fade[oled_get_index] & (~(unsigned)get_mask);
              }
            }
          }
          oled_write_direct_buffer(0u, current_oled.oled_width, 0u, current_oled.oled_height, display_buffer_fade);
        }

        break;
      }
    case OLED_DRAG_DOWN_SCR:
      {
        if(pixels >  current_oled.oled_height)
        {
          break;
        }

        for(uint8_t y_d = pixels; y_d <= current_oled.oled_height; y_d += ((pixels << 2u) > (current_oled.oled_height - y_d) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels)) // All display
        {
          for(uint8_t x_d = 0u; x_d < current_oled.oled_width; x_d++)
          {
            /* Drag image to fade buffer from display buffer */
            for(uint8_t y_l = (current_oled.oled_height - y_d); y_l < current_oled.oled_height; y_l++)
            {
              uint32_t oled_index_put = ((current_oled.oled_width * ( y_l / 8u)) + x_d);
              uint8_t mask_put = (1u << (y_l % 8u));

              uint32_t oled_get_index = ((current_oled.oled_width * (( y_l -(current_oled.oled_height - y_d)) / 8u)) + x_d);
              uint8_t get_mask = (1u << ((y_l - (current_oled.oled_height - y_d)) % 8u));

              if(display_buffer[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_index_put] |= mask_put;
              }
              else
              {
                display_buffer_fade[oled_index_put] =  display_buffer_fade[oled_index_put] & (~(unsigned)mask_put);
              }
            }
          }

          oled_write_direct_buffer(0u, current_oled.oled_width, y_d, current_oled.oled_height, display_buffer_fade);
        }

        break;
      }

    case OLED_DRAG_DOWN_SWITCH:
      {
        if(pixels >  current_oled.oled_height)
        {
          break;
        }
        for(uint8_t y_d = pixels; y_d <= current_oled.oled_height; y_d += ((pixels << 2u) > (current_oled.oled_height - y_d) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels)) // All display
        {
          for(uint8_t x_d = 0u; x_d < current_oled.oled_width; x_d++)
          {
            /* Drag image on fade buffer */
            for(uint8_t y_l = 0u; y_l < (current_oled.oled_height - y_d); y_l++)
            {
              uint32_t oled_get_index =  ((current_oled.oled_width * ((y_l + pixels) / 8u)) + x_d);
              uint8_t get_mask = (1u << ((y_l + pixels) % 8u));

              uint32_t oled_index_put = ((current_oled.oled_width * (y_l / 8u)) + x_d);
              uint8_t mask_put = (1u << ( y_l % 8u));

              if(display_buffer_fade[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_index_put] |= mask_put;
              }
              else
              {
                display_buffer_fade[oled_index_put] =  display_buffer_fade[oled_index_put] & (~(unsigned)mask_put);
              }
            }

            /* Drag image to fade buffer from display buffer */
            for(uint8_t y_l = 0u; y_l < y_d; y_l++)
            {
              uint32_t oled_index_put = ((current_oled.oled_width * ((current_oled.oled_height - y_d + y_l ) / 8u)) + x_d);
              uint8_t mask_put = (1u << ((current_oled.oled_height - y_d + y_l) % 8u));

              uint32_t oled_get_index = ((current_oled.oled_width * (y_l / 8u)) + x_d);
              uint8_t get_mask = (1u << (y_l % 8u));
              if(display_buffer[oled_get_index] & get_mask)
              {
                display_buffer_fade[oled_index_put] |= mask_put;
              }
              else
              {
                display_buffer_fade[oled_index_put] =  display_buffer_fade[oled_index_put] & (~(unsigned)mask_put);
              }
            }
          }
          oled_write_direct_buffer(0u, current_oled.oled_width, 0u, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }

    case OLED_DRAG_RIGHT:
      {
        if(pixels >  current_oled.oled_width)
        {
          break;
        }

        for(uint8_t x = 0u; x <  current_oled.oled_width; x += ((pixels << 2u) > (current_oled.oled_width - x) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels))
        {
          oled_write_direct_buffer((x / ( current_oled.oled_width / 10u)), x, 0u,current_oled.oled_height, display_buffer);
        }
        break;
      }
    case OLED_DRAG_RIGHT_SWITCH:
      {
        if(pixels >  current_oled.oled_width)
        {
          break;
        }
        uint8_t old_x = 0u;
        for(uint8_t x = 0u; x < current_oled.oled_width; x += (((pixels << 2) > (current_oled.oled_width - x) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels)))
        {
          for(uint8_t y = 0u; y < (current_oled.oled_height / 8u); y++)
          {
            uint8_t * disply_w_p = ((display_buffer_fade + ( current_oled.oled_width * (y + 1u))) - 1u);
            uint8_t * display_x = disply_w_p - pixels;
            uint8_t * act_display_p = ((display_buffer + ( current_oled.oled_width * (y + 1u))) - 1u - old_x);

            while(disply_w_p >= (display_buffer_fade + (current_oled.oled_width * y)))
            {
              if(display_x >= (display_buffer_fade + (current_oled.oled_width * y)))
              {
                *disply_w_p-- = *display_x--;
              }
              else
              {
                *disply_w_p-- = *act_display_p--;
              }
            }
          }
          old_x = x;
          oled_write_direct_buffer(0u, current_oled.oled_width, 0u, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }
    case OLED_DRAG_RIGHT_SCR:
      {
        if(pixels >  current_oled.oled_width)
        {
          break;
        }

        for(uint8_t x = 0u; x <  current_oled.oled_width; x += ((pixels << 2) > (current_oled.oled_width - x) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels))
        {
          for(uint8_t y = 0u; y < (current_oled.oled_height / 8u); y++)
          {
            memcpy((void*)(display_buffer_fade + ( current_oled.oled_width * y)),(const void*)(display_buffer + ( current_oled.oled_width * y) + ( current_oled.oled_width - 1u - x)),x);
          }
          oled_write_direct_buffer(0u, x, 0u, current_oled.oled_height, display_buffer_fade);
        }
        break;
      }
    case OLED_DRAG_FROM_CENTER:
      {
        for(uint8_t x = 0u; x < (current_oled.oled_width / 2u); x += ((pixels << 2u) > (current_oled.oled_width - x) ? (pixels /= (pixels >= 2u ? 2u : 1u)) : pixels))
        {
          oled_write_direct_buffer((current_oled.oled_width / 2u) - x, ( current_oled.oled_width / 2u) + x, 0u, current_oled.oled_height, display_buffer);
        }
        break;
      }
    case OLED_DRAG_TO_CENTER:
      {
        for(uint8_t x = 0u; x < (current_oled.oled_width / 2u); x += pixels)
        {
          oled_write_direct_buffer(0u, x, 0u, current_oled.oled_height, display_buffer);
          oled_write_direct_buffer(current_oled.oled_width - x,  current_oled.oled_width, 0u, current_oled.oled_height, display_buffer);
        }
        break;
      }
    }
  }
  oled_update();
}
/*****************************************************************************/
/*                           End of file                                     */
/*****************************************************************************/
