//******************************************************************************
//!  File description:
//!   @file    fonts.h
//!   @author  Dmytro Kryvyi
//!   @version V1.0.0
//!   @date    18/02/2014
//!   @brief   Header for fontsxx.c files
//!
//!                     Copyright (C) 2014
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | ----------------
//!  18/02/2014 | Dmytro Kryvyi    | Initial draft
//
//******************************************************************************

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FONTS_H
#define __FONTS_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/** @addtogroup Utilities
  * @{
  */
  

/** @addtogroup Common
  * @{
  */

/** @addtogroup FONTS
  * @{
  */ 

/** @defgroup FONTS_Exported_Types
  * @{
  */ 

/* Image crossing*/ 
typedef enum
{
  imag_left_to_right,
  imag_top_to_bottom    
} imag_cross_t;
      
/* Font type */   
typedef struct
{    
  const uint8_t *table;
  uint16_t Width;
  uint16_t Height;
  imag_cross_t cross;
  uint8_t char_offset;
  uint8_t char_end;
  uint16_t fontBufferWidth;
} font_type_t;

/* Fonts*/
extern const font_type_t font17x24;
extern const font_type_t font14x20;
extern const font_type_t font11x16;
extern const font_type_t font7x12;
extern const font_type_t font6x8;
extern const font_type_t font5x8;

/* Font array*/
#define TOTAL_FONTS 6
extern const font_type_t * fonts_table[TOTAL_FONTS];

/**
  * @}
  */ 

/** @defgroup FONTS_Exported_Constants
  * @{
  */ 

/**
  * @}
  */ 

/** @defgroup FONTS_Exported_Macros
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup FONTS_Exported_Functions
  * @{
  */ 
/**
  * @}
  */

#ifdef __cplusplus
}
#endif
  
#endif /* __FONTS_H */
 
/**
  * @}
  */

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */      

/*****************************************************************************/
/*                           End of file                                     */
/*****************************************************************************/
