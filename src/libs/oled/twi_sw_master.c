//!*****************************************************************************
//!   @file    twi_sw_master.c
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    12/07/2017
//!   @brief   This file includes the software TWI driver source
//!  Supported features:
//!  - Repeated start
//!  - No multi-master
//!  - Only 7-bit addressing
//!  - Supports clock stretching (with optional SMBus style slave timeout)
//!  - Tries to handle slaves stuck in the middle of transfer
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  12/07/2017 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#include "twi_sw_master.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#ifndef TWI_MASTER_TIMEOUT_COUNTER_LOAD_VALUE
#define TWI_MASTER_TIMEOUT_COUNTER_LOAD_VALUE (5000UL) //!< Unit is number of empty loops. Timeout for SMBus devices is 35 ms. Set to zero to disable slave timeout altogether.
#endif

/* Private macro -------------------------------------------------------------*/
/*!< Configures the TWI pin to Standard-0, No-drive 1 */
#define TWI_PIN_STANDARD0_NODRIVE1(PIN_NUMBER)  nrf_gpio_cfg( PIN_NUMBER, NRF_GPIO_PIN_DIR_INPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE)

/* IAR and GCC compilers optimize set/clear finctions! */
#if defined ( __ICCARM__ )
#pragma optimize=none
#elif defined   ( __GNUC__ )
#pragma GCC optimize ("O1")
#else
#warning "Undefined compiler, check optimization if I2C does not work!"
#endif
void pin_x(uint8_t pin, uint8_t state)
{
  if (state != 0u)
  {
    nrf_gpio_pin_set(pin);
  }
  else
  {
    nrf_gpio_pin_clear(pin);
  }
}

/* These macros are needed to see if the slave is stuck and we as master send dummy clock cycles to end its wait */
#define TWI_SCL_HIGH(PIN_NUMBER)   pin_x(PIN_NUMBER, 1)    /*!< Pulls SCL line high */
#define TWI_SCL_LOW(PIN_NUMBER)    pin_x(PIN_NUMBER, 0)  /*!< Pulls SCL line low  */
#define TWI_SDA_HIGH(PIN_NUMBER)   pin_x(PIN_NUMBER, 1)    /*!< Pulls SDA line high */
#define TWI_SDA_LOW(PIN_NUMBER)    pin_x(PIN_NUMBER, 0)  /*!< Pulls SDA line low  */
#define TWI_SDA_INPUT(PIN_NUMBER)  nrf_gpio_cfg(PIN_NUMBER, NRF_GPIO_PIN_DIR_INPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE)   /*!< Configures SDA pin as input  */
#define TWI_SDA_OUTPUT(PIN_NUMBER) nrf_gpio_cfg(PIN_NUMBER, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE)   /*!< Configures SDA pin as output */
#define TWI_SCL_OUTPUT(PIN_NUMBER) nrf_gpio_cfg(PIN_NUMBER, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_CONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0D1, NRF_GPIO_PIN_NOSENSE)    /*!< Configures SCL pin as output */

#define TWI_SDA_READ(PIN_NUMBER) nrf_gpio_pin_read(PIN_NUMBER)     /*!< Reads current state of SDA */
#define TWI_SCL_READ(PIN_NUMBER) nrf_gpio_pin_read(PIN_NUMBER)     /*!< Reads current state of SCL */

#define TWI_DELAY()  nrf_delay_us(0)    /*!< Time to wait when pin states are changed. For fast-mode the delay can be zero and for standard-mode 4 us delay is sufficient. */

/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

bool inline twi_master_clear_bus(uint8_t sda_pin, uint8_t scl_pin);
bool inline twi_master_issue_startcondition(uint8_t sda_pin, uint8_t scl_pin);
bool inline twi_master_issue_stopcondition(uint8_t sda_pin, uint8_t scl_pin);
bool inline twi_master_issue_startrepitcondition(uint8_t sda_pin, uint8_t scl_pin);
bool inline twi_master_clock_byte(uint_fast8_t databyte, uint8_t sda_pin, uint8_t scl_pin);
bool inline twi_master_clock_byte_in(uint8_t * databyte, bool ack, uint8_t sda_pin, uint8_t scl_pin);
bool inline twi_master_wait_while_scl_low(uint8_t scl_pin);

bool twi_master_init(twi_sw_control_struct_t * control_struct)
{
  if(control_struct != NULL)
  {
    // Configure both pins to output Standard 0, No-drive (open-drain) 1
    TWI_PIN_STANDARD0_NODRIVE1(control_struct->scl_pin);
    TWI_PIN_STANDARD0_NODRIVE1(control_struct->sda_pin);

    // Configure SCL as output
    TWI_SCL_HIGH(control_struct->scl_pin);
    TWI_SCL_OUTPUT(control_struct->scl_pin);

    // Configure SDA as output
    TWI_SDA_HIGH(control_struct->sda_pin);
    TWI_SDA_OUTPUT(control_struct->sda_pin);

    if(twi_master_clear_bus(control_struct->sda_pin, control_struct->scl_pin))
    {
      control_struct->initialized = true;
      return true;
    }
    else
    {
      control_struct->initialized = false;
    }
  }
  return false;
}

bool twi_master_read_reg(twi_sw_control_struct_t * control_struct, uint8_t address, uint8_t reg, uint8_t *data, uint8_t data_length, bool issue_stop)
{
  if(control_struct != NULL)
  {
    if(control_struct->initialized)
    {
      bool transfer_succeeded = true;

      transfer_succeeded &= twi_master_issue_startcondition(control_struct->sda_pin, control_struct->scl_pin);

      transfer_succeeded &= twi_master_clock_byte((address & (~TWI_READ_BIT)), control_struct->sda_pin, control_struct->scl_pin);
      transfer_succeeded &= twi_master_clock_byte(reg, control_struct->sda_pin, control_struct->scl_pin);
      transfer_succeeded &= twi_master_issue_startrepitcondition(control_struct->sda_pin, control_struct->scl_pin);
      transfer_succeeded &= twi_master_clock_byte(address | TWI_READ_BIT, control_struct->sda_pin, control_struct->scl_pin);

      /* Transfer direction is from Slave to Master */
      while (data_length-- && transfer_succeeded)
      {
        // To indicate to slave that we've finished transferring last data byte
        // we need to NACK the last transfer.
        if (data_length == 0)
        {
          transfer_succeeded &= twi_master_clock_byte_in(data, (bool)false, control_struct->sda_pin, control_struct->scl_pin);
        }
        else
        {
          transfer_succeeded &= twi_master_clock_byte_in(data, (bool)true, control_struct->sda_pin, control_struct->scl_pin);
        }
        data++;
      }

      if(issue_stop || !transfer_succeeded)
      {
        transfer_succeeded &= twi_master_issue_stopcondition(control_struct->sda_pin, control_struct->scl_pin);
      }
      if(transfer_succeeded != true)
      {
        twi_master_clear_bus(control_struct->sda_pin, control_struct->scl_pin);
      }
      return transfer_succeeded;
    }
  }
  return false;
}

bool twi_master_write_reg(twi_sw_control_struct_t * control_struct, uint8_t address, uint8_t reg, uint8_t *data, uint8_t data_length, bool issue_stop)
{
  if(control_struct != NULL)
  {
    if(control_struct->initialized)
    {
      bool transfer_succeeded = true;
      transfer_succeeded &= twi_master_issue_startcondition(control_struct->sda_pin, control_struct->scl_pin);
      transfer_succeeded &= twi_master_clock_byte((address & (~TWI_READ_BIT)), control_struct->sda_pin, control_struct->scl_pin);
      transfer_succeeded &= twi_master_clock_byte(reg, control_struct->sda_pin, control_struct->scl_pin);
      /* Transfer direction is from Master to Slave */
      while (data_length-- && transfer_succeeded)
      {
        transfer_succeeded &= twi_master_clock_byte(*data, control_struct->sda_pin, control_struct->scl_pin);
        data++;
      }

      if(issue_stop || !transfer_succeeded)
      {
        transfer_succeeded &= twi_master_issue_stopcondition(control_struct->sda_pin, control_struct->scl_pin);
      }
      if(transfer_succeeded != true)
      {
        twi_master_clear_bus(control_struct->sda_pin, control_struct->scl_pin);
      }

      return transfer_succeeded;
    }
  }
  return false;
}

bool twi_master_transfer(twi_sw_control_struct_t * control_struct, uint8_t address, uint8_t * data, uint8_t data_length, bool issue_stop_condition)
{
  if(control_struct != NULL)
  {
    if(control_struct->initialized)
    {
      bool transfer_succeeded = true;

      transfer_succeeded &= twi_master_issue_startcondition(control_struct->sda_pin, control_struct->scl_pin);
      transfer_succeeded &= twi_master_clock_byte(address, control_struct->sda_pin, control_struct->scl_pin);

      if (address & TWI_READ_BIT)
      {
        /* Transfer direction is from Slave to Master */
        while (data_length-- && transfer_succeeded)
        {
          // To indicate to slave that we've finished transferring last data byte
          // we need to NACK the last transfer.
          if (data_length == 0)
          {
            transfer_succeeded &= twi_master_clock_byte_in(data, (bool)false, control_struct->sda_pin, control_struct->scl_pin);
          }
          else
          {
            transfer_succeeded &= twi_master_clock_byte_in(data, (bool)true, control_struct->sda_pin, control_struct->scl_pin);
          }
          data++;
        }
      }
      else
      {
        /* Transfer direction is from Master to Slave */
        while (data_length-- && transfer_succeeded)
        {
          transfer_succeeded &= twi_master_clock_byte(*data, control_struct->sda_pin, control_struct->scl_pin);
          data++;
        }
      }

      if (issue_stop_condition || !transfer_succeeded)
      {
        transfer_succeeded &= twi_master_issue_stopcondition(control_struct->sda_pin, control_struct->scl_pin);
      }
      if(transfer_succeeded != true)
      {
        twi_master_clear_bus(control_struct->sda_pin, control_struct->scl_pin);
      }
      return transfer_succeeded;
    }
  }
  return false;
}

/**
 * @brief Function for detecting stuck slaves and tries to clear the bus.
 *
 * @retval false if bus is stuck else true if bus is clear.
 */
bool twi_master_clear_bus(uint8_t sda_pin, uint8_t scl_pin)
{
  bool bus_clear;

  TWI_SDA_HIGH(sda_pin);
  TWI_SCL_HIGH(scl_pin);
  TWI_DELAY();


  if (TWI_SDA_READ(sda_pin) == 1 && TWI_SCL_READ(scl_pin) == 1)
  {
    bus_clear = true;
  }
  else if (TWI_SCL_READ(scl_pin) == 1)
  {
    bus_clear = false;
    // Clock max 18 pulses worst case scenario(9 for master to send the rest of command and 9 for slave to respond) to SCL line and wait for SDA come high
    for (uint_fast8_t i = 18; i--;)
    {
      TWI_SCL_LOW(scl_pin);
      TWI_DELAY();
      TWI_SCL_HIGH(scl_pin);
      TWI_DELAY();

      if (TWI_SDA_READ(sda_pin) == 1)
      {
        bus_clear = true;
        break;
      }
    }
  }
  else if (TWI_SDA_READ(sda_pin) == 1)
  {
    TWI_DELAY();
    bus_clear = false;
    // Clock max 18 pulses worst case scenario(9 for master to send the rest of command and 9 for slave to respond) to SCL line and wait for SDA come high
    for (uint_fast8_t i = 18; i--;)
    {
      TWI_SCL_LOW(scl_pin);
      TWI_DELAY();
      TWI_SCL_HIGH(scl_pin);
      TWI_DELAY();

      if ((TWI_SDA_READ(sda_pin) == 1) && (TWI_SCL_READ(scl_pin) == 1))
      {
        bus_clear = true;
        break;
      }
    }

    TWI_SDA_LOW(sda_pin);
    TWI_DELAY();

    bus_clear = false;
    // Clock max 18 pulses worst case scenario(9 for master to send the rest of command and 9 for slave to respond) to SCL line and wait for SDA come high
    for (uint_fast8_t i = 18; i--;)
    {
      TWI_SCL_LOW(scl_pin);
      TWI_DELAY();
      TWI_SCL_HIGH(scl_pin);
      TWI_DELAY();

      if ((TWI_SDA_READ(sda_pin) == 1) && (TWI_SCL_READ(scl_pin) == 1))
      {
        bus_clear = true;
        break;
      }
    }
  }
  else
  {
    bus_clear = false;
  }

  return bus_clear;
}

/**
 * @brief Function for issuing TWI START condition to the bus.
 *
 * START condition is signaled by pulling SDA low while SCL is high. After this function SCL and SDA will be low.
 *
 * @return
 * @retval false Timeout detected
 * @retval true Clocking succeeded
 */
bool twi_master_issue_startcondition(uint8_t sda_pin, uint8_t scl_pin)
{
  // Make sure both SDA and SCL are high before pulling SDA low.
  TWI_SDA_HIGH(sda_pin);
  TWI_DELAY();
  if (!twi_master_wait_while_scl_low(scl_pin))
  {
    return false;
  }

  TWI_SDA_LOW(sda_pin);
  TWI_DELAY();

  // Other module function expect SCL to be low
  TWI_SCL_LOW(scl_pin);
  TWI_DELAY();

  return true;
}

/**
 * @brief Function for issuing TWI STOP condition to the bus.
 *
 * STOP condition is signaled by pulling SDA high while SCL is high. After this function SDA and SCL will be high.
 *
 * @return
 * @retval false Timeout detected
 * @retval true Clocking succeeded
 */
bool twi_master_issue_stopcondition(uint8_t sda_pin, uint8_t scl_pin)
{
  TWI_SDA_LOW(sda_pin);
  TWI_DELAY();
  if (!twi_master_wait_while_scl_low(scl_pin))
  {
    return false;
  }

  TWI_SDA_HIGH(sda_pin);
  TWI_DELAY();

  return true;
}

/**
 * @brief Function for issuing TWI Repeat Start condition to the bus.
 *
 * Repeated START condition is signaled by pulling SCL is high while SDA high. After this function SDA and SCL will be low and ready to clockin byte.
 *
 * @return
 * @retval false Timeout detected
 * @retval true Clocking succeeded
 */
bool twi_master_issue_startrepitcondition(uint8_t sda_pin, uint8_t scl_pin)
{
  TWI_SDA_HIGH(sda_pin);
  TWI_DELAY();

  if (!twi_master_wait_while_scl_low(scl_pin))
  {
    return false;
  }

  TWI_DELAY();

  TWI_SDA_LOW(sda_pin);
  TWI_DELAY();

  // Other module function expect SCL to be low
  TWI_SCL_LOW(scl_pin);
  TWI_DELAY();

  return true;
}

/**
 * @brief Function for clocking one data byte out and reads slave acknowledgment.
 *
 * Can handle clock stretching.
 * After calling this function SCL is low and SDA low/high depending on the
 * value of LSB of the data byte.
 * SCL is expected to be output and low when entering this function.
 *
 * @param databyte Data byte to clock out.
 * @return
 * @retval true Slave acknowledged byte.
 * @retval false Timeout or slave didn't acknowledge byte.
 */
bool twi_master_clock_byte(uint_fast8_t databyte, uint8_t sda_pin, uint8_t scl_pin)
{
  bool transfer_succeeded = true;

  /** @snippet [TWI SW master write] */
  // Make sure SDA is an output
  TWI_SDA_OUTPUT(sda_pin);

  // MSB first
  for (uint_fast8_t i = 0x80; i != 0; i >>= 1)
  {
    TWI_SCL_LOW(scl_pin);
    TWI_DELAY();

    if (databyte & i)
    {
      TWI_SDA_HIGH(sda_pin);
    }
    else
    {
      TWI_SDA_LOW(sda_pin);
    }

    if (!twi_master_wait_while_scl_low(scl_pin))
    {
      transfer_succeeded = false; // Timeout
      break;
    }
  }

  // Finish last data bit by pulling SCL low
  TWI_SCL_LOW(scl_pin);
  TWI_DELAY();

  /** @snippet [TWI SW master write] */

  // Configure TWI_SDA pin as input for receiving the ACK bit
  TWI_SDA_INPUT(sda_pin);

  // Give some time for the slave to load the ACK bit on the line
  TWI_DELAY();

  // Pull SCL high and wait a moment for SDA line to settle
  // Make sure slave is not stretching the clock
  transfer_succeeded &= twi_master_wait_while_scl_low(scl_pin);

  // Read ACK/NACK. NACK == 1, ACK == 0
  transfer_succeeded &= !(TWI_SDA_READ(sda_pin));

  // Finish ACK/NACK bit clock cycle and give slave a moment to release control
  // of the SDA line
  TWI_SCL_LOW(scl_pin);
  TWI_DELAY();

  // Configure TWI_SDA pin as output as other module functions expect that
  TWI_SDA_OUTPUT(sda_pin);

  return transfer_succeeded;
}

/**
 * @brief Function for clocking one data byte in and sends ACK/NACK bit.
 *
 * Can handle clock stretching.
 * SCL is expected to be output and low when entering this function.
 * After calling this function, SCL is high and SDA low/high depending if ACK/NACK was sent.
 *
 * @param databyte Data byte to clock out.
 * @param ack If true, send ACK. Otherwise send NACK.
 * @return
 * @retval true Byte read succesfully
 * @retval false Timeout detected
 */
bool twi_master_clock_byte_in(uint8_t *databyte, bool ack, uint8_t sda_pin, uint8_t scl_pin)
{
  uint_fast8_t byte_read          = 0;
  bool         transfer_succeeded = true;

  /** @snippet [TWI SW master read] */
  // Make sure SDA is an input
  TWI_SDA_INPUT(sda_pin);

  // SCL state is guaranteed to be high here

  // MSB first
  for (uint_fast8_t i = 0x80; i != 0; i >>= 1)
  {
    if (!twi_master_wait_while_scl_low(scl_pin))
    {
      transfer_succeeded = false;
      break;
    }

    if (TWI_SDA_READ(sda_pin))
    {
      byte_read |= i;
    }
    else
    {
      // No need to do anything
    }

    TWI_SCL_LOW(scl_pin);
    TWI_DELAY();
  }

  // Make sure SDA is an output before we exit the function
  TWI_SDA_OUTPUT(sda_pin);
  /** @snippet [TWI SW master read] */

  *databyte = (uint8_t)byte_read;

  // Send ACK bit

  // SDA high == NACK, SDA low == ACK
  if (ack)
  {
    TWI_SDA_LOW(sda_pin);
  }
  else
  {
    TWI_SDA_HIGH(sda_pin);
  }

  // Let SDA line settle for a moment
  TWI_DELAY();

  // Drive SCL high to start ACK/NACK bit transfer
  // Wait until SCL is high, or timeout occurs
  if (!twi_master_wait_while_scl_low(scl_pin))
  {
    transfer_succeeded = false; // Timeout
  }

  // Finish ACK/NACK bit clock cycle and give slave a moment to react
  TWI_SCL_LOW(scl_pin);
  TWI_DELAY();

  return transfer_succeeded;
}

/**
 * @brief Function for pulling SCL high and waits until it is high or timeout occurs.
 *
 * SCL is expected to be output before entering this function.
 * @note If TWI_MASTER_TIMEOUT_COUNTER_LOAD_VALUE is set to zero, timeout functionality is not compiled in.
 * @return
 * @retval true SCL is now high.
 * @retval false Timeout occurred and SCL is still low.
 */
bool twi_master_wait_while_scl_low(uint8_t scl_pin)
{
#if TWI_MASTER_TIMEOUT_COUNTER_LOAD_VALUE != 0
  uint32_t volatile timeout_counter = TWI_MASTER_TIMEOUT_COUNTER_LOAD_VALUE;
#endif

  // Pull SCL high just in case if something left it low
  TWI_SCL_HIGH(scl_pin);
  TWI_DELAY();

  while (TWI_SCL_READ(scl_pin) == 0)
  {
    // If SCL is low, one of the slaves is busy and we must wait

#if TWI_MASTER_TIMEOUT_COUNTER_LOAD_VALUE != 0
    if (timeout_counter-- == 0)
    {
      // If timeout_detected, return false
      return false;
    }
#endif
  }

  return true;
}

/*****************************************************************************/
/*                           End of file                                     */
/*****************************************************************************/
