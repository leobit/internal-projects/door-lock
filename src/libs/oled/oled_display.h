//******************************************************************************
//!  File description:
//!   @file    oled_display.h
//!   @author  Dmytro Kryvyi
//!   @version V1.0.0
//!   @date    26/06/2016
//!   @brief   This file includes the OLED driver
//!
//!                     Copyright (C) 2016
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | ----------------
//!  24/07/2016 | Dmytro Kryvyi    | Initial draft
//!  02/08/2016 | Dmytro Kryvyi    | Beta
//
//******************************************************************************

#ifndef _OLED_DISPLAY_H_
#define _OLED_DISPLAY_H_

/* Includes ------------------------------------------------------------------*/
#include "device.h"
#include "stdint.h"
#include "easy_gl.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/* Exported typedef ----------------------------------------------------------*/

/* Display color */
typedef enum oled_cl_e
{
  OLED_COLOR_BLACK,
  OLED_COLOR_WHITE,
  OLED_COLOR_INVERSE
} oled_cl_t;

/* Display orient */
typedef enum oled_scrol_frame_e
{
  OLED_SCROL_5FRAME   = 0,
  OLED_SCROL_64FRAME  = 1,
  OLED_SCROL_128FRAME = 2,
  OLED_SCROL_256FRAME = 3,
  OLED_SCROL_3FRAME   = 4,
  OLED_SCROL_4FRAME   = 5,
  OLED_SCROL_25FRAME  = 6,
  OLED_SCROL_2FRAME   = 7
} oled_scrol_frame_t;

/* Display fadding image */
typedef enum oled_fade
{
  OLED_DRAG_NONE = EASYGL_EFFECT_NONE, // Just send frame to OLED
  OLED_DRAG_LEFT = EASYGL_EFFECT_DRAG_LEFT,     // Start new frame from left side
  OLED_DRAG_LEFT_SCR = EASYGL_EFFECT_DRAG_LEFT_SCR, // Fade image from left side
  
  OLED_DRAG_LEFT_SWITCH = EASYGL_EFFECT_DRAG_LEFT_SWITCH, // Fade image from left side
  OLED_DRAG_UP = EASYGL_EFFECT_DRAG_UP,       // Start new frame from top side
  OLED_DRAG_UP_SCR = EASYGL_EFFECT_DRAG_UP_SCR,   // Fade image from top side
  
  OLED_DRAG_UP_SWITCH = EASYGL_EFFECT_DRAG_UP_SWITCH,// Fade image from top side
  OLED_DRAG_DOWN = EASYGL_EFFECT_DRAG_DOWN,     // Start new frame from bottom side
  OLED_DRAG_DOWN_SCR = EASYGL_EFFECT_DRAG_DOWN_SCR, // Fade image from bottom side
  
  OLED_DRAG_DOWN_SWITCH = EASYGL_EFFECT_DRAG_DOWN_SWITCH, // Fade image from bottom side
  OLED_DRAG_RIGHT = EASYGL_EFFECT_DRAG_RIGHT,    // Start new frame from right side
  OLED_DRAG_RIGHT_SCR = EASYGL_EFFECT_DRAG_RIGHT_SCR, // Fade imege from right side   
  
  OLED_DRAG_RIGHT_SWITCH = EASYGL_EFFECT_DRAG_RIGHT_SWITCH,
  OLED_DRAG_FROM_CENTER = EASYGL_EFFECT_DRAG_FROM_CENTER,
  OLED_DRAG_TO_CENTER = EASYGL_EFFECT_DRAG_TO_CENTER

} oled_fade_type;
/* Exported define ----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
extern "C" {
#endif

  bool oled_init(void);
  void oled_ioInit(void);
  void oled_deinitialize(void);
  void oled_standby(void);  
  void oled_setSleepMode(void);
  void oled_setActiveMode(void);  
  void oled_setContrast(uint8_t contrast);
  void oled_invertDisplay(bool flag);
   
  void oled_putPix(uint8_t x, uint8_t y, oled_cl_t color);
  oled_cl_t oled_getPix(uint8_t x, uint8_t y);  
  void oled_fill_rect(uint8_t x, uint8_t y, uint8_t x_offs, uint8_t y_offs, oled_cl_t color);  
 
  void oled_clear(void); 
  void oled_update(void); 
  void oled_update_area(uint8_t x, uint8_t y, uint8_t x_offs, uint8_t y_offs);
  void oled_bufferize(void);
  void oled_drag_in(oled_fade_type ftype, uint8_t pixels);
  void oled_write_direct_buffer(uint8_t xs, uint8_t xe, uint8_t ys, uint8_t ye, uint8_t * buff_ptr); 
  
  bool oled_setOrientation(device_orientation_t newOrientation);
  device_orientation_t oled_getOrientation(void);
  void oled_setscrollright(uint8_t start, uint8_t end, oled_scrol_frame_t frames);
  void oled_setscrollleft(uint8_t start, uint8_t end, oled_scrol_frame_t frames);
  void oled_setvert_scrol(uint8_t start, uint8_t end);
  void oled_stopscroll(void);
  void oled_startscroll(void);
  
  uint8_t oled_getHeight(void);
  uint8_t oled_getWidth(void);
    
#ifdef __cplusplus
}
#endif

#endif  /* _OLED_DISPLAY_H_ */

/******************************************************************************/
/*                            End of file                                     */
/******************************************************************************/
