//******************************************************************************
//!  File description:
//!   @file    easy_gl.h
//!   @author  Dmytro Kryvyi
//!   @version V1.0.0
//!   @date    28/11/2016
//!   @brief   This file includes the easy GL header.
//!
//!                     Copyright (C) 2016
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | ----------------
//!  28/11/2016 | Dmytro Kryvyi    | Initial draft
//!  08/12/2016 | Dmytro Kryvyi    | Beta
//
//******************************************************************************

#ifndef _EASY_GL_h_
#define _EASY_GL_h_

/* Includes ------------------------------------------------------------------*/
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "fonts.h"
#include "device.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Bases for effects */
#define EFFECT_BIT_BASE_LEFT    0x10u
#define EFFECT_BIT_BASE_RIGHT   0x20u
#define EFFECT_BIT_BASE_TOP     0x40u
#define EFFECT_BIT_BASE_BOTTOM  0x80u
#define EFFECT_BIT_BASE_OTHERS  0x100u

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/

/* Image structure. */
typedef struct image_s
{
  const void * imagPtr;
  uint16_t height;
  uint16_t width;
  imag_cross_t img_cr;
  enum
  {
    IMAGE_COLOR_1BIT_BW,
    IMAGE_COLOR_3BIT_RGB
  } image_bit_perPix;
} image_t;

/* Display text mode. */
typedef enum
{
  EASYGL_TEXT_NORMAL,
  EASYGL_TEXT_DIGHTERING  
} easyGL_drawMode_t;

/* EasyGL transit effect. */
typedef enum 
{
  EASYGL_EFFECT_NONE = 0,                              // Just update the screen
  EASYGL_EFFECT_DRAG_LEFT = EFFECT_BIT_BASE_LEFT,      // Bring new frame from left side
  EASYGL_EFFECT_DRAG_LEFT_SCR,                         // Fade image from left side
  EASYGL_EFFECT_DRAG_LEFT_SWITCH,                      // Fade image from left side

  EASYGL_EFFECT_DRAG_RIGHT = EFFECT_BIT_BASE_RIGHT,   // Start new frame from right side
  EASYGL_EFFECT_DRAG_RIGHT_SCR,                       // Fade imege from right side   
  EASYGL_EFFECT_DRAG_RIGHT_SWITCH,
  
  EASYGL_EFFECT_DRAG_UP = EFFECT_BIT_BASE_TOP,       // Start new frame from top side
  EASYGL_EFFECT_DRAG_UP_SCR,                         // Fade image from top side
  EASYGL_EFFECT_DRAG_UP_SWITCH,                      // Fade image from top side

  EASYGL_EFFECT_DRAG_DOWN = EFFECT_BIT_BASE_BOTTOM,  // Start new frame from bottom side
  EASYGL_EFFECT_DRAG_DOWN_SCR,                       // Fade image from bottom side
  EASYGL_EFFECT_DRAG_DOWN_SWITCH,                    // Fade image from bottom side

  EASYGL_EFFECT_DRAG_FROM_CENTER = EFFECT_BIT_BASE_OTHERS,
  EASYGL_EFFECT_DRAG_TO_CENTER
} easyGL_UpdateSCREffect_t;

/* Typedef of color. */
typedef uint8_t easyGL_cl_t;

/* Point structure*/   
typedef struct 
{
  int16_t x;
  int16_t y;
} easyGL_point_t, * easyGL_pPoint_t;   

/* Exported define -----------------------------------------------------------*/	
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/* Absolute macro.*/
#define ABS(x)  ((x) > 0 ? (x) : -(x))

/* Image cration */
#define CREATE_IMAGE(modificator, name, _height, _width, _crossing, _color_palet, ...)   \
 static const uint8_t name##frame [] = {##__VA_ARGS__};                   \
 modificator const image_t name = {                                       \
   .imagPtr = (const void *) (name##frame),                               \
    .height = _height,                                                    \
      .width = _width,                                                    \
        .img_cr = _crossing,                                              \
          .image_bit_perPix = _color_palet                                \
}

/* Exported functions ------------------------------------------------------- */
  
  uint16_t easyGL_initialize(const font_type_t * font);
  
  void easyGL_setFont(const font_type_t * font);
  font_type_t * easyGL_getFont(void);
  uint16_t easyGL_getTextlengthByFont(const font_type_t * font, char * str);
  
  void easyGL_setBackCol(easyGL_cl_t color);
  void easyGL_setActiveCol(easyGL_cl_t color);
  easyGL_cl_t easyGL_getBackCol(void);
  easyGL_cl_t easyGL_getActiveCol(void);
  
  void easyGL_setDrawMode(easyGL_drawMode_t mode);
  easyGL_drawMode_t easyGL_getDrawMode(void);
  
  void easyGL_putCharacter(char charact, int16_t x, int16_t y, const font_type_t * font);
  void easyGL_print(char * string, int16_t x, int16_t y, const font_type_t * font);
  void easyGL_print_s(char * string, int16_t x, int16_t y, uint16_t characters_count, const font_type_t * font);
  void easyGL_prints(char * string, const font_type_t * font);
  
  void easyGL_putImg(const image_t * ptr, int16_t x, int16_t y);
  void easyGL_putBWimg(void * ptr, int16_t x, int16_t y, uint16_t x_offs, uint16_t y_offs, imag_cross_t img_cr);
  void easyGL_putPartOfImg(const image_t * ptr, int16_t x, int16_t y, uint16_t imag_area_x_start, uint16_t imag_area_y_start, uint16_t x_offset, uint16_t y_offset);
  
  void easyGL_clearFrame(void);
  void easyGL_updateDisplay(void);
  void easyGL_updateDisplay_area(int16_t xs, int16_t ys, uint16_t x_offs, uint16_t y_offs);
  
  void easyGL_setThisFrameAsPrevForEffect(void);
  void easyGL_updateDisplayWithEffect(easyGL_UpdateSCREffect_t ftype, uint16_t effectStep);
  
  uint16_t easyGL_getHeight(void);
  uint16_t easyGL_getWidth(void);
  
  void easyGL_setOrientation(device_orientation_t newOrientation);
  device_orientation_t easyGL_getOrientation(void);
  
  void easyGL_drawPixel(int16_t xs, int16_t ys, easyGL_cl_t color);
  easyGL_cl_t easyGL_getPixel(int16_t x, int16_t y);
  easyGL_cl_t easyGL_getPixelFromImage(const image_t * ptr, uint16_t x, uint16_t y);
  
  void easyGL_drawHLine(int16_t xs, int16_t ys, uint16_t length, uint16_t roughtness);
  void easyGL_drawVLine(int16_t xs, int16_t ys, uint16_t length, uint16_t roughtness);
  void easyGL_drawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t roughtness);
  
  void easyGL_drawRect(int16_t xs, int16_t ys, uint16_t width, uint16_t height, uint16_t roughtness);
  void easyGL_drawCircle(int16_t xs, int16_t ys, int16_t Radius, uint16_t roughtness);    
  void easyGL_drawTriangle(int16_t x1, int16_t x2, int16_t x3, int16_t y1, int16_t y2, int16_t y3, uint16_t roughtness);
  void easyGL_drawPolygon(easyGL_pPoint_t Points, uint16_t PointCount, uint16_t roughtness);
  void easyGL_drawEllipse(int16_t xs, int16_t ys, uint16_t xRadius, uint16_t yRadius, uint16_t roughtness);
  
  void easyGL_fillRect(int16_t xs, int16_t ys, uint16_t width, uint16_t height);
  void easyGL_fillCircle(int16_t xs, int16_t ys, uint16_t Radius);
  void easyGL_fillTriangle(int16_t x1, int16_t x2, int16_t x3, int16_t y1, int16_t y2, int16_t y3);
  void easyGL_fillPolygon(easyGL_pPoint_t Points, uint16_t PointCount);
  void easyGL_fillEllipse(int16_t xs, int16_t ys, uint16_t xRadius, uint16_t yRadius);
  
#ifdef __cplusplus
}
#endif

#endif  /* _EASY_GL_h_ */

/******************************************************************************/
/*                            End of file                                     */
/******************************************************************************/
