//******************************************************************************
//!  File description:
//!   @file    easy_gl.c
//!   @author  Dmytro Kryvyi
//!   @version V1.0.0
//!   @date    28/11/2016
//!   @brief   This file includes the easy GL implementation.
//!
//!                     Copyright (C) 2016
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | ----------------
//!  28/11/2016 | Dmytro Kryvyi    | Initial draft
//!  08/12/2016 | Dmytro Kryvyi    | Beta
//!  30/10/2017 | Dmytro Kryvyi    | Most changed, nef functional, completely redesigned 
//
//******************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "easy_gl.h"
#include "string.h"
#include "math.h"
#include "compiler_abstraction.h"
#include "oled_display.h"

/* Private typedef -----------------------------------------------------------*/
/* Private definitions 0 -----------------------------------------------------*/

/* The lines rought, that will be used at draving elements. */
#define EASY_GL_DEFAULT_ROUGHTNESS        1u
#define EASY_GL_DEFAULT_ROUGHTNESS_FILL   1u

/* Default frame start at points. Do not change! */
#define FRAME_START_X  0
#define FRAME_START_Y  0

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* EASYGL information and control*/
struct
{
  /* Physical dimension of display matrix. */
  uint16_t easyGL_height;
  uint16_t easyGL_width;  
  
  /* Current position of console mode text, color and font variables. */
  int16_t console_symbol_XPos;
  int16_t console_symbol_YPos;  
  const font_type_t * current_font;  
  easyGL_cl_t activeDrawColor;
  easyGL_cl_t activeBackColor;
  
  /* Frame orientation and drawing mode */
  bool emulate_orientation;
  device_orientation_t current_orientation;   
  easyGL_drawMode_t drawing_mode;
  
} current_easyGL;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Private definitions 1 -----------------------------------------------------*/

/* Linking to PHY. */

#define DISPLAY_PHY_INITIALIZE()                                 oled_init()

#define DISPLAY_PHY_GET_HEIGHT()                                 oled_getHeight()
#define DISPLAY_PHY_GET_WIDTH()                                  oled_getWidth()
#define DISPLAY_PHY_GET_ORIENTATION()                            oled_getOrientation()
#define DISPLAY_PHY_SET_ORIENTATION(orient)                      oled_setOrientation(orient)

#define DISPLAY_PHY_PUT_PIX(x, y, color)                         oled_putPix((uint8_t)x, (uint8_t)y, (oled_cl_t) color)
#define DISPLAY_PHY_GET_PIX(x, y)                                (uint8_t)oled_getPix((uint8_t)x, (uint8_t)y)

#define DISPLAY_PHY_FILL_RECT(xs, ys, width, height, color)      oled_fill_rect((uint8_t) xs, (uint8_t) ys, width, height, (oled_cl_t) color)
#define DISPLAY_PHY_UPDATE_RECT(x, y, x_offs, y_offs)            oled_update_area((uint8_t) x,(uint8_t) y, x_offs, y_offs)
#define DISPLAY_PHY_UPDATE()                                     oled_update()
#define DISPLAY_PHY_CLEAR()                                      oled_clear()
#define DISPLAY_PHY_BUFFERIZE()                                  oled_bufferize()
#define DISPLAY_PHY_EFFECT(e, step)                              oled_drag_in((oled_fade_type)e, step)


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define GET_X_FROM_POINT(Z)              ((int16_t)((Points + Z)->x))
#define GET_Y_FROM_POINT(Z)              ((int16_t)((Points + Z)->y))

/* Exported functions --------------------------------------------------------*/

/**
* @brief easyGL initalizaton
*        
* @param font - font that will be use for easyGL
* @retval None
*/
uint16_t easyGL_initialize(const font_type_t * font)
{
  uint16_t retcode = (DISPLAY_PHY_INITIALIZE() != true);

  current_easyGL.activeBackColor = OLED_COLOR_BLACK;
  current_easyGL.activeDrawColor = OLED_COLOR_WHITE;  
  current_easyGL.current_font = font;
  current_easyGL.emulate_orientation = true;
  current_easyGL.easyGL_height = DISPLAY_PHY_GET_HEIGHT();
  current_easyGL.easyGL_width = DISPLAY_PHY_GET_WIDTH();
  current_easyGL.console_symbol_XPos = 0;
  current_easyGL.console_symbol_YPos = 0;
  current_easyGL.current_orientation = DEVICE_ORIENT_LANDSCAPE_BOTTOM;
  current_easyGL.drawing_mode = EASYGL_TEXT_NORMAL;
  
  current_easyGL.current_orientation = easyGL_getOrientation();
  
  return retcode;
}

/**
* @brief  writes Pixel.
* @param  xs: the x position
* @param  ys: the y position
* @param  RGB_Code: the pixel color in ARGB mode (8-8-8-8)  
* @retval None
*/
void easyGL_drawPixel(int16_t x, int16_t y, easyGL_cl_t color)
{
  if(current_easyGL.emulate_orientation)
  {
    switch(current_easyGL.current_orientation)
    {         
    case DEVICE_ORIENT_PORTRAIT_LEFT:
      {
        int16_t t_y = y;
        y = ((int16_t)(current_easyGL.easyGL_height - 1u)) - x;
        x = t_y;

        if((y >= FRAME_START_X) && (x >= FRAME_START_Y))
        {
          DISPLAY_PHY_PUT_PIX(x, y, color);
        }
        return;
      }

    case DEVICE_ORIENT_PORTRAIT_RIGHT:
      {
        int16_t t_y = y;
        y = x;
        x = ((int16_t)(current_easyGL.easyGL_width - 1u)) - t_y;

        if((y >= FRAME_START_X) && (x >= FRAME_START_Y))
        {
          DISPLAY_PHY_PUT_PIX(x, y, color);
        }
        return;
      }

    case DEVICE_ORIENT_LANDSCAPE_BOTTOM:
      {  
        x = ((int16_t)(current_easyGL.easyGL_width - 1u)) - x;
        y = ((int16_t)(current_easyGL.easyGL_height - 1u)) - y;
      }
    default:
      break;
    }
  }

  if((x >= FRAME_START_X) && (y >= FRAME_START_Y))
  {
    DISPLAY_PHY_PUT_PIX(x, y, color);
  }
}


/**
* @brief  writes Pixel.
* @param  xs: the x position
* @param  ys: the y position
* @retval Selected pixel color
*/
easyGL_cl_t easyGL_getPixel(int16_t x, int16_t y)
{
  if(current_easyGL.emulate_orientation)
  {
    switch(current_easyGL.current_orientation)
    {
      case DEVICE_ORIENT_PORTRAIT_LEFT:
      {
        int16_t t_y = y;
        y = ((int16_t)(current_easyGL.easyGL_height - 1u)) - x;
        x = t_y;

        if((y >= FRAME_START_X) && (x >= FRAME_START_Y))
        {
          return DISPLAY_PHY_GET_PIX(x, y);
        }
        return (easyGL_cl_t) 0u;
      }
      
    case DEVICE_ORIENT_PORTRAIT_RIGHT:
      {
        int16_t t_y = y;
        y = x;
        x = ((int16_t)(current_easyGL.easyGL_width - 1u)) - t_y;
        
        if((y >= FRAME_START_X) && (x >= FRAME_START_Y))
        {
          return DISPLAY_PHY_GET_PIX(x, y);
        }
        return (easyGL_cl_t) 0u;
      }

    case DEVICE_ORIENT_LANDSCAPE_BOTTOM:
      {  
        x = ((int16_t)(current_easyGL.easyGL_width - 1u)) - x;
        y = ((int16_t)(current_easyGL.easyGL_height - 1u))- y;
      }
    default:
      break;
    }
  }

  if((x >= FRAME_START_X) && (y >= FRAME_START_Y))
  {
    return DISPLAY_PHY_GET_PIX(x, y);
  }
  return (easyGL_cl_t) 0u;
}

/**
* @brief  Displays a full rectangle.
* @param  xs: the x position
* @param  ys: the y position
* @param  height: rectangle height
* @param  width: rectangle width
* @retval None
*/
void easyGL_fillRect(int16_t xs, int16_t ys, uint16_t width, uint16_t height)
{
  if(current_easyGL.emulate_orientation)
  {
    switch(current_easyGL.current_orientation)
    {

    case DEVICE_ORIENT_PORTRAIT_LEFT:
      {
        int16_t t_ys = ys;
        ys = ((current_easyGL.easyGL_height - 1u) - (xs + (int16_t)width));
        xs = t_ys;
        uint16_t t_wh = width;
        width = height;
        height = t_wh;
        break;
      }

    case DEVICE_ORIENT_PORTRAIT_RIGHT:
      {    
        int16_t t_ys = ys;
        ys = xs;
        xs = (((int16_t)(current_easyGL.easyGL_width - 1u)) - t_ys - (int16_t)height);
        uint16_t t_wh = width;
        width = height;
        height = t_wh;  
        break;
      }

    case DEVICE_ORIENT_LANDSCAPE_BOTTOM:
      {  
        xs = ((int16_t)(current_easyGL.easyGL_width - 1u)) - (xs + (int16_t)width);
        ys = ((int16_t)(current_easyGL.easyGL_height- 1u)) - (ys + (int16_t)height);
      }

    default:
      break;
    }
  }

  /* If rectangle position is not out of diplay area. */
  if((((int16_t)width + xs) > FRAME_START_X) && (((int16_t)height + ys) > FRAME_START_Y))
  {
    if(xs < FRAME_START_X)
    {
      width += (xs - FRAME_START_X);
      xs = FRAME_START_X;
    }

    if(ys < FRAME_START_Y)
    {
      height += (ys - FRAME_START_Y);
      ys = FRAME_START_Y;
    }
    
    DISPLAY_PHY_FILL_RECT(xs, ys, width, height, current_easyGL.activeDrawColor);
  }
}

/**
* @brief Set current easyGL orientation
*        
* @param easyGL orientation
* @retval None
*/  
void easyGL_setOrientation(device_orientation_t newOrientation)
{
  if(DISPLAY_PHY_SET_ORIENTATION(newOrientation) == true)
  {
    current_easyGL.emulate_orientation = false;
  }
  else
  {
    current_easyGL.emulate_orientation = true;
  }
  current_easyGL.current_orientation = newOrientation;
}

/**
* @brief return current easyGL orientation
*        
* @param None
* @retval easyGL orientation
*/  
device_orientation_t easyGL_getOrientation(void)
{
  if(current_easyGL.emulate_orientation)
  {
    return current_easyGL.current_orientation;
  }
  else
  {
    return (device_orientation_t) DISPLAY_PHY_GET_ORIENTATION();
  }
}

/**
* @brief return display height
*        
* @param None
* @retval Height
*/  
uint16_t easyGL_getHeight(void)
{
  if(current_easyGL.emulate_orientation)
  {
    switch(current_easyGL.current_orientation)
    {
    case DEVICE_ORIENT_PORTRAIT_RIGHT:
    case DEVICE_ORIENT_PORTRAIT_LEFT:
      return current_easyGL.easyGL_width;  
    default:
      return current_easyGL.easyGL_height; 
    }
  }
  else
  {
    return DISPLAY_PHY_GET_HEIGHT(); 
  }
}

/**
* @brief return easyGL width
*        
* @param None
* @retval easyGL Width
*/  
uint16_t easyGL_getWidth(void)
{
  if(current_easyGL.emulate_orientation)
  {
    switch(current_easyGL.current_orientation)
    {
    case DEVICE_ORIENT_PORTRAIT_RIGHT:
    case DEVICE_ORIENT_PORTRAIT_LEFT:
      return current_easyGL.easyGL_height; 
    default:
      return current_easyGL.easyGL_width;  
    }
  }
  else
  {
    return DISPLAY_PHY_GET_WIDTH(); 
  }
}

/**
* @brief Upload buffer to display
*        
* @param  None
* @retval None
*/
void easyGL_updateDisplay(void)
{
  DISPLAY_PHY_UPDATE();
}

/**
* @brief Update display area with coordinates
*        
* @param position(xs,yy), offset(_offs, y_offs)
* @retval None
*/
void easyGL_updateDisplay_area(int16_t xs, int16_t ys, uint16_t x_offs, uint16_t y_offs)
{
  if(current_easyGL.emulate_orientation)
  {
    switch(current_easyGL.current_orientation)
    {

    case DEVICE_ORIENT_PORTRAIT_LEFT:
      {
        int16_t t_ys = ys;
        ys = ((current_easyGL.easyGL_height - 1u) - (xs + (int16_t)x_offs));
        xs = t_ys;
        uint16_t t_wh = x_offs;
        x_offs = y_offs;
        y_offs = t_wh;
        break;
      }

    case DEVICE_ORIENT_PORTRAIT_RIGHT:
      {    
        int16_t t_ys = ys;
        ys = xs;
        xs = (((int16_t)(current_easyGL.easyGL_width - 1u)) - t_ys - (int16_t)y_offs);
        uint16_t t_wh = x_offs;
        x_offs = y_offs;
        y_offs = t_wh;  
        break;
      }

    case DEVICE_ORIENT_LANDSCAPE_BOTTOM:
      {  
        xs = ((int16_t)(current_easyGL.easyGL_width - 1u)) - (xs + (int16_t)x_offs);
        ys = ((int16_t)(current_easyGL.easyGL_height- 1u)) - (ys + (int16_t)y_offs);
      }

    default:
      break;
    }
  }

  /* If rectangle position is not out of diplay area. */
  if((((int16_t)x_offs + xs) > FRAME_START_X) && (((int16_t)y_offs + ys) > FRAME_START_Y))
  {
    if(xs < FRAME_START_X)
    {
      x_offs += (xs - FRAME_START_X);
      xs = FRAME_START_X;
    }

    if(ys < FRAME_START_Y)
    {
      y_offs += (ys - FRAME_START_Y);
      ys = FRAME_START_Y;
    }
    
    DISPLAY_PHY_UPDATE_RECT(xs, ys, x_offs, y_offs);
  }
}

/**
* @brief Clear frame buffer
*        
* @param  None
* @retval None
*/
void easyGL_clearFrame(void)
{
  DISPLAY_PHY_CLEAR();
}

/**
* @brief Update display with effect
*        
* @param  ftype - effect type
* @param  effectStep - step in pixel witch will be used to render effect
* @retval None
*/
void easyGL_updateDisplayWithEffect(easyGL_UpdateSCREffect_t ftype, uint16_t effectStep)
{
  easyGL_UpdateSCREffect_t effect = ftype;
  if(current_easyGL.emulate_orientation)
  {
    switch(current_easyGL.current_orientation)
    {    
    case DEVICE_ORIENT_LANDSCAPE_BOTTOM:
      {   
        if(ftype & EFFECT_BIT_BASE_TOP)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_BOTTOM + (ftype & (~EFFECT_BIT_BASE_TOP)));
          break;
        }
        
        if(ftype & EFFECT_BIT_BASE_BOTTOM)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_TOP + (ftype & (~EFFECT_BIT_BASE_BOTTOM)));
          break;
        }
        break;
      }
      
    case DEVICE_ORIENT_PORTRAIT_RIGHT:
      {
        if(ftype & EFFECT_BIT_BASE_LEFT)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_TOP + (ftype & (~EFFECT_BIT_BASE_LEFT)));
          break;
        }
        
        if(ftype & EFFECT_BIT_BASE_RIGHT)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_BOTTOM + (ftype & (~EFFECT_BIT_BASE_RIGHT)));
          break;
        }
        
        if(ftype & EFFECT_BIT_BASE_TOP)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_RIGHT + (ftype & (~EFFECT_BIT_BASE_TOP)));
          break;
        }
        
        if(ftype & EFFECT_BIT_BASE_BOTTOM)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_LEFT + (ftype & (~EFFECT_BIT_BASE_BOTTOM)));
          break;
        }
        break;
      }
      
    case DEVICE_ORIENT_PORTRAIT_LEFT:
      {
        if(ftype & EFFECT_BIT_BASE_LEFT)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_BOTTOM + (ftype & (~EFFECT_BIT_BASE_LEFT)));
          break;
        }
        
        if(ftype & EFFECT_BIT_BASE_RIGHT)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_TOP + (ftype & (~EFFECT_BIT_BASE_RIGHT)));
          break;
        }
        
        if(ftype & EFFECT_BIT_BASE_TOP)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_LEFT + (ftype & (~EFFECT_BIT_BASE_TOP)));
          break;
        }
        
        if(ftype & EFFECT_BIT_BASE_BOTTOM)
        {
          effect = (easyGL_UpdateSCREffect_t)(EFFECT_BIT_BASE_RIGHT + (ftype & (~EFFECT_BIT_BASE_BOTTOM)));
          break;
        }
        break;
      }
    }
  }

  DISPLAY_PHY_EFFECT(effect, effectStep);  
}

/**
* @brief This function uses to copy current frame to buffer for generation the transmission effect
*        
* @param  None
* @retval None
*/
void easyGL_setThisFrameAsPrevForEffect(void)
{
  DISPLAY_PHY_BUFFERIZE();
}

/**
* @brief  Displays an image.
* @param  ptr: the Image pointer
* @param  x, y: the x and y position
* @retval None
*/
void easyGL_putImg(const image_t * ptr, int16_t x, int16_t y)
{
  if(ptr->image_bit_perPix == IMAGE_COLOR_1BIT_BW)
  {
    easyGL_putBWimg((void*)ptr->imagPtr, x, y, ptr->width, ptr->height, ptr->img_cr);
  }
}

/**
* @brief draw black and white image
*        
* @param position(x,y) and charact
* @retval None
*/  
void easyGL_putBWimg(void * ptr, int16_t x, int16_t y, uint16_t x_offs, uint16_t y_offs, imag_cross_t img_cr)
{  
  volatile uint32_t i;
  uint16_t width, column;
  uint8_t * buf = (uint8_t *) ptr;
  
  if(current_easyGL.drawing_mode == EASYGL_TEXT_NORMAL)
  {
    if(img_cr == imag_top_to_bottom)
    {   
      for(column = 0u; column != y_offs; column++, buf += (((column & 0x07) == 0u) ? x_offs : 0u))     
      { 
        uint8_t shifted_dat = (uint8_t)(1 << (column & 0x07));
        for(width = 0u; width != x_offs; width++ )
        {
          if(*(buf + width) & shifted_dat)        
          {
            easyGL_drawPixel(x + (int16_t)width, y + (int16_t)column, current_easyGL.activeDrawColor);          
          }
          else      
          {
            easyGL_drawPixel(x + (int16_t)width, y + (int16_t)column, current_easyGL.activeBackColor);       
          }
        }
      }
    }   
    else
      if(img_cr == imag_left_to_right)
      {
        for( column = 0u; column < y_offs; column++)
        {
          i = 7u;
          for(width = 0u; width < x_offs; width++ )
          {
            if((*buf) & (uint8_t)(1u << i))         
            {
              easyGL_drawPixel(x + (int16_t)width, y + (int16_t)column, current_easyGL.activeDrawColor);       
            }
            else                            
            {            
              easyGL_drawPixel(x + (int16_t)width, y + (int16_t)column, current_easyGL.activeBackColor);                 
            }
            
            if(i == 0u) 
            {
              buf++;
              i = 8u;
            }
            i--; 
          }
          
          if(i != 7u)  
          {
            buf++;
          }
        }
      }
  }
  else
  {
    if(img_cr == imag_top_to_bottom)
    {   
      for(column = 0u; column != y_offs; column++, buf+= (((column & 0x07) == 0u) ? x_offs : 0u))     
      { 
        uint8_t shifted_dat = (uint8_t)(1u << (column & 0x07));
        for(width = 0u; width != x_offs; width++ )
        {
          if(*(buf + width) & shifted_dat)        
          {
            easyGL_drawPixel(x + (int16_t)width, y + (int16_t)column, current_easyGL.activeDrawColor);          
          }
        }
      }
    }   
    else
    {
      if(img_cr == imag_left_to_right)
      {
        for( column = 0u; column < y_offs; column++)
        {
          i = 7u;
          for(width = 0u; width < x_offs; width++ )
          {
            if((*buf) & (uint8_t)(1u << i))         
            {
              easyGL_drawPixel(x + (int16_t)width, y + (int16_t)column, current_easyGL.activeDrawColor);       
            }
            
            if(i == 0u) 
            {
              buf++;
              i = 8u;
            }
            i--; 
          }
          
          if(i != 7u)
          {
            buf++;
          }
        }
      }
    }
  }
}

/**
* @brief Put part of image to display
*        
* @param  ptr - pointer to image 
* @param  x, y - pixel position on image
* @retval pixel color
*/
easyGL_cl_t easyGL_getPixelFromImage(const image_t * ptr, uint16_t x, uint16_t y)
{
  if((x < ptr->width) && (ptr->height > y))
  {
    const uint8_t * image_p = (const uint8_t *)ptr->imagPtr;
    if(ptr->image_bit_perPix == IMAGE_COLOR_1BIT_BW)
    {
      if(ptr->img_cr == imag_top_to_bottom)
      {           
        uint8_t shifted_dat = (uint8_t)(1u << (y & 0x07));
        if(image_p[(((y / 8u) * ptr->width) + x)] & shifted_dat)
        {
          return current_easyGL.activeDrawColor;          
        }
        else      
        {
          return current_easyGL.activeBackColor;       
        }                  
      }   
      else
        if(ptr->img_cr == imag_left_to_right)
        {
          uint8_t shifted_dat = (uint8_t)(0x80 >> (x & 0x07));
          if(image_p[(((ptr->width / 8u) * y) + (x / 8u))] & shifted_dat)
          {
            return current_easyGL.activeDrawColor;          
          }
          else      
          {
            return current_easyGL.activeBackColor;       
          } 
        } 
    }
  }
  return (easyGL_cl_t) 0u;
}

/**
* @brief Put part of image to display
*        
* @param  ptr - pointer to image 
* @param  x, y - start position on display 
* @param  imag_area_x_start, imag_area_y_start - start point on image
* @param  x_offset, y_offset - size of image area to show
* @retval None
*/
void easyGL_putPartOfImg(const image_t * ptr, int16_t x, int16_t y, uint16_t imag_area_x_start, uint16_t imag_area_y_start, uint16_t x_offset, uint16_t y_offset)
{
  int16_t display_resol_width = (int16_t) easyGL_getWidth();
  int16_t display_resol_height = (int16_t) easyGL_getHeight();
  
  /* Check for outside of display and NULL pointer */
  if((x > display_resol_width) || (y > display_resol_height) || (ptr == NULL))
  {
    return;
  }
  
  /* If x less than zero calculate crop out */
  if(x < FRAME_START_X)
  {
    if((-x) > x_offset)
    {
      return;
    }
    x_offset += x;
    imag_area_x_start += (-x);
    x = FRAME_START_X;
  }
  
  /* If y less than zero calculate crop out */
  if(y < FRAME_START_Y)
  {
    if((-y) > y_offset)
    {
      return;
    }
    y_offset += y;
    imag_area_y_start += (-y);
    y = FRAME_START_Y;
  }
  
  if((ptr->width < imag_area_x_start) || (ptr->height < imag_area_y_start))
  {    
    return;
  }
  
  if(ptr->width < (imag_area_x_start + x_offset))
  {
    x_offset = ptr->width - imag_area_x_start;
  }
  
  if(ptr->height < (imag_area_y_start + y_offset))
  {
    y_offset = ptr->height - imag_area_y_start;
  }
  
  int16_t y_max = y + y_offset;
  if(y_max > display_resol_height)
  {
    y_max = display_resol_height;
  }
  
  int16_t x_max = x + x_offset;
  if(x_max > display_resol_width)
  {
    x_max = display_resol_width;
  }
  
  while(y < y_max)
  {    
    for(uint16_t phy_x = x, imag_x = imag_area_x_start; phy_x < x_max; phy_x++, imag_x++)
    {
      easyGL_drawPixel(phy_x, y, easyGL_getPixelFromImage(ptr, imag_x, imag_area_y_start));      
    }   
    y++;
    imag_area_y_start++;    
  }  
}

/**
* @brief Set easyGL text color
*        
* @param easyGL color
* @retval None
*/
void easyGL_setActiveCol(easyGL_cl_t color)
{
  current_easyGL.activeDrawColor = color;
}

/**
* @brief Set easyGL background color
*        
* @param easyGL color
* @retval None
*/
void easyGL_setBackCol(easyGL_cl_t color)
{
  current_easyGL.activeBackColor = color;
}

/**
* @brief Get easyGL background color
*        
* @param None
* @retval easyGL color
*/
easyGL_cl_t easyGL_getBackCol(void)
{
  return current_easyGL.activeBackColor;
}

/**
* @brief Get easyGL active drawing color
*        
* @param None 
* @retval easyGL color
*/
easyGL_cl_t easyGL_getActiveCol(void)
{
  return current_easyGL.activeDrawColor;  
}

/**
* @brief Set easyGL font
*        
* @param easyGL font(use type font_type_t)
* @retval None
*/
void easyGL_setFont(const font_type_t * font)
{
  current_easyGL.current_font = font;
}

/**
* @brief Get easyGL font
*        
* @param  None
* @retval easyGL font(use type font_type_t)
*/
font_type_t * easyGL_getFont(void)
{
  return (font_type_t *) current_easyGL.current_font;
}

/**
* @brief Get easyGL font
*
* @param  None
* @retval easyGL font(use type font_type_t)
*/
uint16_t easyGL_getTextlengthByFont(const font_type_t * font, char * str)
{
  uint16_t text_length = 0u;
  while(*str)
  {
    text_length += font->Width;
    str++;    
  }
  return text_length;
}

/**
* @brief draw single craracter
*        
* @param position(x,y) and charact
* @retval None
*/  
void easyGL_putCharacter(char charact, int16_t x, int16_t y, const font_type_t * font)
{
  unsigned int line = 0u;
  uint8_t ucharact =(uint8_t) charact;
  
  if(font == NULL)
  {
    font = current_easyGL.current_font;
  }
  
  current_easyGL.console_symbol_XPos = x;
  current_easyGL.console_symbol_YPos = y;
  
  if(font->cross == imag_top_to_bottom)
  {
    line = font->Width; 
    
    if(((charact >= font->char_end) && (font->char_end != 0u)) || (font->char_offset > ucharact))  /* Undefined symbol */ 
    {
      easyGL_drawRect(x, y, font->Width - 1u, font->Height, EASY_GL_DEFAULT_ROUGHTNESS);
    }
    else
    {
      if(font->Height > 8u)
      {
        uint16_t i = 0u;
        for(; i < (font->Height / 8); i++)
        {
          void * ptr =(void*)(((ucharact - font->char_offset) * line) + (unsigned  int)(font->table) + (font->fontBufferWidth * i));
          easyGL_putBWimg(ptr, x, y, font->Width, 8u, font->cross);
          y += 8u;
        }
        if(font->Height & (8u - 1u))
        {
          void * ptr =(void*)(((ucharact - font->char_offset) * line) + (unsigned  int)(font->table) + (font->fontBufferWidth * i));
          easyGL_putBWimg(ptr, x, y, font->Width, (font->Height & (8u - 1u)), font->cross);
        }        
      }
      else
      {
        void * ptr =(void*)(((ucharact - font->char_offset) * line) +(unsigned  int)(font->table));
        easyGL_putBWimg(ptr,x,y,font->Width,font->Height, font->cross);
      }
    }
  } 
  else
    if(font->cross == imag_left_to_right)
    {
      line =((font->Width + 7u) / 8u) * font->Height; 
      if(((charact >= font->char_end) && (font->char_end != 0u)) || (font->char_offset > ucharact))  /* Undefined symbol */ 
      {
        easyGL_drawRect(x, y, font->Width - 1u, font->Height, EASY_GL_DEFAULT_ROUGHTNESS);
      }
      else
      {
        void * ptr = (void*)(((ucharact - font->char_offset) * line) +(unsigned  int)(font->table));
        easyGL_putBWimg(ptr, x, y, font->Width, font->Height, font->cross);
      }
    } 
  
}

/**
* @brief print multiple craracters
*        
* @param position(x,y) and character string
* @retval None
*/  
void easyGL_print(char * string, int16_t x, int16_t y, const font_type_t * font)
{
  int16_t xn = x, yn = y;
  uint16_t i = 0u;
  
  if(font == NULL)
  {
    font = current_easyGL.current_font;
  }
  
  while(string[i] != '\0')
  { 
    if(string[i] == '\n') 
    {
      yn  += font->Height; 
      i++;
      continue;
    }
    
    if(string[i] == '\r') 
    {
      xn = x;
      i++;
      continue;
    }
    
    if(string[i] == '\b') 
    {
      if(xn > font->Width)
      { 
        xn -= font->Width;       
      }
      i++; 
      continue;
    }
    
    if(string[i] == '\v') 
    {
      if(xn > font->Width)
      { 
        xn += (font->Width / 2u) + 2u;       
      }
      i++; 
      continue;
    }
    easyGL_putCharacter(string[i], xn, yn, font);
    xn += font->Width;
    if(xn >= (easyGL_getWidth() - font->Width / 2u))
    { 
      xn = x;
      yn += font->Height;        
    }  
    
    if(yn > easyGL_getHeight())
    {
      yn = y;
    }
    
    i++;
  }
  current_easyGL.console_symbol_XPos = xn;
  current_easyGL.console_symbol_YPos = yn;  
}

/**
* @brief print desired craracters
*        
* @param position(x,y) and character string
* @retval None
*/  
void easyGL_print_s(char * string, int16_t x, int16_t y, uint16_t characters_count, const font_type_t * font)
{
  int16_t xn = x, yn = y;
  uint16_t i = 0u;
  
  if(font == NULL)
  {
    font = current_easyGL.current_font;
  }
  
  while(characters_count--)
  { 
    if(string[i] == '\n') 
    {
      yn  += font->Height; 
      i++;
      continue;
    }
    
    if(string[i] == '\r') 
    {
      xn = x;
      i++;
      continue;
    }
    
    if(string[i] == '\b') 
    {
      if(xn > font->Width)
      { 
        xn -= font->Width;       
      }
      i++; 
      continue;
    }
    
    easyGL_putCharacter(string[i], xn, yn, font);
    xn += font->Width;
    if(xn >= (easyGL_getWidth() - font->Width / 2))
    { 
      xn = x;
      yn += font->Height;        
    }  
    
    if(yn > easyGL_getHeight())
    {
      yn = y;
    }
    
    i++;
  }
  current_easyGL.console_symbol_XPos = xn;
  current_easyGL.console_symbol_YPos = yn;  
}

/**
* @brief print multiple craracters, at last position
*        
* @param string - string
* @retval None
*/  
void easyGL_prints(char * string, const font_type_t * font)
{
  easyGL_print(string, current_easyGL.console_symbol_XPos, current_easyGL.console_symbol_YPos, font);    
}

/**
* @brief Set current easyGL text mode
*        
* @param Text mode
* @retval none
*/  
void easyGL_setDrawMode(easyGL_drawMode_t mode)
{
  current_easyGL.drawing_mode = mode;
}

/**
* @brief Set current easyGL text mode
*        
* @param None
* @retval Text mode
*/  
easyGL_drawMode_t easyGL_getDrawMode(void)
{
  return current_easyGL.drawing_mode;
}

/**
* @brief  Displays a rectangle.
* @param  xs: the x position
* @param  ys: the y position
* @param  height: display rectangle height
* @param  width: display rectangle width
* @retval None
*/
void easyGL_drawRect(int16_t xs, int16_t ys, uint16_t width, uint16_t height, uint16_t roughtness)
{
  if((width > 0u) && (height > 0u))
  {
    easyGL_drawHLine(xs, ys, width, roughtness);
    easyGL_drawHLine(xs, ys + (int16_t)(height - 1u), width, roughtness);
    easyGL_drawVLine(xs, ys, height, roughtness);
    easyGL_drawVLine(xs + (int16_t)(width - 1u), ys, height, roughtness);
  }
}

/**
* @brief  Displays an horizontal line.
* @param  xs: the x position
* @param  ys: the y position
* @param  length: line length
* @retval None
*/
void easyGL_drawHLine(int16_t xs, int16_t ys, uint16_t length, uint16_t roughtness)
{
  easyGL_fillRect(xs, ys, length, roughtness);
}

/**
* @brief  Displays a vertical line.
* @param  xs: the x position
* @param  ys: the y position
* @param  length: line length
* @retval None
*/
void easyGL_drawVLine(int16_t xs, int16_t ys, uint16_t length, uint16_t roughtness)
{
  easyGL_fillRect(xs, ys, roughtness, length);
}

/**
* @brief  Displays an uni-line (between two points).
* @param  x1: the point 1 x position
* @param  y1: the point 1 y position
* @param  x2: the point 2 x position
* @param  y2: the point 2 y position
* @retval None
*/
void easyGL_drawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t roughtness)
{
  int16_t deltax = 0, deltay = 0, x = 0, y = 0, xinc1 = 0, xinc2 = 0, 
  yinc1 = 0, yinc2 = 0, den = 0, num = 0, numadd = 0, numpixels = 0, 
  curpixel = 0;
  
  deltax = ABS(x2 - x1);        /* The difference between the x's */
  deltay = ABS(y2 - y1);        /* The difference between the y's */
  x = x1;                       /* Start x off at the first pixel */
  y = y1;                       /* Start y off at the first pixel */
  
  if (x2 >= x1)                 /* The x-values are increasing */
  {
    xinc1 = 1;
    xinc2 = 1;
  }
  else                          /* The x-values are decreasing */
  {
    xinc1 = -1;
    xinc2 = -1;
  }
  
  if (y2 >= y1)                 /* The y-values are increasing */
  {
    yinc1 = 1;
    yinc2 = 1;
  }
  else                          /* The y-values are decreasing */
  {
    yinc1 = -1;
    yinc2 = -1;
  }
  
  if (deltax >= deltay)         /* There is at least one x-value for every y-value */
  {
    xinc1 = 0;                  /* Don't change the x when numerator >= denominator */
    yinc2 = 0;                  /* Don't change the y for every iteration */
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;         /* There are more x-values than y-values */
  }
  else                          /* There is at least one y-value for every x-value */
  {
    xinc2 = 0;                  /* Don't change the x for every iteration */
    yinc1 = 0;                  /* Don't change the y when numerator >= denominator */
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;         /* There are more y-values than x-values */
  }
  
  for (curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    easyGL_drawPixel(x, y, current_easyGL.activeDrawColor);   /* draw the current pixel */
    num += numadd;                            /* Increase the numerator by the top of the fraction */
    if (num >= den)                           /* Check if numerator >= denominator */
    {
      num -= den;                             /* Calculate the new numerator value */
      x += xinc1;                             /* Change the x as appropriate */
      y += yinc1;                             /* Change the y as appropriate */
    }
    x += xinc2;                               /* Change the x as appropriate */
    y += yinc2;                               /* Change the y as appropriate */
  }
}

/**
* @brief  Displays a circle.
* @param  xs: the x position
* @param  ys: the y position
* @param  Radius: the circle radius
* @retval None
*/
void easyGL_drawCircle(int16_t xs, int16_t ys, int16_t Radius, uint16_t roughtness)
{
  int32_t  d;      /* Decision Variable */ 
  uint32_t  curx;  /* Current x Value */
  uint32_t  cury;  /* Current y Value */ 
  
  while(roughtness != 0u)
  {
    d = 3 - (Radius << 1u);
    curx = 0u;
    cury = Radius;
    
    while (curx <= cury)
    {
      easyGL_drawPixel((xs + curx), (ys - cury), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs - curx), (ys - cury), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs + cury), (ys - curx), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs - cury), (ys - curx), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs + curx), (ys + cury), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs - curx), (ys + cury), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs + cury), (ys + curx), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs - cury), (ys + curx), current_easyGL.activeDrawColor);   
      
      if (d < 0)
      { 
        d += (curx << 2u) + 6;
      }
      else
      {
        d += ((curx - cury) << 2u) + 10;
        cury--;
      }
      curx++;
    }
    if(Radius != 0)
    {
      roughtness--;
      Radius--;
    }
    else
    {
      break;
    }
  }
}

/**
* @brief  fill triangle.
* @param  x1: the point 1 x position
* @param  y1: the point 1 y position
* @param  x2: the point 2 x position
* @param  y2: the point 2 y position
* @param  x3: the point 3 x position
* @param  y3: the point 3 y position
* @retval None
*/
void easyGL_drawTriangle(int16_t x1, int16_t x2, int16_t x3, int16_t y1, int16_t y2, int16_t y3, uint16_t roughtness)
{
  easyGL_drawLine(x1, y1, x2, y2, roughtness); 
  easyGL_drawLine(x3, y3, x2, y2, roughtness); 
  easyGL_drawLine(x1, y1, x3, y3, roughtness);   
}

/**
* @brief  Displays an poly-line (between many points).
* @param  Points: pointer to the points array
* @param  PointCount: Number of points
* @retval None
*/
void easyGL_drawPolygon(easyGL_pPoint_t Points, uint16_t PointCount, uint16_t roughtness)
{
  int16_t x = 0, y = 0;
  
  if(PointCount < 2u)
  {
    return;
  }
  
  easyGL_drawLine(Points->x, Points->y, (Points+PointCount - 1u)->x, (Points+PointCount - 1u)->y, roughtness);
  
  while(--PointCount)
  {
    x = Points->x;
    y = Points->y;
    Points++;
    easyGL_drawLine(x, y, Points->x, Points->y, roughtness);
  }
}

/**
* @brief  Displays an Ellipse.
* @param  xs: the x position
* @param  ys: the y position
* @param  xRadius: the x radius of ellipse
* @param  yRadius: the y radius of ellipse
* @retval None
*/
void easyGL_drawEllipse(int16_t xs, int16_t ys, uint16_t xRadius, uint16_t yRadius, uint16_t roughtness)
{
  while(roughtness != 0)
  {
    int16_t x = 0, y = -((signed)yRadius), err = 2 - 2 * xRadius, e2;
    float k = 0.0f, rad1 = 0.0f, rad2 = 0.0f;
    
    rad1 = xRadius;
    rad2 = yRadius;
    
    k = (float)(rad2 / rad1);
    
    do { 
      easyGL_drawPixel((xs-(int16_t)(x/k)), (ys+y), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs+(int16_t)(x/k)), (ys+y), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs+(int16_t)(x/k)), (ys-y), current_easyGL.activeDrawColor);
      easyGL_drawPixel((xs-(int16_t)(x/k)), (ys-y), current_easyGL.activeDrawColor);      
      
      e2 = err;
      if (e2 <= x) 
      {
        err += ((++x * 2) + 1);
        if (-y == x && e2 <= y) 
        {
          e2 = 0;
        }
      }
      if (e2 > y)
      {
        err += ((++y * 2) + 1);
      }
    } while (y <= 0);
    
    if((xRadius != 0) && (yRadius != 0))
    {
      roughtness--;
      yRadius--;
      xRadius--;
    }
    else
    {
      break;
    }
  }
}

/**
* @brief  Displays a full circle.
* @param  xs: the x position
* @param  ys: the y position
* @param  Radius: the circle radius
* @retval None
*/
void easyGL_fillCircle(int16_t xs, int16_t ys, uint16_t Radius)
{
  int32_t  d;      /* Decision Variable */ 
  uint32_t  curx;  /* Current x Value */
  uint32_t  cury;  /* Current y Value */ 
  
  d = 3 - (Radius << 1);
  
  curx = 0;
  cury = Radius;
  
  while (curx <= cury)
  {
    if(cury > 0u) 
    {
      easyGL_drawHLine(xs - cury, ys + curx, 2 * cury, EASY_GL_DEFAULT_ROUGHTNESS_FILL);
      easyGL_drawHLine(xs - cury, ys - curx, 2 * cury, EASY_GL_DEFAULT_ROUGHTNESS_FILL);
    }
    
    if(curx > 0u) 
    {
      easyGL_drawHLine(xs - curx, ys - cury, 2 * curx, EASY_GL_DEFAULT_ROUGHTNESS_FILL);
      easyGL_drawHLine(xs - curx, ys + cury, 2 * curx, EASY_GL_DEFAULT_ROUGHTNESS_FILL);
    }
    if (d < 0)
    { 
      d += (curx << 2u) + 6;
    }
    else
    {
      d += ((curx - cury) << 2u) + 10;
      cury--;
    }
    curx++;
  }
  easyGL_drawCircle(xs, ys, Radius, EASY_GL_DEFAULT_ROUGHTNESS_FILL);
}

/**
* @brief  fill triangle.
* @param  x1: the point 1 x position
* @param  y1: the point 1 y position
* @param  x2: the point 2 x position
* @param  y2: the point 2 y position
* @param  x3: the point 3 x position
* @param  y3: the point 3 y position
* @retval None
*/
void easyGL_fillTriangle(int16_t x1, int16_t x2, int16_t x3, int16_t y1, int16_t y2, int16_t y3)
{ 
  int16_t deltax = 0, deltay = 0, x = 0, y = 0, xinc1 = 0, xinc2 = 0, 
  yinc1 = 0, yinc2 = 0, den = 0, num = 0, numadd = 0, numpixels = 0, 
  curpixel = 0;
  
  deltax = ABS(x2 - x1);        /* The difference between the x's */
  deltay = ABS(y2 - y1);        /* The difference between the y's */
  x = x1;                       /* Start x off at the first pixel */
  y = y1;                       /* Start y off at the first pixel */
  
  if (x2 >= x1)                 /* The x-values are increasing */
  {
    xinc1 = 1;
    xinc2 = 1;
  }
  else                          /* The x-values are decreasing */
  {
    xinc1 = -1;
    xinc2 = -1;
  }
  
  if (y2 >= y1)                 /* The y-values are increasing */
  {
    yinc1 = 1;
    yinc2 = 1;
  }
  else                          /* The y-values are decreasing */
  {
    yinc1 = -1;
    yinc2 = -1;
  }
  
  if (deltax >= deltay)         /* There is at least one x-value for every y-value */
  {
    xinc1 = 0;                  /* Don't change the x when numerator >= denominator */
    yinc2 = 0;                  /* Don't change the y for every iteration */
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax;         /* There are more x-values than y-values */
  }
  else                          /* There is at least one y-value for every x-value */
  {
    xinc2 = 0;                  /* Don't change the x for every iteration */
    yinc1 = 0;                  /* Don't change the y when numerator >= denominator */
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay;         /* There are more y-values than x-values */
  }
  
  for (curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    easyGL_drawLine(x, y, x3, y3, EASY_GL_DEFAULT_ROUGHTNESS_FILL);
    
    num += numadd;              /* Increase the numerator by the top of the fraction */
    if (num >= den)             /* Check if numerator >= denominator */
    {
      num -= den;               /* Calculate the new numerator value */
      x += xinc1;               /* Change the x as appropriate */
      y += yinc1;               /* Change the y as appropriate */
    }
    x += xinc2;                 /* Change the x as appropriate */
    y += yinc2;                 /* Change the y as appropriate */
  } 
}

/**
* @brief  Displays a full poly-line (between many points).
* @param  easyGL_point_ts: pointer to the points array
* @param  PointCount: Number of points
* @retval None
*/
void easyGL_fillPolygon(easyGL_pPoint_t Points, uint16_t PointCount)
{
  
  int16_t x = 0, y = 0, x2 = 0, y2 = 0, xcenter = 0, ycenter = 0, xfirst = 0, yfirst = 0, pixelx = 0, pixely = 0, counter = 0;
  int16_t  imageleft = 0, imageright = 0, imagetop = 0, imagebottom = 0;  
  
  imageleft = imageright = Points->x;
  imagetop = imagebottom = Points->y;
  
  for(counter = 1; counter < PointCount; counter++)
  {
    pixelx = GET_X_FROM_POINT(counter);
    if(pixelx < imageleft)
    {
      imageleft = pixelx;
    }
    if(pixelx > imageright)
    {
      imageright = pixelx;
    }
    
    pixely = GET_Y_FROM_POINT(counter);
    if(pixely < imagetop)
    { 
      imagetop = pixely;
    }
    if(pixely > imagebottom)
    {
      imagebottom = pixely;
    }
  }  
  
  if(PointCount < 2)
  {
    return;
  }
  
  xcenter = (imageleft + imageright) / 2;
  ycenter = (imagebottom + imagetop) / 2;
  
  xfirst = Points->x;
  yfirst = Points->y;
  
  while(--PointCount)
  {
    x = Points->x;
    y = Points->y;
    Points++;
    x2 = Points->x;
    y2 = Points->y;    
    
    easyGL_fillTriangle(x, x2, xcenter, y, y2, ycenter);
    easyGL_fillTriangle(x, xcenter, x2, y, ycenter, y2);
    easyGL_fillTriangle(xcenter, x2, x, ycenter, y2, y);   
  }
  
  easyGL_fillTriangle(xfirst, x2, xcenter, yfirst, y2, ycenter);
  easyGL_fillTriangle(xfirst, xcenter, x2, yfirst, ycenter, y2);
  easyGL_fillTriangle(xcenter, x2, xfirst, ycenter, y2, yfirst);   
}

/**
* @brief  draw a full ellipse.
* @param  xs: the x position
* @param  ys: the y position
* @param  xRadius: x radius of ellipse
* @param  yRadius: y radius of ellipse. 
* @retval None
*/
void easyGL_fillEllipse(int16_t xs, int16_t ys, uint16_t xRadius, uint16_t yRadius)
{
  int16_t x = 0, y = -((signed)yRadius), err = 2 - 2 * xRadius, e2;
  float k = 0, rad1 = 0, rad2 = 0;
  
  rad1 = xRadius;
  rad2 = yRadius;
  k = (float)(rad2 / rad1);
  
  do 
  { 
    easyGL_drawHLine((int16_t)(xs - (x / k)), (ys + y), (int16_t)(2 * (x / k) + 1), EASY_GL_DEFAULT_ROUGHTNESS_FILL);
    easyGL_drawHLine((int16_t)(xs - (x / k)), (ys - y), (int16_t)(2 * (x / k) + 1), EASY_GL_DEFAULT_ROUGHTNESS_FILL);
    
    e2 = err;
    if (e2 <= x) 
    {
      err += ((++x * 2) + 1);
      if ((-y == x) && (e2 <= y)) 
      {
        e2 = 0;
      }
    }
    if (e2 > y) 
    {
      err += ((++y * 2) + 1);
    }
  } while (y <= 0);
}

/*****************************************************************************/
/*                           End of file                                     */
/*****************************************************************************/
