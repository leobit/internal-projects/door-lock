//******************************************************************************
//!  File description:
//!   @file    device.h
//!   @author  Dmytro Kryvyi
//!   @version V1.0.0
//!   @date    28/11/2016
//!   @brief   This file includes the device depedencies
//!
//!                     Copyright (C) 2016
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | ----------------
//!  28/11/2016 | Dmytro Kryvyi    | Initial draft
//
//******************************************************************************

#ifndef __DEVICE__H_
#define __DEVICE__H_

/* Includes ------------------------------------------------------------------*/
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/

/* Device orientation */
typedef enum
{
  DEVICE_ORIENT_LANDSCAPE_TOP,
  DEVICE_ORIENT_LANDSCAPE_BOTTOM,
  DEVICE_ORIENT_PORTRAIT_LEFT,
  DEVICE_ORIENT_PORTRAIT_RIGHT 
} device_orientation_t;

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Image cration */
/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
extern "C" {
#endif
  
  
#ifdef __cplusplus
}
#endif

#endif  /* __DEVICE__H_ */

/******************************************************************************/
/*                            End of file                                     */
/******************************************************************************/
