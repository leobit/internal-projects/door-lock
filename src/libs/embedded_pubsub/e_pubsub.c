//!******************************************************************************
//!   @file    e_pubsub.c
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    05/03/2018
//!   @brief   This is C source file for Embedded Publish Subscribe Library.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  05/03/2018 | Dmytro Kryvyi    | Initial draft
//!  13/03/2018 | Dmytro Kryvyi    | Beta
//!  16/03/2018 | Dmytro Kryvyi    | Code refactoring
//!  04/04/2018 | Dmytro Kryvyi    | Doxygen style and code updated
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "e_pubsub.h"
#include "e_pubsub_config.h"
#include "osa_layer.h"
#include <string.h>

/* Private define section 0 --------------------------------------------------*/

/* Module name for debug purpose */
#define MODULE_NAME "EPUBSUB"

/* Private typedef -----------------------------------------------------------*/

/** Subscriber structure typedef */
typedef struct
{
  void * pPrevSubscriber;                   ///< Previous subscriber.
  void * pNextSubscriber;                   ///< Next subscriber.
  void * pParentTopicHandle;                ///< Parent topic.
  void ** clientHandle;                     ///< Pointer to client handle.
  pubsub_callback_t subscriberCallback;     ///< Subscriber callback fnc.
  pubsub_msgFlags_t subscriberMsgFlags;     ///< Subscriber message flags.
} pubsub_subscriber_t;

/** Topic structure typedef */
typedef struct
{
  const char * pTopicName;                  ///< Pointer to topic name.
  void * pPrevTopicHandle;                  ///< Previous topic pointer.
  void * pNextTopicHandle;                  ///< Next topic pointer.
  pubsub_subscriber_t * pFirstSubscriber;   ///< Pointer to first topic subscriber.
  pubsub_subscriber_t * pLastSubscriber;    ///< Pointer to last topic subscriber.
  pubsub_topicProperties_t topicProperties; ///< Topic properties.
  pubsub_topicID_t topicID;                 ///< Topic ID.
} pubsub_topic_t;

/* Private define section 0 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Variables for local topics */

/* Thread handle */
static osa_THandle_t localTopicThreadHandle = (osa_THandle_t) NULL;

/* Queue handle */
static osa_QHandle_t localQueueHandle = (osa_QHandle_t) NULL;

/* Topic linked lists */
static pubsub_topic_t * pLocalTopicLastNode = (pubsub_topic_t *) NULL;
static pubsub_topic_t * pLocalTopicFirstNode = (pubsub_topic_t *) NULL;

/* Mutex handle */
static osa_MHandle_t localMuxHandle = (osa_MHandle_t) NULL;

/* Variables for shared topics */

/* Transmitter callbacks */
static volatile pubsub_transmitterCallback_t pSharedTransmitterFunc = (pubsub_transmitterCallback_t) NULL;
static volatile pubsub_transmitterCallback_t pLocalTransmitterFunc = (pubsub_transmitterCallback_t) NULL;

/* Thread handle */
static osa_THandle_t sharedTopicThreadHandle = (osa_THandle_t) NULL;

/* Queues handle */
static osa_QHandle_t sharedQueueHandle = (osa_QHandle_t) NULL;

/* Topic linked lists */
static pubsub_topic_t * pSharedTopicFirstNode = (pubsub_topic_t *) NULL;
static pubsub_topic_t * pSharedTopicLastNode = (pubsub_topic_t *) NULL;

/* Mutex handle */
static osa_MHandle_t sharedMuxHandle = (osa_MHandle_t) NULL;

/* Statistics and management */
static size_t totalLocalSubscribers = 0u;
static size_t totalSharedSubscribers = 0u;
static size_t totalLocalTopics = 0u;
static size_t totalSharedTopics = 0u;

/* Private function prototypes -----------------------------------------------*/
static void * localTopicThread_function(osa_threadArg_t * pArg);
static void * sharedTopicThread_function(osa_threadArg_t * pArg);
static pubsub_topicID_t pubsub_nameToID(const char * pTopicName);

/* Private functions ---------------------------------------------------------*/

/**
 * @brief	Local topic dispatcher thread.
 *
 * @param[in]	pArg	Thread parameter.
 *
 * @retval	NULL - not used.
 */
static void * localTopicThread_function(osa_threadArg_t * pArg)
{
  /* Due to right way to exit set thread entered flag to true */
  pArg->threadEnteredFlag = true;

  /* While threadTerminateFlag equal to false */
  while (pArg->threadTerminateFlag == false)
  {
    /* User code there */
    pubsub_packet_t * pPacket = (pubsub_packet_t *) NULL;
    size_t msg_len = 0u;
    /* We use prio to determine the messages that received from other machine */
    uint32_t msgPrio = EPB_PACKET_INTERNAL_PRIO;

    if (osa_Qpeek(localQueueHandle, (void **) &pPacket, &msg_len, &msgPrio, PUBSUB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
    {
      /* Topic local - take the mutex to prevent of modification of linked list */
      VALIDATE_CRITIC((localMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(localMuxHandle, 10u) == RET_CODE_SUCCESS)
      {
        pubsub_topic_t * pTempTopic = pLocalTopicFirstNode;
        while (pTempTopic != (pubsub_topic_t *) NULL)
        {
          if (pTempTopic->topicID == pPacket->topicID)
          {
            pubsub_subscriber_t * subscriber = (pubsub_subscriber_t *) pTempTopic->pFirstSubscriber;
            /* Call subscribers callback */
            while(subscriber != (pubsub_subscriber_t *) NULL)
            {
              /* If subscriber have atleast one same flag as message */
              if ((pPacket->msgFlags & subscriber->subscriberMsgFlags) != (pubsub_msgFlags_t) 0u)
              {
                VALIDATE_CRITIC((subscriber->subscriberCallback == (pubsub_callback_t) NULL), RET_CODE_ERROR_NULL, "Subscriber callback NULL pointer!");
                if (subscriber->subscriberCallback != (pubsub_callback_t) NULL)
                {
                  subscriber->subscriberCallback((const pubsub_subscriberHandle_t *) subscriber, (const void *) &pPacket->message[0], pPacket->length, pPacket->msgFlags);
                }
              }
              subscriber = (pubsub_subscriber_t *) subscriber->pNextSubscriber;
            }
            break;
          }
          pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
        }
        ret_code_t mux_ret = osa_Munlock(localMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");

        /* If transmitter was set and message from internal source */
        if ((pLocalTransmitterFunc != (pubsub_transmitterCallback_t) NULL) && (msgPrio != EPB_PACKET_EXTERNAL_PRIO))
        {
          pLocalTransmitterFunc(pPacket, msg_len);
        }

        ssize_t ret_size = osa_Qreceive(localQueueHandle, NULL, EPB_LOCAL_QMAXELEMENTSIZE, NULL, PUBSUB_DEFAULT_TIMEOUT);
        VALIDATE_CRITIC((ret_size == -1), RET_CODE_ERROR_INVALID_STATE, "Failed to receive message!");
      }
    }

    osa_Tyield();

    /* Custom suspend mechanism */
    while (pArg->threadSuspendedFlag)
    {
      osa_TsleepMS(1u);
      if (pArg->threadTerminateFlag == true)
      {
        break;
      }
    }
  }

  UTIL_DBG_INFO("The thread localTopicThread_function finished!");

  pArg->threadTerminatedFlag = true;
  osa_Texit(NULL);
  return NULL;
}

/**
 * @brief	Shared topic dispatcher thread.
 *
 * @param[in]	pArg	Thread parameter.
 *
 * @retval	NULL - not used.
 */
static void * sharedTopicThread_function(osa_threadArg_t * pArg)
{
  /* Due to right way to exit set thread entered flag to true */
  pArg->threadEnteredFlag = true;

  /* While threadTerminateFlag equal to false */
  while (pArg->threadTerminateFlag == false)
  {
    /* User code there */
    pubsub_packet_t * pPacket = (pubsub_packet_t *) NULL;
    size_t msg_len = 0u;
    /* We use prio to determine the messages that received from other machine */
    uint32_t msgPrio = EPB_PACKET_INTERNAL_PRIO;
    if (osa_Qpeek(sharedQueueHandle, (void **) &pPacket, &msg_len, &msgPrio, PUBSUB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
    {
      /* Topic shared - take the mutex to prevent of modification of linked list */
      VALIDATE_CRITIC((sharedMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(sharedMuxHandle, 10u) == RET_CODE_SUCCESS)
      {
        pubsub_topic_t * pTempTopic = pSharedTopicFirstNode;
        while (pTempTopic != (pubsub_topic_t *) NULL)
        {
          if (pTempTopic->topicID == pPacket->topicID)
          {
            pubsub_subscriber_t * subscriber = (pubsub_subscriber_t *) pTempTopic->pFirstSubscriber;
            /* Call subscribers callback */
            while(subscriber != (pubsub_subscriber_t *) NULL)
            {
              /* If subscriber have atleast one same flag as message */
              if ((pPacket->msgFlags & subscriber->subscriberMsgFlags) != (pubsub_msgFlags_t) 0u)
              {
                VALIDATE_CRITIC((subscriber->subscriberCallback == (pubsub_callback_t) NULL), RET_CODE_ERROR_NULL, "Subscriber callback NULL pointer!");
                if (subscriber->subscriberCallback != (pubsub_callback_t) NULL)
                {
                  subscriber->subscriberCallback((const pubsub_subscriberHandle_t *) subscriber, (const void *) &pPacket->message[0], pPacket->length, pPacket->msgFlags);
                }
              }
              subscriber = (pubsub_subscriber_t *) subscriber->pNextSubscriber;
            }
            break;
          }
          pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
        }
        ret_code_t mux_ret = osa_Munlock(sharedMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");

        /* If transmitter was set and message from internal source */
        if ((pSharedTransmitterFunc != (pubsub_transmitterCallback_t) NULL) && (msgPrio != EPB_PACKET_EXTERNAL_PRIO))
        {
          pSharedTransmitterFunc(pPacket, msg_len);
        }
        ssize_t ret_size = osa_Qreceive(sharedQueueHandle, NULL, EPB_SHARED_QMAXELEMENTSIZE, NULL, PUBSUB_DEFAULT_TIMEOUT);
        VALIDATE_CRITIC((ret_size == -1), RET_CODE_ERROR_INVALID_STATE, "Failed to receive message!");
      }
    }

    osa_Tyield();

    /* Custom suspend mechanism */
    while (pArg->threadSuspendedFlag)
    {
      osa_TsleepMS(1u);
      if (pArg->threadTerminateFlag == true)
      {
        break;
      }
    }
  }

  UTIL_DBG_INFO("The thread sharedTopicThread_function finished!");

  pArg->threadTerminatedFlag = true;
  osa_Texit(NULL);
  return NULL;
}

/**
 * @brief	Converts the name of topic to fixed length topic ID.
 *
 * @param[in]	pTopicName - pointer to string.
 *
 * @retval	pubsub_topicID_t value that generated from name.
 */
static pubsub_topicID_t pubsub_nameToID(const char * pTopicName)
{
  if (pTopicName != (const char *) NULL)
  {
  	return (pubsub_topicID_t) osa_uHash((uint8_t *) pTopicName, strlen(pTopicName));
  }
  return NULL_TOPIC;
}

/* Exported functions ------------------------------------------------------- */

/**
 * @brief	Initialize the pubsub library.
 *
 * @param	None.
 *
 * @retval	RET_CODE_SUCCESS				If initialization was successful.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal problems, you may call this function again to get RET_CODE_SUCCESS.
 */
ret_code_t pubsub_initialize(void)
{
  ret_code_t ret_code = RET_CODE_SUCCESS;

  /* Initialize the queues */
  osa_QAttr qAttr;
  qAttr.q_max_hold_msgs_num = EPB_LOCAL_QELEMENTS;
  qAttr.q_max_msg_size = EPB_LOCAL_QMAXELEMENTSIZE;
  if (localQueueHandle == (osa_QHandle_t) NULL)
  {
    if (osa_Qcreate(&localQueueHandle, "EPB_LQ", &qAttr) != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }

  qAttr.q_max_hold_msgs_num = EPB_SHARED_QELEMENTS;
  qAttr.q_max_msg_size = EPB_SHARED_QMAXELEMENTSIZE;
  if (sharedQueueHandle == (osa_QHandle_t) NULL)
  {
    if (osa_Qcreate(&sharedQueueHandle, "EPB_SQ", &qAttr) != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }

  /* Initialize the mutexes */
  if (sharedMuxHandle == (osa_MHandle_t) NULL)
  {
    if (osa_Mcreate(&sharedMuxHandle, false) != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }

  if (localMuxHandle == (osa_MHandle_t) NULL)
  {
    if (osa_Mcreate(&localMuxHandle ,false) != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }

  /* Create the threads */
  if(ret_code == RET_CODE_SUCCESS)
  {
    if (sharedTopicThreadHandle == (osa_THandle_t) NULL)
    {
      if (osa_Tcreate(&sharedTopicThreadHandle, sharedTopicThread_function, NULL, "PBS", EPB_SHARED_MANAGER_THREAD_STACK_DEPTH, SHARED_TOPICS_THR_PRIO) != RET_CODE_SUCCESS)
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
    }
  }

  if(ret_code == RET_CODE_SUCCESS)
  {
    if (localTopicThreadHandle == (osa_THandle_t) NULL)
    {
      if (osa_Tcreate(&localTopicThreadHandle, localTopicThread_function, NULL, "PBL", EPB_LOCAL_MANAGER_THREAD_STACK_DEPTH, LOCAL_TOPICS_THR_PRIO) != RET_CODE_SUCCESS)
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
    }
  }

  totalLocalSubscribers = 0u;
  totalLocalTopics = 0u;
  totalSharedSubscribers = 0u;
  totalSharedTopics = 0u;

  return ret_code;
}

/**
 * @brief	De-initialize the pubsub library.
 *
 * @param	None.
 *
 * @retval	RET_CODE_SUCCESS				If de-initialization was successful.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal error: library may be not initialized.
 */
ret_code_t pubsub_deinitialize(void)
{
  ret_code_t ret_code = RET_CODE_SUCCESS;

  /* The maximum response time in MS for thread before hard deleting */
  static const uint32_t threadShutTime = PUBSUB_DEFAULT_TIMEOUT * 2u;
  /* Delete all of the threads */
  if (localTopicThreadHandle != (osa_THandle_t) NULL)
  {
    if (osa_Tdelete(&localTopicThreadHandle, threadShutTime) != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }

  if (sharedTopicThreadHandle != (osa_THandle_t) NULL)
  {
    if (osa_Tdelete(&sharedTopicThreadHandle, threadShutTime) != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }

  if (ret_code == RET_CODE_SUCCESS)
  {
    /* Let transmitter function to be NULL */
    pSharedTransmitterFunc = (pubsub_transmitterCallback_t) NULL;

    /* Delete all of queues */
    if (sharedQueueHandle != (osa_QHandle_t) NULL)
    {
      if (osa_Qdelete(&sharedQueueHandle) != RET_CODE_SUCCESS)
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
    }

    if (localQueueHandle != (osa_QHandle_t) NULL)
    {
      if (osa_Qdelete(&localQueueHandle) != RET_CODE_SUCCESS)
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
    }

    /* Delete all of mutexes */
    if (sharedMuxHandle != (osa_MHandle_t) NULL)
    {
      /* Deleting all of shared topics - take the mutex */
      if (osa_Mlock(sharedMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        pubsub_topic_t * pTempTopic = pSharedTopicFirstNode;
        /* Unlink the first last pointers */
        pSharedTopicLastNode = pSharedTopicFirstNode = (pubsub_topic_t *) NULL;
        while (pTempTopic != (pubsub_topic_t *) NULL)
        {
          /* Delete subscribers and free taken memory */
          pubsub_subscriber_t * subscriber = pTempTopic->pFirstSubscriber;
          while (subscriber != (pubsub_subscriber_t *) NULL)
          {
            if (subscriber->clientHandle != (void **) NULL)
            {
              *(subscriber->clientHandle) = (void *) NULL;
            }
            void * pFreeMem = (void *) subscriber;
            subscriber = (pubsub_subscriber_t *) subscriber->pNextSubscriber;
            FREE(void, pFreeMem);
            totalSharedSubscribers--;
          }

          /* Get the next topic */
          pubsub_topic_t * nextTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          /* Delete topic */
          FREE(pubsub_topic_t, pTempTopic);

          pTempTopic = nextTopic;
          totalSharedTopics--;
        }

        ret_code_t mux_ret = osa_Munlock(sharedMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }

      if (osa_Mdelete(&sharedMuxHandle) != RET_CODE_SUCCESS)
      {
        ret_code = RET_CODE_ERROR_INTERNAL;
      }
    }

    if (localMuxHandle != (osa_MHandle_t) NULL)
    {
      /* Delete all of local topics- take the mutex */
      if (osa_Mlock(localMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        pubsub_topic_t * pTempTopic = pLocalTopicFirstNode;
        /* Unlink the first last pointers */
        pLocalTopicLastNode = pLocalTopicFirstNode = (pubsub_topic_t *) NULL;
        while (pTempTopic != (pubsub_topic_t *) NULL)
        {
          /* Delete subscribers and free taken memory */
          pubsub_subscriber_t * subscriber = pTempTopic->pFirstSubscriber;
          while (subscriber != (pubsub_subscriber_t *) NULL)
          {
            if (subscriber->clientHandle != (void **) NULL)
            {
              *(subscriber->clientHandle) = (void *) NULL;
            }
            void * pFreeMem = (void *) subscriber;
            subscriber = (pubsub_subscriber_t *) subscriber->pNextSubscriber;
            FREE(void, pFreeMem);
            totalLocalSubscribers--;
          }

          /* Get the next topic */
          pubsub_topic_t * nextTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          /* Delete topic */
          FREE(pubsub_topic_t, pTempTopic);

          pTempTopic = nextTopic;
          totalLocalTopics--;
        }

      }
      ret_code_t mux_ret = osa_Munlock(localMuxHandle);
      VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
    }

    if (osa_Mdelete(&localMuxHandle) != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }

  VALIDATE_CRITIC((totalLocalSubscribers != 0), totalLocalSubscribers, "Not all of local clients has been deleted!");
  VALIDATE_CRITIC((totalLocalTopics != 0), totalLocalTopics, "Not all of local topics has been deleted!");

  VALIDATE_CRITIC((totalSharedSubscribers != 0), totalSharedSubscribers, "Not all of shared clients has been deleted!");
  VALIDATE_CRITIC((totalSharedTopics != 0), totalSharedTopics, "Not all of shared topics has been deleted!");

  return ret_code;
}

/**
 * @brief	Set the handler for out coming data Shared stream.
 *
 * @param[in]	pTransmitterFunc - pointer to function that will handle the out coming data for other remote topics.
 *
 * @retval	RET_CODE_SUCCESS		If callback set successfully.
 */
ret_code_t pubsub_registerSharedTransport(pubsub_transmitterCallback_t pTransmitterFunc)
{
  ret_code_t ret_code = RET_CODE_SUCCESS;
  pSharedTransmitterFunc = pTransmitterFunc;
  return ret_code;
}

/**
 * @brief	Set the handler for out coming data Local stream.
 *
 * @param[in]	pTransmitterFunc - pointer to function that will handle the out coming data for other remote topics.
 *
 * @retval	RET_CODE_SUCCESS		If callback set successfully.
 */
ret_code_t pubsub_registerLocalTransport(pubsub_transmitterCallback_t pTransmitterFunc)
{
  ret_code_t ret_code = RET_CODE_SUCCESS;
  pLocalTransmitterFunc = pTransmitterFunc;
  return ret_code;
}


/**
 * @brief	Call this function to handle the data that arrived from other device/module.
 *
 * @param[in]	pData - pointer to raw packet data.
 * @param[in]	size - size of data.
 * @param[in]   queue_to_shared - queue msg to shared topic.
 *
 * @retval	RET_CODE_SUCCESS				If data successfully handled and enqueued.
 * @retval	RET_CODE_ERROR_INVALID_LENGTH	If data corrupted and length not corrects.
 * @retval	RET_CODE_ERROR_INTERNAL			If internal error: library may be not initialized or queue error.
 */
ret_code_t pubsub_handleData(const void * pData, const size_t size, bool queue_to_shared)
{
  ret_code_t ret_code = RET_CODE_ERROR_INVALID_LENGTH;
  pubsub_packet_t * pPacket = (pubsub_packet_t *) pData;
  size_t packetSize = (sizeof(pubsub_packet_t) + pPacket->length);

  /* Verify the packet */
  if (packetSize == size)
  {
    if (queue_to_shared == true)
    {
      ret_code = osa_Qsend(sharedQueueHandle, pData, size, EPB_PACKET_EXTERNAL_PRIO, PUBSUB_DEFAULT_TIMEOUT);
    }
    else
    {
      ret_code = osa_Qsend(localQueueHandle, pData, size, EPB_PACKET_EXTERNAL_PRIO, PUBSUB_DEFAULT_TIMEOUT);
    }
    if (ret_code != RET_CODE_SUCCESS)
    {
      ret_code = RET_CODE_ERROR_INTERNAL;
    }
  }
  return ret_code;
}
/**
 * @brief	Get total created clients.
 *
 * @param	None.
 *
 * @retval	The number of created clients.
 */
size_t pubsub_getSubscribersCount(void)
{
  return (totalLocalSubscribers + totalSharedSubscribers);
}

/**
 * @brief	Get total created topics.
 *
 * @param	None.
 *
 * @retval	The number of created topics.
 */
size_t pubsub_getTopicsCount(void)
{
  return (totalSharedTopics + totalLocalTopics);
}

/**
 * @brief	Create topic.
 *
 * @param[in]	pTopicName - pointer to the name of topic.
 * @param[in]	flags - the flags of topic @ref pubsub_topicProperties_t.
 *
 * @retval	The created Topic ID or NULL_TOPIC if topic not created.
 */
pubsub_topicID_t pubsub_serverCreateTopic(const char * pTopicName, pubsub_topicProperties_t flags)
{
  pubsub_topicID_t retTopicID = NULL_TOPIC;
  pubsub_topic_t * topicNode = (pubsub_topic_t *) NULL;

  MALLOC(pubsub_topic_t, topicNode, sizeof(pubsub_topic_t));
  if ((topicNode != (pubsub_topic_t *) NULL) && (pTopicName != (const char *) NULL))
  {
    /* Initialize created topic node */
    topicNode->pTopicName = pTopicName;
    topicNode->pFirstSubscriber = (pubsub_subscriber_t *) NULL;
    topicNode->pLastSubscriber = (pubsub_subscriber_t *) NULL;
    topicNode->pNextTopicHandle = (void *) NULL;
    topicNode->pPrevTopicHandle = (void *) NULL;
    retTopicID = topicNode->topicID = pubsub_nameToID(pTopicName);
    topicNode->topicProperties = flags;

    /* Checks the shared flag */
    if ((flags & PUBSUB_TPROP_SHARED_TOPIC) != 0u)
    {
      /* Topic shared - take the mutex to modify linked list */
      VALIDATE_CRITIC((sharedMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(sharedMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        /* Topic exixstimg flag */
        bool exists = false;
        /* This topic is first in the list */
        if (pSharedTopicFirstNode == (pubsub_topic_t *) NULL)
        {
          pSharedTopicFirstNode = topicNode;
        }
        else
        {
          /* Check for topic existings */
          pubsub_topic_t * pTempTopic = pSharedTopicFirstNode;
          while (pTempTopic != (pubsub_topic_t *) NULL)
          {
            /* If topic ID equal to the desired topic ID */
            if (pTempTopic->topicID == retTopicID)
            {
              exists = true;
              break;
            }
            pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          }

          /* If topic not exist - link created one */
          if (exists == false)
          {
            VALIDATE_CRITIC((pSharedTopicLastNode == (pubsub_topic_t *) NULL), RET_CODE_ERROR_NULL, "It seems that topic linked list damaged!");
            if (pSharedTopicLastNode != (pubsub_topic_t *) NULL)
            {
              topicNode->pPrevTopicHandle = (void *) pSharedTopicLastNode;
              pSharedTopicLastNode->pNextTopicHandle = (void *) topicNode;
            }
          }
        }

        /* If topic already created */
        if (exists == false)
        {
          pSharedTopicLastNode = topicNode;
          totalSharedTopics++;
        }
        else
        {
          FREE(pubsub_topic_t, topicNode);
        }
        ret_code_t mux_ret = osa_Munlock(sharedMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        FREE(pubsub_topic_t, topicNode);
        retTopicID = NULL_TOPIC;
      }
    }
    else
    {
      bool exists = false;
      /* Topic local - take the mutex to modify linked list */
      VALIDATE_CRITIC((localMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(localMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        if (pLocalTopicFirstNode == (pubsub_topic_t *) NULL)
        {
          pLocalTopicFirstNode = topicNode;
        }
        else
        {
          /* Check for topic existings */
          pubsub_topic_t * pTempTopic = pLocalTopicFirstNode;
          while (pTempTopic != (pubsub_topic_t *) NULL)
          {
            /* If topic ID equal to the desired topic ID */
            if (pTempTopic->topicID == retTopicID)
            {
              exists = true;
              break;
            }
            pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          }

          /* If topic not exist - link created one */
          if (exists == false)
          {
            VALIDATE_CRITIC((pLocalTopicLastNode == (pubsub_topic_t *) NULL), RET_CODE_ERROR_NULL, "It seems that topic linked list damaged!");
            if (pLocalTopicLastNode != (pubsub_topic_t *) NULL)
            {
              topicNode->pPrevTopicHandle = (void *)pLocalTopicLastNode;
              pLocalTopicLastNode->pNextTopicHandle = (void *) topicNode;
            }
          }
        }

        /* If topic already created */
        if (exists == false)
        {
          pLocalTopicLastNode = topicNode;
          totalLocalTopics++;
        }
        else
        {
          FREE(pubsub_topic_t, topicNode);
        }
        ret_code_t mux_ret = osa_Munlock(localMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        FREE(pubsub_topic_t, topicNode);
        retTopicID = NULL_TOPIC;
      }
    }
  }
  return retTopicID;
}

/**
 * @brief	Create topic by topic indentificator.
 *
 * @param[in]	topicID - th topic ID.
 * @param[in]	flags - the flags of topic @ref pubsub_topicProperties_t.
 *
 * @retval	The created Topic ID or NULL_TOPIC if topic not created.
 */
pubsub_topicID_t pubsub_serverCreateTopicByID(pubsub_topicID_t topicID, pubsub_topicProperties_t flags)
{
  pubsub_topicID_t retTopicID = NULL_TOPIC;
  pubsub_topic_t * topicNode = (pubsub_topic_t *) NULL;

  MALLOC(pubsub_topic_t, topicNode, sizeof(pubsub_topic_t));
  if ((topicNode != (pubsub_topic_t *) NULL) && (topicID != NULL_TOPIC))
  {
    /* Initialize created topic node */
    topicNode->pTopicName = "Unknown topic";
    topicNode->pFirstSubscriber = (pubsub_subscriber_t *) NULL;
    topicNode->pLastSubscriber = (pubsub_subscriber_t *) NULL;
    topicNode->pNextTopicHandle = (void *) NULL;
    topicNode->pPrevTopicHandle = (void *) NULL;
    retTopicID = topicNode->topicID = topicID;
    topicNode->topicProperties = flags;

    /* Check the shared flag */
    if ((flags & PUBSUB_TPROP_SHARED_TOPIC) != 0u)
    {
      /* Topic shared - take the mutex to modify linked list */
      VALIDATE_CRITIC((sharedMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(sharedMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        /* Topic exixstimg flag */
        bool exists = false;
        /* This topic is first in the list */
        if (pSharedTopicFirstNode == (pubsub_topic_t *) NULL)
        {
          pSharedTopicFirstNode = topicNode;
        }
        else
        {
          /* Check for topic existings */
          pubsub_topic_t * pTempTopic = pSharedTopicFirstNode;
          while (pTempTopic != (pubsub_topic_t *) NULL)
          {
            /* If topic ID equal to the desired topic ID */
            if (pTempTopic->topicID == topicID)
            {
              exists = true;
              break;
            }
            pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          }

          /* If topic not exist - link created one */
          if (exists == false)
          {
            VALIDATE_CRITIC((pSharedTopicLastNode == (pubsub_topic_t *) NULL), RET_CODE_ERROR_NULL, "It seems that topic linked list damaged!");
            if (pSharedTopicLastNode != (pubsub_topic_t *) NULL)
            {
              topicNode->pPrevTopicHandle = (void *) pSharedTopicLastNode;
              pSharedTopicLastNode->pNextTopicHandle = (void *) topicNode;
            }
          }
        }

        /* If topic already created */
        if (exists == false)
        {
          pSharedTopicLastNode = topicNode;
          totalSharedTopics++;
        }
        else
        {
          FREE(pubsub_topic_t, topicNode);
        }
        ret_code_t mux_ret = osa_Munlock(sharedMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        FREE(pubsub_topic_t, topicNode);
        retTopicID = NULL_TOPIC;
      }
    }
    else
    {
      bool exists = false;
      /* Topic local - take the mutex to modify linked list */
      VALIDATE_CRITIC((localMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(localMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        if (pLocalTopicFirstNode == (pubsub_topic_t *) NULL)
        {
          pLocalTopicFirstNode = topicNode;
        }
        else
        {
          /* Check for topic existings */
          pubsub_topic_t * pTempTopic = pLocalTopicFirstNode;
          while (pTempTopic != (pubsub_topic_t *) NULL)
          {
            /* If topic ID equal to the desired topic ID */
            if (pTempTopic->topicID == topicID)
            {
              exists = true;
              break;
            }
            pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          }

          /* If topic not exist - link created one */
          if (exists == false)
          {
            VALIDATE_CRITIC((pLocalTopicLastNode == (pubsub_topic_t *) NULL), RET_CODE_ERROR_NULL, "It seems that topic linked list damaged!");
            if (pLocalTopicLastNode != (pubsub_topic_t *) NULL)
            {
              topicNode->pPrevTopicHandle = (void *)pLocalTopicLastNode;
              pLocalTopicLastNode->pNextTopicHandle = (void *) topicNode;
            }
          }
        }

        /* If topic already created */
        if (exists == false)
        {
          pLocalTopicLastNode = topicNode;
          totalLocalTopics++;
        }
        else
        {
          FREE(pubsub_topic_t, topicNode);
        }
        ret_code_t mux_ret = osa_Munlock(localMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        FREE(pubsub_topic_t, topicNode);
        retTopicID = NULL_TOPIC;
      }
    }
  }
  return retTopicID;
}

/**
 * @brief	Publish data to desired Topic's subscribers by topic ID or name.
 *
 * @param[in]	pTopicName - the name of topic, if NULL - not used.
 * @param[in]	topicID - the ID of topic, if NULL_TOPIC - not used.
 * @param[in]	pData - the pointer to data.
 * @param[in]	size - the size of data to publish.
 * @param[in]	flags - the flags of published data.
 * @param[in]	timeout - the timeout to enqueue the data.
 *
 * note: The parameter: pTopicName must be not NULL or topicID must not be NULL_TOPIC.
 *
 * @retval	RET_CODE_SUCCESS				If data enqueued to topic.
 * @retval	RET_CODE_ERROR_NO_MEM 			if no memory in HEAP.
 * @retval	RET_CODE_ERROR_NOT_SUPPORTED	if packet enqueued but topic not registered at this machine.
 * @retval	RET_CODE_ERROR_NOT_FOUND		If invalid topic name or ID.
 * @retval	RET_CODE_ERROR_NULL				If NULL pData pointer or size == 0 has been passed to function.
 * @retval	RET_CODE_ERROR_TIMEOUT			If data not enqueued to the FIFO due to timeout.
 * @retval	RET_CODE_ERROR_BUSY				If mutex is busy.
 */
ret_code_t pubsub_serverPublish(const char * pTopicName, pubsub_topicID_t topicID, const void * pData, size_t size, pubsub_msgFlags_t flags,  uint32_t timeout)
{
  ret_code_t ret_code = RET_CODE_ERROR_NOT_FOUND;

  /* Check the topic ID and generate if this needed */
  if (topicID == NULL_TOPIC)
  {
    topicID = pubsub_nameToID(pTopicName);
  }

  if (topicID != NULL_TOPIC)
  {
    /* Checks the arguments */
    if (pData != (const void *) NULL)
    {
      pubsub_topic_t * pTopic = (pubsub_topic_t *) NULL;
      pubsub_topic_t * pTempTopic = (pubsub_topic_t *) NULL;

      /* Searching for topic in the BD */
      /* Topic local - take the mutex */
      VALIDATE_CRITIC((localMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(localMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        pTempTopic = pLocalTopicFirstNode;
        while (pTempTopic != (pubsub_topic_t *) NULL)
        {
          if (pTempTopic->topicID == topicID)
          {
            pTopic = pTempTopic;
            break;
          }
          pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
        }
        ret_code_t mux_ret = osa_Munlock(localMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        ret_code = RET_CODE_ERROR_BUSY;
      }

      /* If topic not found in local list */
      if(pTopic == (pubsub_topic_t *) NULL)
      {
        /* Find in shared topics - take the mutex */
        VALIDATE_CRITIC((sharedMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
        if (osa_Mlock(sharedMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
        {
          pTempTopic = pSharedTopicFirstNode;
          while (pTempTopic != (pubsub_topic_t *) NULL)
          {
            if (pTempTopic->topicID == topicID)
            {
              pTopic = pTempTopic;
              break;
            }
            pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          }
          ret_code_t mux_ret = osa_Munlock(sharedMuxHandle);
          VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
        }
        else
        {
          ret_code = RET_CODE_ERROR_BUSY;
        }
      }

      if (ret_code != RET_CODE_ERROR_BUSY)
      {
        ret_code = RET_CODE_ERROR_NO_MEM;
        pubsub_packet_t * pPacket = (pubsub_packet_t *) NULL;
        MALLOC(pubsub_packet_t, pPacket, (sizeof(pubsub_packet_t) + size));
        if (pPacket != (pubsub_packet_t *) NULL)
        {
          /* Populate the packet */
          pPacket->msgFlags = flags;
          pPacket->length = size;
          memcpy((void *) pPacket->message, pData, size);
          osa_QHandle_t destinationQueueHandle = sharedQueueHandle;
          if (pTopic != (pubsub_topic_t *) NULL)
          {
            pPacket->topicID = pTopic->topicID;
            if ((pTopic->topicProperties & PUBSUB_TPROP_SHARED_TOPIC) == 0u)
            {
              destinationQueueHandle = localQueueHandle;
            }
            ret_code = RET_CODE_SUCCESS;
          }
          else
          {
            ret_code = RET_CODE_ERROR_NOT_SUPPORTED;
            pPacket->topicID = topicID;
          }

          ret_code = osa_Qsend(destinationQueueHandle, pPacket, (sizeof(pubsub_packet_t) + size), EPB_PACKET_INTERNAL_PRIO, timeout);
          FREE(pubsub_packet_t, pPacket);
        }
      }
    }
    else
    {
      ret_code = RET_CODE_ERROR_NULL;
    }
  }

  return ret_code;
}

/**
 * @brief	Delete topic and topic's subscribers by name or ID.
 *
 * @param[in]	pTopicName - the name of topic, if NULL - not used.
 * @param[in]	topicID - the ID of topic, if NULL_TOPIC - not used.
 *
 * note: The parameter: pTopicName must be not NULL or topicID must not be NULL_TOPIC.
 *
 * @retval	RET_CODE_SUCCESS				If topic and subscribers successfully deleted.
 * @retval	RET_CODE_ERROR_NOT_FOUND		If invalid topic name or ID.
 * @retval	RET_CODE_ERROR_BUSY				If mutex is busy.
 */
ret_code_t pubsub_serverDeleteTopic(const char * pTopicName, pubsub_topicID_t topicID)
{
  ret_code_t ret_code = RET_CODE_ERROR_NOT_FOUND;
  /* Check the topic ID and generate if this needed */
  if (topicID == NULL_TOPIC)
  {
    topicID = pubsub_nameToID(pTopicName);
  }

  if (topicID != NULL_TOPIC)
  {
    /* Topic local - take the mutex */
    VALIDATE_CRITIC((localMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
    if (osa_Mlock(localMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
    {
      pubsub_topic_t * pTempTopic = pLocalTopicFirstNode;
      while (pTempTopic != (pubsub_topic_t * ) NULL)
      {
        /* If topic ID equal to the desired topic ID */
        if (pTempTopic->topicID == topicID)
        {
          /* Delete subscribers and free taken memory */
          pubsub_subscriber_t * subscriber = pTempTopic->pFirstSubscriber;
          while (subscriber != (pubsub_subscriber_t *) NULL)
          {
            if (subscriber->clientHandle != (void **) NULL)
            {
              *(subscriber->clientHandle) = (void *) NULL;
            }
            void * pFreeMem = (void *) subscriber;
            subscriber = (pubsub_subscriber_t *) subscriber->pNextSubscriber;
            FREE(void, pFreeMem);
            totalLocalSubscribers--;
          }

          pubsub_topic_t * prevTopic = (pubsub_topic_t *) pTempTopic->pPrevTopicHandle;
          pubsub_topic_t * nextTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;

          /* If previous topic eq NULL then we deleted the first topic in list */
          if (prevTopic == (pubsub_topic_t *) NULL)
          {
            /* It seems that we delete first node in list, let fist node points to next topic node */
            pLocalTopicFirstNode = nextTopic;
            /* If first topic node eq to NULL than no more topics in list, let last node points to NULL*/
            if (pLocalTopicFirstNode == (pubsub_topic_t *) NULL)
            {
              pLocalTopicLastNode = (pubsub_topic_t *) NULL;
            }
          }
          else
          {
            /* Previous topic exists link the next topic */
            prevTopic->pNextTopicHandle = (void *) nextTopic;
            /* If the next topic points to NULL: we delete the last topic, update last topic pointer */
            if (nextTopic == (pubsub_topic_t *) NULL)
            {
              pLocalTopicLastNode = prevTopic;
            }
            else
            {
              nextTopic->pPrevTopicHandle = (void *) prevTopic;
            }
          }

          /* Delete topic */
          FREE(pubsub_topic_t, pTempTopic);
          ret_code = RET_CODE_SUCCESS;
          totalLocalTopics--;
          break;
        }
        /* All is OK. When we free mem upper we break the while! */
        pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
      }
      ret_code_t mux_ret = osa_Munlock(localMuxHandle);
      VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
    }
    else
    {
      ret_code = RET_CODE_ERROR_BUSY;
    }

    /* If topic not found in local list */
    if(ret_code != RET_CODE_SUCCESS)
    {
      /* Find in shared topics - take the mutex */
      VALIDATE_CRITIC((sharedMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(sharedMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        pubsub_topic_t * pTempTopic = pSharedTopicFirstNode;
        while (pTempTopic != (pubsub_topic_t *) NULL)
        {
          /* If topic ID equal to the desired topic ID */
          if (pTempTopic->topicID == topicID)
          {
            /* Delete subscribers and free taken memory */
            pubsub_subscriber_t * subscriber = pTempTopic->pFirstSubscriber;
            while (subscriber != (pubsub_subscriber_t *) NULL)
            {
              if (subscriber->clientHandle != (void **) NULL)
              {
                *(subscriber->clientHandle) = (void *) NULL;
              }
              void * pFreeMem = (void *) subscriber;
              subscriber = (pubsub_subscriber_t *) subscriber->pNextSubscriber;
              FREE(void, pFreeMem);
              totalSharedSubscribers--;
            }

            pubsub_topic_t * prevTopic = (pubsub_topic_t *) pTempTopic->pPrevTopicHandle;
            pubsub_topic_t * nextTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;

            /* If previous topic eq NULL then we deleted the first topic in list */
            if (prevTopic == (pubsub_topic_t *) NULL)
            {
              /* It seems that we delete first node in list, let fist node points to next topic node */
              pSharedTopicFirstNode = nextTopic;
              /* If first topic node eq to NULL than no more topics in list, let last node points to NULL*/
              if (pSharedTopicFirstNode == (pubsub_topic_t *) NULL)
              {
                pSharedTopicLastNode = (pubsub_topic_t *) NULL;
              }
            }
            else
            {
              /* Previous topic exists link the next topic */
              prevTopic->pNextTopicHandle = (void *) nextTopic;
              /* If the next topic points to NULL: we delete the last topic, update last topic pointer */
              if (nextTopic == (pubsub_topic_t *) NULL)
              {
                pSharedTopicLastNode = prevTopic;
              }
              else
              {
                nextTopic->pPrevTopicHandle = (void *) prevTopic;
              }
            }

            /* Delete topic */
            FREE(pubsub_topic_t, pTempTopic);
            ret_code = RET_CODE_SUCCESS;
            totalSharedTopics--;
            break;
          }
          /* All is OK. When we free mem upper we break the while! */
          pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
        }
        ret_code_t mux_ret = osa_Munlock(sharedMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        ret_code = RET_CODE_ERROR_BUSY;
      }
    }
  }
  return ret_code;
}

/**
 * @brief	Create new subscriber and subscribe to desired topic.
 *
 * @param[out]	pSubscriberHandle - the pointer to the static client handle variable, if NULL - not used (subscriber will be created, but handle not be passed back).
 * @param[in]	pTopicName - the name of topic, if NULL - not used.
 * @param[in]	topicID - the ID of topic, if NULL_TOPIC - not used.
 * @param[in]	pCallBack - the pointer to callback function that will be invoked when message arrive.
 * @param[in]	flags - the flags for messaging filtering.
 *
 * note: The parameter: pTopicName must be not NULL or topicID must not be NULL_TOPIC.
 *
 * @retval	RET_CODE_SUCCESS				If subscriber created and and subscribed to topic.
 * @retval	RET_CODE_ERROR_NULL				If NULL pCallBack pointer has been passed to function.
 * @retval	RET_CODE_ERROR_NOT_FOUND		If invalid topic name or ID.
 * @retval	RET_CODE_ERROR_NO_MEM			If no available memory in HEAP.
 * @retval	RET_CODE_ERROR_BUSY				If mutex is busy.
 */
ret_code_t pubsub_createSubscriber(pubsub_subscriberHandle_t * pSubscriberHandle, const char * pTopicName,  pubsub_topicID_t topicID, pubsub_callback_t pCallBack, pubsub_msgFlags_t flags)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  if (pCallBack != (pubsub_callback_t) NULL)
  {
    ret_code = RET_CODE_ERROR_NOT_FOUND;
    /* Check the topic ID and generate if this needed */
    if (topicID == NULL_TOPIC)
    {
      topicID = pubsub_nameToID(pTopicName);
    }

    if (topicID != NULL_TOPIC)
    {
      /* Topic local - take the mutex */
      VALIDATE_CRITIC((localMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
      if (osa_Mlock(localMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        pubsub_topic_t * pTempTopic = pLocalTopicFirstNode;
        while (pTempTopic != (pubsub_topic_t *) NULL)
        {
          /* If topic ID equal to the desired topic ID */
          if (pTempTopic->topicID == topicID)
          {
            /* Create subscriber */
            pubsub_subscriber_t * subscriber = (pubsub_subscriber_t *) NULL;
            MALLOC(pubsub_subscriber_t, subscriber, sizeof(pubsub_subscriber_t));
            if (subscriber != (pubsub_subscriber_t *) NULL)
            {
              /* Fill subscriber fields and link */
              if (pSubscriberHandle)
              {
                *pSubscriberHandle = (pubsub_subscriberHandle_t) subscriber;
              }
              subscriber->clientHandle = (void **)pSubscriberHandle;
              subscriber->subscriberMsgFlags = flags;
              subscriber->pParentTopicHandle = (void *) pTempTopic;
              subscriber->subscriberCallback = pCallBack;
              subscriber->pNextSubscriber = (void *) NULL;

              /* Link the clients */
              subscriber->pPrevSubscriber = (void *) pTempTopic->pLastSubscriber;
              if (pTempTopic->pFirstSubscriber == (pubsub_subscriber_t *) NULL)
              {
                pTempTopic->pFirstSubscriber = subscriber;
              }

              if (pTempTopic->pLastSubscriber != (pubsub_subscriber_t *) NULL)
              {
                pTempTopic->pLastSubscriber->pNextSubscriber = (void *) subscriber;
              }
              pTempTopic->pLastSubscriber = subscriber;
              totalLocalSubscribers++;
              ret_code = RET_CODE_SUCCESS;
            }
            else
            {
              ret_code = RET_CODE_ERROR_NO_MEM;
            }
            break;
          }

          pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
        }
        ret_code_t mux_ret = osa_Munlock(localMuxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        ret_code = RET_CODE_ERROR_BUSY;
      }
      /* If topic not found in local list */
      if(ret_code == RET_CODE_ERROR_NOT_FOUND)
      {
        /* Find in shared topics - take the mutex */
        VALIDATE_CRITIC((sharedMuxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub lib not initialized!");
        if (osa_Mlock(sharedMuxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
        {
          pubsub_topic_t * pTempTopic = pSharedTopicFirstNode;
          while (pTempTopic != (pubsub_topic_t *) NULL)
          {
            if (pTempTopic->topicID == topicID)
            {
              /* Create subscriber */
              pubsub_subscriber_t * subscriber = (pubsub_subscriber_t *) NULL;
              MALLOC(pubsub_subscriber_t, subscriber, sizeof(pubsub_subscriber_t));
              if (subscriber != (pubsub_subscriber_t *) NULL)
              {
                /* Fill subscriber fields and link */
                if (pSubscriberHandle)
                {
                  *pSubscriberHandle = (pubsub_subscriberHandle_t) subscriber;
                }
                subscriber->clientHandle = (void **)pSubscriberHandle;
                subscriber->subscriberMsgFlags = flags;
                subscriber->pParentTopicHandle = (void *) pTempTopic;
                subscriber->subscriberCallback = pCallBack;
                subscriber->pNextSubscriber = (void *) NULL;

                /* Link the clients */
                subscriber->pPrevSubscriber = (void *) pTempTopic->pLastSubscriber;
                if (pTempTopic->pFirstSubscriber == (pubsub_subscriber_t *) NULL)
                {
                  pTempTopic->pFirstSubscriber = subscriber;
                }

                if (pTempTopic->pLastSubscriber != (pubsub_subscriber_t *) NULL)
                {
                  pTempTopic->pLastSubscriber->pNextSubscriber = (void *) subscriber;
                }
                pTempTopic->pLastSubscriber = subscriber;
                totalSharedSubscribers++;
                ret_code = RET_CODE_SUCCESS;
              }
              else
              {
                ret_code = RET_CODE_ERROR_NO_MEM;
              }
              break;
            }
            pTempTopic = (pubsub_topic_t *) pTempTopic->pNextTopicHandle;
          }
          ret_code_t mux_ret = osa_Munlock(sharedMuxHandle);
          VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
        }
        else
        {
          ret_code = RET_CODE_ERROR_BUSY;
        }
      }
    }
  }
  return ret_code;
}

/**
 * @brief	Quicker version of pubsub_serverPublish with using of client handle.
 *
 * @param[in]	clientHandle - the static client handle variable.
 * @param[in]	pData - pointer to data.
 * @param[in]	size - the size of data.
 * @param[in]	flags - the flags for messaging filtering.
 * @param[in]	 - the timeout to enqueue the data.
 *
 *
 * @retval	RET_CODE_SUCCESS				If message enqueued.
 * @retval	RET_CODE_ERROR_NULL				If NULL clientHandle or pData pointer or size == 0 has been passed to function.
 * @retval	RET_CODE_ERROR_NO_MEM			If no available memory in HEAP.
 */
ret_code_t pubsub_clientPost(pubsub_subscriberHandle_t clientHandle, const void * pData, size_t size, pubsub_msgFlags_t flags, uint32_t timeout)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  /* Checks the arguments */
  if ((clientHandle != (pubsub_subscriberHandle_t) NULL)
      && (pData != (const void *) NULL)
        && (size != 0)
          )
  {
    pubsub_subscriber_t * subscriber = (pubsub_subscriber_t *) clientHandle;
    pubsub_topic_t * pTopic = (pubsub_topic_t *) subscriber->pParentTopicHandle;
    if (pTopic != (pubsub_topic_t *) NULL)
    {
      osa_QHandle_t destinationQueueHandle = sharedQueueHandle;
      if ((pTopic->topicProperties & PUBSUB_TPROP_SHARED_TOPIC) == 0u)
      {
        destinationQueueHandle = localQueueHandle;
      }
      pubsub_packet_t * pPacket = (pubsub_packet_t *) NULL;
      MALLOC(pubsub_packet_t, pPacket, (sizeof(pubsub_packet_t) + size));
      if (pPacket != (pubsub_packet_t *) NULL)
      {
        /* Populate the packet */
        pPacket->topicID = pTopic->topicID;
        pPacket->msgFlags = flags;
        pPacket->length = size;
        memcpy((void *) pPacket->message, pData, size);

        ret_code = osa_Qsend(destinationQueueHandle, pPacket, (sizeof(pubsub_packet_t) + size), EPB_PACKET_INTERNAL_PRIO, timeout);
        FREE(pubsub_packet_t, pPacket);
      }
      else
      {
        ret_code = RET_CODE_ERROR_NO_MEM;
      }
    }
  }
  return ret_code;
}

/**
 * @brief	Quicker version of pubsub_serverPublish with using of client handle.
 *
 * @param[out]	pSubscriberHandle - the pointer to static client handle variable.
 *
 *
 * @retval	RET_CODE_SUCCESS				If subscriber deleted.
 * @retval	RET_CODE_ERROR_NULL				If NULL pSubscriberHandle has been passed to function.
 * @retval	RET_CODE_ERROR_BUSY				If mutex is busy.
 */
ret_code_t pubsub_deleteSubscriber(pubsub_subscriberHandle_t * pSubscriberHandle)
{
  ret_code_t ret_code = RET_CODE_ERROR_NULL;
  /* Checks the arguments */
  if (pSubscriberHandle != (pubsub_subscriberHandle_t *) NULL)
  {
    if (*pSubscriberHandle != (pubsub_subscriberHandle_t) NULL)
    {
      pubsub_subscriber_t * subscriber = (pubsub_subscriber_t *) (*pSubscriberHandle);
      pubsub_topic_t * pTopic = (pubsub_topic_t *) subscriber->pParentTopicHandle;

      /* Topic local - take the mutex */
      osa_MHandle_t muxHandle = localMuxHandle;
      if((pTopic->topicProperties & PUBSUB_TPROP_SHARED_TOPIC) != 0u)
      {
        muxHandle = sharedMuxHandle;
      }
      VALIDATE_CRITIC((muxHandle == (osa_MHandle_t) NULL), RET_CODE_ERROR_NULL, "It seems that pubsub library not initialized!");

      if (osa_Mlock(muxHandle, EPB_DEFAULT_TIMEOUT) == RET_CODE_SUCCESS)
      {
        /* The client may change their state when we wait for mutex */
        if (*pSubscriberHandle != (pubsub_subscriberHandle_t) NULL)
        {
          pubsub_subscriber_t * prevSubscriber = (pubsub_subscriber_t *) subscriber->pPrevSubscriber;
          pubsub_subscriber_t * nextSubscriber = (pubsub_subscriber_t *) subscriber->pNextSubscriber;

          /* If in list only one subscriber */
          if ((prevSubscriber == (pubsub_subscriber_t *) NULL)
              && (nextSubscriber == (pubsub_subscriber_t *) NULL)
                )
          {
            pTopic->pFirstSubscriber = (pubsub_subscriber_t *) NULL;
            pTopic->pLastSubscriber = (pubsub_subscriber_t *) NULL;
          }
          else
          {
            /* If we delete first subscriber in list */
            if (prevSubscriber == (pubsub_subscriber_t *) NULL)
            {
              pTopic->pFirstSubscriber = (pubsub_subscriber_t *) nextSubscriber;
            }
            else
            {
              /* If we delete last subscriber in list */
              if (nextSubscriber == (pubsub_subscriber_t *) NULL)
              {
                pTopic->pLastSubscriber = prevSubscriber;
              }
              else
              {
                prevSubscriber->pNextSubscriber = (void *) nextSubscriber;
                nextSubscriber->pPrevSubscriber = (void *) prevSubscriber;
              }
            }
          }
          if (muxHandle == localMuxHandle)
          {
            totalLocalSubscribers--;
          }
          else
          {
            totalSharedSubscribers--;
          }
          FREE(pubsub_subscriberHandle_t, *pSubscriberHandle);
          *pSubscriberHandle = (pubsub_subscriberHandle_t) NULL;
          ret_code = RET_CODE_SUCCESS;
        }
        ret_code_t mux_ret = osa_Munlock(muxHandle);
        VALIDATE_CRITIC((mux_ret != RET_CODE_SUCCESS), mux_ret, "Failed to unlock the mutex!");
      }
      else
      {
        ret_code = RET_CODE_ERROR_BUSY;
      }
    }
  }
  return ret_code;
}

/*--------------------------------------- TESTING MODULE ---------------------------------------------*/

#ifdef DEBUG
/* Unit tests implementation */

/* Topic handle */
static pubsub_topicID_t topicA = NULL_TOPIC;

/* Client handle */
static pubsub_subscriberHandle_t myClient = (pubsub_subscriberHandle_t) NULL;

/* Callback for subscribers */
static void pubsub_callbackFun(const pubsub_subscriberHandle_t * clientHandle, const void * pData, const size_t size, const pubsub_msgFlags_t flags)
{
  (void) size;
  if(flags & 0x5A)
  {
    UTIL_DBG_INFO("Local handle: myClient");
    UTIL_DBG_INFO("Client with handle: %p received: %s ",  clientHandle, (char *)pData);
  }
  else
  {
    UTIL_DBG_INFO("Client with handle: %p received: %s ", clientHandle, (char *)pData);
  }
  osa_TsleepMS(1u);
}

/* Callback for subscribers */
static void pubsub_sharedcallbackFun(const pubsub_subscriberHandle_t * clientHandle, const void * pData, const size_t size, const pubsub_msgFlags_t flags)
{
  (void) size;
  (void) flags;
  UTIL_DBG_INFO("I'm subscriber to shared topic!");
  UTIL_DBG_INFO("Client with handle: %p received: %s ",  clientHandle, (char *)pData);
  osa_TsleepMS(1u);
}

/**
* @brief	Function to test the functionality of this library.
*
* @param[in]	times - time in seconds during testing will be active.
*
* @retval	None.
*/
void pubsub_unittest(uint32_t times)
{
  UTIL_DBG_INFO("\n");
  UTIL_DBG_INFO("Test of Embedded PUB/Sub library begin for %d seconds!", times);
  /* For dynamic memory checking: if memAddr at start eq to memAddr at end than we not lose any memory */
  void * memPtr1 = (void *) NULL;
  MALLOC(void, memPtr1, sizeof(void *));
  FREE(void, memPtr1);
  VALIDATE_CRITIC((memPtr1 == (void *) NULL), 2, "Memory allocation invalid!");

  ret_code_t ret_code = pubsub_initialize();
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to initialize!");

  topicA = pubsub_serverCreateTopic("MyperfectTopicName", PUBSUB_TPROP_NONE);
  pubsub_serverCreateTopic("Shared Topic", PUBSUB_TPROP_SHARED_TOPIC);
  VALIDATE_CRITIC((topicA == NULL_TOPIC), topicA, "Failed to create topic!");

  for (uint32_t i = 0; i < times; i++)
  {
    ret_code = pubsub_createSubscriber(NULL, NULL, topicA, pubsub_callbackFun, 0xA5);
    VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to create subscriber by topic ID!");

    ret_code = pubsub_createSubscriber(NULL, "MyperfectTopicName", NULL_TOPIC, pubsub_callbackFun, 0xA5);
    VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to create subscriber by topic name!");
  }

  ret_code = pubsub_createSubscriber(&myClient, "MyperfectTopicName", NULL_TOPIC, pubsub_callbackFun, 0x5A);
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to create subscriber by topic name!");

  ret_code = pubsub_createSubscriber(NULL, "Shared Topic", NULL_TOPIC, pubsub_sharedcallbackFun, 0xFF);
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to create subscriber by topic name!");

  UTIL_DBG_INFO("There is: %u subscribers, and %u topics!", pubsub_getSubscribersCount(), pubsub_getTopicsCount());

  ret_code = pubsub_serverPublish("MyperfectTopicName", NULL_TOPIC, "Hello via NAME!", sizeof("Hello via NAME!"), 0xA5, PUBSUB_DEFAULT_TIMEOUT);
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to publish via: pubsub_serverPublish!");

  ret_code = pubsub_serverPublish("Shared Topic", NULL_TOPIC, "Hello via NAME!", sizeof("Hello to shared topic!"), 0xA5, PUBSUB_DEFAULT_TIMEOUT);
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to publish via: pubsub_serverPublish!");

  ret_code = pubsub_serverPublish(NULL, topicA, "Hello via ID!", sizeof("Hello via ID!"), 0xA5, PUBSUB_DEFAULT_TIMEOUT);
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to publish via: pubsub_serverPublish!");

  ret_code = pubsub_clientPost(myClient, "Hello via to my client CID!", sizeof("Hello via to my client CID!"), 0x5A, PUBSUB_DEFAULT_TIMEOUT);
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to publish via: pubsub_clientPost!");

  osa_TsleepMS(times * 1000u);

  pubsub_serverDeleteTopic("MyperfectTopicName", NULL_TOPIC);
  ret_code = pubsub_deinitialize();
  VALIDATE_CRITIC((ret_code != RET_CODE_SUCCESS), ret_code, "Failed to destruct the lib!");

  void * memPtr2 = (void *)NULL;
  MALLOC(void, memPtr2, sizeof(void *));
  FREE(void ,memPtr2);

  if (memPtr1 != memPtr2)
  {
    UTIL_DBG_WARN("There was memory leakage!");
    MEM_VALIDATE();
  }
  UTIL_DBG_INFO("Test of Embedded PUB/SUB library finished!");
}

#endif

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
