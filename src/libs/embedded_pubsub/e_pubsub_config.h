//!******************************************************************************
//!   @file    e_pubsub_config.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    06/04/2018
//!   @brief   This is C header with symbols Embedded Publish Subscribe Library.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  06/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#ifndef _E_PUBSUB_CONFIG_H_
#define _E_PUBSUB_CONFIG_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "generic_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/

#ifdef NRF52840_XXAA
/* For NRF only */
/* Queues elements:
 * LOCAL - used for store elements only for local handling.
 * SHARED - used for storing elements that will be shared to others topics over external channel.
 */
#define EPB_LOCAL_QELEMENTS  		16u
#define EPB_LOCAL_QMAXELEMENTSIZE       255   /*!< Local Q element size in bytes. */

#define EPB_SHARED_QELEMENTS  		16u
#define EPB_SHARED_QMAXELEMENTSIZE      255   /*!< Shared Q element size in bytes. */

/** Default timeout to create/delete subscribers/topics */
#define EPB_DEFAULT_TIMEOUT 100u

/* Threads priorities */
#define SHARED_TOPICS_THR_PRIO	1u
#define LOCAL_TOPICS_THR_PRIO	1u

/** Local manager stack depth */
#define EPB_LOCAL_MANAGER_THREAD_STACK_DEPTH   400u

/** Shared manager stack depth */
#define EPB_SHARED_MANAGER_THREAD_STACK_DEPTH   400u

#else

/* Queues elements:
 * LOCAL - used for store elements only for local handling.
 * SHARED - used for storing elements that will be shared to others topics over external channel.
 */
#define EPB_LOCAL_QELEMENTS  		255u
#define EPB_LOCAL_QMAXELEMENTSIZE       32     /*!< Local Q element size in bytes. */

#define EPB_SHARED_QELEMENTS  		255u
#define EPB_SHARED_QMAXELEMENTSIZE      32      /*!< Shared Q element size in bytes. */

/** Default timeout to create/delete subscribers/topics */
#define EPB_DEFAULT_TIMEOUT 50u

/* Threads priorities */
#define SHARED_TOPICS_THR_PRIO	1u
#define LOCAL_TOPICS_THR_PRIO	1u

/** Local manager stack depth */
#define EPB_LOCAL_MANAGER_THREAD_STACK_DEPTH   0u  // Default stack depth

/** Shared manager stack depth */
#define EPB_SHARED_MANAGER_THREAD_STACK_DEPTH   0u  // Default stack depth


#endif	/* NRF52840_XXAA */

/* Defined pseudo priorities uses for recognition of packet source. */
/** The pseudo prio of packet that passed from function pubsub_handleData.
 *  @note To use this feature packet @ref USE_FREERTOS_Q_PRIO must be as 1.
 */
#define EPB_PACKET_EXTERNAL_PRIO	0u

/** The pseudoprio of packet that publish via lib API.
 *  @note To use this feature packet @ref USE_FREERTOS_Q_PRIO must be as 1.
 */
#define EPB_PACKET_INTERNAL_PRIO	1u

/** The maximum timeout for task management and queues handle */
#define PUBSUB_DEFAULT_TIMEOUT        250u

/* Exported typedef ----------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
}
#endif

#endif  /* _E_PUBSUB_CONFIG_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
