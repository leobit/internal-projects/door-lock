//!******************************************************************************
//!   @file    e_pubsub.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    05/03/2018
//!   @brief   This is C header file for Embedded Publish Subscribe Library.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  05/03/2018 | Dmytro Kryvyi    | Initial draft
//!  13/03/2018 | Dmytro Kryvyi    | Beta
//!  16/03/2018 | Dmytro Kryvyi    | Code refactoring
//!  04/04/2018 | Dmytro Kryvyi    | Doxygen style and code updated
//!
//!*****************************************************************************

#ifndef _E_PUBSUB_H_
#define _E_PUBSUB_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "generic_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/

/** The type for topic properties flags. */
typedef enum
{
  /** No property flag. */
  PUBSUB_TPROP_NONE = 0u,
  /** Use this flag if you want to create global topic with shared messages. */
  PUBSUB_TPROP_SHARED_TOPIC = (1u << (0u)),

} pubsub_topicProperties_t;

/** The type for publish/subscribe message flags. */
typedef uint8_t pubsub_msgFlags_t;

/** The type for handling the topic instance. */
typedef uint8_t pubsub_topicID_t;

/** The type for handling the suscriber instance.
  * @note Every handle must be located at static addresses */
typedef void * pubsub_subscriberHandle_t;

/** E_pubsub packet structure typedef */
typedef __PACKED_STRUCT pubsub_packet_s
{
	pubsub_topicID_t topicID;                ///< Topic ID.
	pubsub_msgFlags_t msgFlags;              ///< Message flags.
	uint8_t length;                          ///< Message length.
	uint8_t message[];                       ///< Message content.
} pubsub_packet_t;

/** Typedef of handler for handling the messages */
typedef void (*pubsub_callback_t)(const pubsub_subscriberHandle_t * clientHandle,
                                  const void * pData, const size_t size,
                                  const pubsub_msgFlags_t flags);

/** Typedef of callback for transmit the packet to topic on other machine */
typedef void (*pubsub_transmitterCallback_t)(const pubsub_packet_t * pPacket, const size_t packetSize);

/* Exported define -----------------------------------------------------------*/

/** Illegal topic ID */
#define NULL_TOPIC  ((pubsub_topicID_t) 0u)

/** The topic flags for all messages */
#define PUBSUB_ALL_MSG_FLAGS ((pubsub_msgFlags_t) 0xFFu)

/** Illegal subscriber handle */
#define NULL_SUBSCRIBER_HANDLE ((pubsub_subscriberHandle_t) NULL)

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/* Pubsub basic functions */
ret_code_t pubsub_initialize(void);
ret_code_t pubsub_deinitialize(void);

size_t pubsub_getSubscribersCount(void);
size_t pubsub_getTopicsCount(void);

/* Server interface */
pubsub_topicID_t pubsub_serverCreateTopic(const char * pTopicName, pubsub_topicProperties_t flags);
pubsub_topicID_t pubsub_serverCreateTopicByID(pubsub_topicID_t topicID, pubsub_topicProperties_t flags);
ret_code_t pubsub_serverDeleteTopic(const char * pTopicName, pubsub_topicID_t topicID);

/* Sybscriber interface */
ret_code_t pubsub_createSubscriber(pubsub_subscriberHandle_t * pSubscriberHandle,
                                   const char * pTopicName,  pubsub_topicID_t topicID,
                                   pubsub_callback_t pCallBack, pubsub_msgFlags_t flags);

ret_code_t pubsub_deleteSubscriber(pubsub_subscriberHandle_t * pSubscriberHandle);

/* Publish & post */
ret_code_t pubsub_serverPublish(const char * pTopicName, pubsub_topicID_t topicID,
                                const void * pData, size_t size, pubsub_msgFlags_t flags, uint32_t timeout);

ret_code_t pubsub_clientPost(pubsub_subscriberHandle_t clientHandle, const void * pData,
                             size_t size, pubsub_msgFlags_t flags, uint32_t timeout);

/* Transport interface & direct access*/
ret_code_t pubsub_registerSharedTransport(pubsub_transmitterCallback_t pTransmitterFunc);
ret_code_t pubsub_registerLocalTransport(pubsub_transmitterCallback_t pTransmitterFunc);

ret_code_t pubsub_handleData(const void * pData, const size_t size, bool queue_to_shared);

#ifdef DEBUG
/* Unit tests */
void pubsub_unittest(uint32_t times);
#endif

#ifdef __cplusplus
}
#endif

#endif  /* _E_PUBSUB_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
