//!******************************************************************************
//!   @file    generic_errors.h
//!   @author  Dmytro Kryvyi
//!   @version 0.1.0
//!   @date    06/03/2018
//!   @brief   This is C header file and error codes
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  06/03/2018 | Dmytro Kryvyi    | Initial draft
//!  16/03/2018 | Dmytro Kryvyi    | Code refactoring
//!  04/04/2018 | Dmytro Kryvyi    | Doxygen style and code updated
//!
//!*****************************************************************************

#ifndef _GENERIC_ERRORS_H_
#define _GENERIC_ERRORS_H_

#ifndef NRF5_SOC
/* For other systems */

/* Includes ------------------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define RET_CODE_ERROR_BASE_NUM      (0u)       ///< RET_CODE error base

/* Exported typedef ----------------------------------------------------------*/

/* Return codes for */
typedef enum returnInfo
{
  RET_CODE_SUCCESS = RET_CODE_ERROR_BASE_NUM,     ///< Successful command
  RET_CODE_ERROR_INTERNAL,                        ///< Internal Error
  RET_CODE_ERROR_NO_MEM,                          ///< No Memory for operation
  RET_CODE_ERROR_NOT_FOUND,                       ///< Not found
  RET_CODE_ERROR_NOT_SUPPORTED,                   ///< Not supported
  RET_CODE_ERROR_INVALID_PARAM,                   ///< Invalid Parameter
  RET_CODE_ERROR_INVALID_STATE,                   ///< Invalid state, operation disallowed in this state
  RET_CODE_ERROR_INVALID_LENGTH,                  ///< Invalid Length
  RET_CODE_ERROR_INVALID_FLAGS,                   ///< Invalid Flags
  RET_CODE_ERROR_INVALID_DATA,                    ///< Invalid Data
  RET_CODE_ERROR_DATA_SIZE,                       ///< Invalid Data size
  RET_CODE_ERROR_TIMEOUT,                         ///< Operation timed out
  RET_CODE_ERROR_NULL,                            ///< Null Pointer
  RET_CODE_ERROR_FORBIDDEN,                       ///< Forbidden Operation
  RET_CODE_ERROR_INVALID_ADDR,                    ///< Bad Memory Address
  RET_CODE_ERROR_BUSY,                            ///< Busy
  RET_CODE_ERROR_RESOURCES                        ///< Not enough resources for operation
} ret_code_t;

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/* Return code if this true */
#define IF_TRUE_RETC(expr, errcode)  if (expr) return errcode

/* If debug enabled */
#ifdef DEBUG

#include <stdio.h>
#include <stdlib.h>

/** Checking for true, if expression true - exit from program with exitcode */
#define VALIDATE_CRITIC(expr, errcode, text) \
    do { \
        if (expr) { \
          perror(#expr); \
            fprintf(stderr, "Function '%s' reported critical error at '%d' line code: %d - "text, __func__, __LINE__, errcode); \
            puts("\n"); \
            exit(-1); \
        } \
    } while (0)

/** Checking for true, if expression !true - print DBG message. */
#define ASSERT_TRUE(expr, ...) \
    do { \
        if (!expr) { \
        	printf(MODULE_NAME " - Validate failed - %s at line %d: >> ", __func__, __LINE__);\
            printf(__VA_ARGS__);\
            puts("");\
        } \
    } while (0)

/** Debug info */
#define UTIL_DBG_INFO(...)  printf(MODULE_NAME " - DBG Info - %s at line %d: >> ", __func__, __LINE__); printf(__VA_ARGS__); puts("")

/** Debug warning */
#define UTIL_DBG_WARN(...)  printf(MODULE_NAME " - DBG warn - %s at line %d: >> ", __func__, __LINE__); printf(__VA_ARGS__); puts("")

/** Flush debug buffer */
#define UTIL_DBG_FLUSH()   puts("")

#else

/** Checking for true */
#define VALIDATE_CRITIC(expr, errcode, text) while(expr)

/** Debug info */
#define UTIL_DBG_INFO(...)

/** Debug warning */
#define UTIL_DBG_WARN(...)

/** Flush debug buffer */
#define UTIL_DBG_FLUSH()

/** Checking for true, if expression !true - print DBG message. */
#define ASSERT_TRUE(expr, ...)

#endif // DEBUG

/* Exported functions ------------------------------------------------------- */

#else
/* For NRF5 based systems */

/* Includes ------------------------------------------------------------------*/
#include "sdk_errors.h"
#include "app_error.h"
#include "nrf_error.h"

/* Exported define -----------------------------------------------------------*/
/* Exported typedef ----------------------------------------------------------*/

/* Return codes for */
enum returnInfo
{
  RET_CODE_SUCCESS = NRF_SUCCESS,                           ///< Successful command
  RET_CODE_ERROR_INTERNAL = NRF_ERROR_INTERNAL,             ///< Internal Error
  RET_CODE_ERROR_NO_MEM = NRF_ERROR_NO_MEM,                 ///< No Memory for operation
  RET_CODE_ERROR_NOT_FOUND = NRF_ERROR_NOT_FOUND,           ///< Not found
  RET_CODE_ERROR_NOT_SUPPORTED = NRF_ERROR_NOT_SUPPORTED,   ///< Not supported
  RET_CODE_ERROR_INVALID_PARAM = NRF_ERROR_INVALID_PARAM,   ///< Invalid Parameter
  RET_CODE_ERROR_INVALID_STATE = NRF_ERROR_INVALID_STATE,   ///< Invalid state, operation disallowed in this state
  RET_CODE_ERROR_INVALID_LENGTH = NRF_ERROR_INVALID_LENGTH, ///< Invalid Length
  RET_CODE_ERROR_INVALID_FLAGS = NRF_ERROR_INVALID_FLAGS,   ///< Invalid Flags
  RET_CODE_ERROR_INVALID_DATA = NRF_ERROR_INVALID_DATA,     ///< Invalid Data
  RET_CODE_ERROR_DATA_SIZE = NRF_ERROR_DATA_SIZE,           ///< Invalid Data size
  RET_CODE_ERROR_TIMEOUT = NRF_ERROR_TIMEOUT,               ///< Operation timed out
  RET_CODE_ERROR_NULL = NRF_ERROR_NULL,                     ///< Null Pointer
  RET_CODE_ERROR_FORBIDDEN = NRF_ERROR_FORBIDDEN,           ///< Forbidden Operation
  RET_CODE_ERROR_INVALID_ADDR = NRF_ERROR_INVALID_ADDR,     ///< Bad Memory Address
  RET_CODE_ERROR_BUSY = NRF_ERROR_BUSY,                     ///< Busy
  RET_CODE_ERROR_RESOURCES = NRF_ERROR_RESOURCES            ///< Not enough resources for operation
};

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

/** Return code if this true */
#define IF_TRUE_RETC(expr, errcode)  if (expr) return errcode

/** If debug enabled */
#ifdef DEBUG
#include "nrf_log.h"
#include "nrf_log_ctrl.h"

/** Checking for true, if expression true - exit from program with exitcode */
#define VALIDATE_CRITIC(expr, errcode, text) \
    do { \
        if (expr) { \
          app_error_handler(errcode, __LINE__, (uint8_t*) text);  \
        } \
    } while (0)

/** Debug info */
#define UTIL_DBG_INFO(...)  NRF_LOG_INFO(MODULE_NAME ": " __VA_ARGS__)

/** Debug warning */
#define UTIL_DBG_WARN(...)  NRF_LOG_WARNING(MODULE_NAME ":" __VA_ARGS__)

/** Flush debug buffer */
#define UTIL_DBG_FLUSH()   NRF_LOG_FLUSH()

/** Checking for true, if expression !true - print DBG message. */
#define ASSERT_TRUE(expr, ...) \
    do { \
        if (expr) { \
        	 NRF_LOG_INFO(MODULE_NAME " - ASSERT: " __VA_ARGS__);\
        } \
    } while (0)

#else

/** Checking for true */
#define VALIDATE_CRITIC(expr, errcode, text)  while(expr)

/** Debug info */
#define UTIL_DBG_INFO(...)

/** Debug warning */
#define UTIL_DBG_WARN(...)

/** Flush debug buffer */
#define UTIL_DBG_FLUSH()

/** Checking for true, if expression !true - print DBG message. */
#define ASSERT_TRUE(expr, ...)

#endif // DEBUG

/* Exported functions ------------------------------------------------------- */

#endif // NRF5_SOC

/** Null pointer checking */
#define VERIFY_NOT_NULL(type, ptr) VALIDATE_CRITIC((ptr == (type *) NULL), RET_CODE_ERROR_NULL, "NULL pointer exception!")

#endif // _GENERIC_ERRORS_H_

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
