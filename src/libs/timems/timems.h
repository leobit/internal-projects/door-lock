//!*****************************************************************************
//!   @file    timems.h
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    04/04/2018
//!   @brief   This is C header file that defines API and symbols for timems lib.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  04/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

#ifndef BACKEND_CORE_TIMEMS_H_
#define BACKEND_CORE_TIMEMS_H_

/* Includes ------------------------------------------------------------------*/
#include <time.h>
#include <stdint.h>
#include <stdbool.h>
#include "generic_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/

/* Backward compability. */
#define get_elapsed_time_ms() timems_get_systemup_time()
#define get_system_time_ms() timems_get_systemup_time()

/* Exported functions ------------------------------------------------------- */

/* Basic functions */
ret_code_t timems_initialize(void);
ret_code_t timems_deinitialize(void);

/* Get time */
uint32_t timems_get_tflfc_time();
uint32_t timems_get_systemup_time();

/* STD time */
time_t time(time_t * timeptr);
void time_set_std_time(time_t time);

#ifdef __cplusplus
}
#endif

#endif /* BACKEND_CORE_TIMEMS_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
