//!*****************************************************************************
//!   @file    timems.c
//!   @author  Dmytro Kryvyi
//!   @version 1.0.0
//!   @date    04/04/2018
//!   @brief   This is C source file that implements API for timems lib.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  04/04/2018 | Dmytro Kryvyi    | Initial draft
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/
#include "timems.h"
#include "nrf_drv_rtc.h"
#include "app_util_platform.h"

/* Private define section 0 --------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private define section 1 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/** Declaring an instance of nrf_drv_rtc for RTC2. */
const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(2);

/** Sustem up time */
static volatile uint32_t system_up_time = 0u;

/** STD lib time */
static volatile time_t std_time = 0u;

/** The time capture at point when STD time updated */
static volatile uint32_t last_std_updated_at_system_up_time = 0u;

/* Private functions prototypes ----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
 * @brief: Function for handling the RTC2 interrupts.
 *
 * Triggered on TICK and COMPARE0 match.
 */
static void rtc_handler(nrf_drv_rtc_int_type_t int_type)
{
  if (int_type == NRF_DRV_RTC_INT_OVERFLOW)
  {
    uint32_t sys_up_time_before = system_up_time;
    /* Add to system up time in ms calculated time in ms that equeal to RTC overflow */
    system_up_time += (((1u << 24u) / 1024u) * 1000u);

    /* Calculete STD time */
    uint32_t sys_time_now = timems_get_systemup_time();
    uint32_t std_time_increment;
    /* Check for overflow */
    if (sys_up_time_before < sys_time_now)
    {
      std_time_increment = ((sys_time_now - last_std_updated_at_system_up_time) / 1000u);
    }
    else
    {
      std_time_increment = (((UINT32_MAX - last_std_updated_at_system_up_time) + sys_time_now) / 1000u);
    }
    last_std_updated_at_system_up_time = sys_time_now;
    std_time += std_time_increment;
  }
}
/* Exported functions ------------------------------------------------------- */

/**
 * @brief	Function for initializing the timems lib.
 *
 * @param  None.
 *
 * @retval	NRF_SUCCESS	If lib initialized successfully else @ref ret_code_t.
 */
ret_code_t timems_initialize(void)
{
  ret_code_t ret_code;

  /* Initialize RTC instance */
  nrf_drv_rtc_config_t config =
  {
    .prescaler          = RTC_FREQ_TO_PRESCALER(1024u),
    .interrupt_priority = APP_IRQ_PRIORITY_LOW,
    .reliable           = RTC_DEFAULT_CONFIG_RELIABLE,
    .tick_latency       = RTC_US_TO_TICKS(NRF_MAXIMUM_LATENCY_US, 1024u),
  };

  ret_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
  if (ret_code == NRF_SUCCESS)
  {
    /* Reset the counters */
    system_up_time = 0u;
    last_std_updated_at_system_up_time = 0u;

    /* Set up time. */
    struct tm orig;
    orig.tm_hour = 15u;
    orig.tm_min = 00u;
    orig.tm_sec = 0u;
    orig.tm_year = 118u;
    orig.tm_mon = 3u;
    orig.tm_mday = 5u;

    std_time = mktime(&orig);

    /* Disable tick event */
    nrf_drv_rtc_tick_disable(&rtc);

    /* Enable IRQ for RTC counter overflow */
    nrf_drv_rtc_overflow_enable(&rtc, true);

    /* Power on RTC instance */
    nrf_drv_rtc_enable(&rtc);
  }
  return ret_code;
}

/**
 * @brief	Function for de-initializing the timems lib.
 *
 * @param  None.
 *
 * @retval	NRF_SUCCESS	If lib initialized successfully else @ref ret_code_t.
 */
ret_code_t timems_deinitialize(void)
{
  ret_code_t ret_code = NRF_SUCCESS;

  /* Disable and de-initialize RTC peripheral */
  nrf_drv_rtc_disable(&rtc);
  nrf_drv_rtc_uninit(&rtc);

  return ret_code;
}

/**
 * @brief	Get the time in MS from system start.
 *
 * @param  None.
 *
 * @retval	Milliseconds from system start.
 */
uint32_t timems_get_systemup_time()
{
  /* Capture the times */
  uint64_t sys_uptime = (uint64_t) system_up_time;
  uint64_t rtc_counter = nrf_drv_rtc_counter_get(&rtc);

  return (uint32_t)(sys_uptime + ((rtc_counter * 1000u) / 1024u));
}

/**
 * @brief	Get the time in MS from last call this function.
 *
 * @param  None.
 *
 * @retval	Time in milliseconds from last call of this function.
 */
uint32_t timems_get_tflfc_time()
{
  /** Time from last function call */
  static volatile uint32_t tflfc_time = 0u;
  uint32_t ret_time = 0u;
  uint32_t sys_time = timems_get_systemup_time();

  /* If overflow */
  if (tflfc_time > sys_time)
  {
    ret_time = (UINT32_MAX - tflfc_time) + sys_time;
  }
  else
  {
    ret_time = sys_time - tflfc_time;
  }
  tflfc_time = sys_time;

  return ret_time;
}

/**
 * @brief	Get the STD time in Sec from start of 70x.
 *
 * @note This function used by STD time to get time.
 * @param[out]  timeptr - will be overvritten with time.
 *
 * @retval	STD time in Sec from start of 70x.
 */
time_t time(time_t * timeptr)
{
  /* Calculete the time due to specific RTC working */
  time_t math_time = std_time;
  uint32_t sys_time_now = timems_get_systemup_time();
  math_time += ((sys_time_now - last_std_updated_at_system_up_time) / 1000u);

  if (timeptr != NULL)
  {
    *timeptr = math_time;
  }

  return math_time;
}

/**
 * @brief	Set the STD time in Sec from start of 70x.
 *
 * @param[in]  time - actual time.
 *
 * @retval	None.
 */
void time_set_std_time(time_t time)
{
  last_std_updated_at_system_up_time = timems_get_systemup_time();

  std_time = time;
}

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
