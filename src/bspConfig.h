//!*****************************************************************************
//!   @file    bsp_config.h
//!   @author  Dmytro Kryvyi
//!   @version 0.9.0
//!   @date    06/03/2018
//!   @brief   This C header file contain defined symbols for Board Support Package.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  21/03/2018 | Dmytro Kryvyi    | Initial draft
//!
//!******************************************************************************

#ifndef _LEO2_BSP_H_
#define _LEO2_BSP_H_

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "nrf.h"
#include "nrf_peripherals.h"
#include "nrf_assert.h"
#include "compiler_abstraction.h"
#include "generic_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported define -----------------------------------------------------------*/
/* TODO: Change pins due to final board. */

/* Buzzer pin number */
#define BSP_PIN_BUZZER  22u    /*!< Buzzer peripheral pin number */

/* GPS UARTE instance index and pins */
#define BSP_GPS_UARTE_INDEX    0    /*!< GPS UARTE peripheral instance ID */
#define BSP_GPS_TIM_INDEX      1    /*!< GPS TIMER peripheral instance ID */
#define BSP_PIN_GPS_TXD        6u   /*!< GPS peripheral UARTE TXD pin number */
#define BSP_PIN_GPS_RXD        8u   /*!< GPS peripheral UARTE RXD pin number */

/* Exported typedef ----------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
}
#endif

#endif  /* _LEO2_BSP_H_ */

/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/
