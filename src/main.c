//!*****************************************************************************
//!   @file    main.c
//!   @author  Stages Dash team
//!   @version 0.1.0
//!   @date    11/04/2018
//!   @brief   This is main C source file for Stages Dash 2.
//!
//!   @copyright  Copyright (C) 2018
//!
//!*****************************************************************************
//! __Revisions:__
//!  Date       | Author           | Comments
//!  ---------- | ---------------- | --------
//!  11/04/2018 | Stages Dash team | Initial draft
//!
//!*****************************************************************************

/* Includes ------------------------------------------------------------------*/

/* STD libs */
#include <stdint.h>
#include <string.h>


/* Nordic standart includes */
#include "nrf.h"
#include "nrf_sdh.h"
#include "nrf_sdm.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_freertos.h"
#include "nordic_common.h"

/* Nordic drivers */
#include "nrf_drv_clock.h"

/* Nordic APP lib */
#include "app_util_platform.h"
#include "app_timer.h"
#include "app_error.h"

/* FreeRTOS headers */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

/* Custom libs */
#include "generic_errors.h"
#include "osa_layer.h"
#include "timems.h"
#include "e_pubsub.h"

/* Log */
#if (NRF_LOG_ENABLED != 0)
#include "dash_log.h"
#endif

/* Debug */
#ifdef DEBUG
#include "hardfault.h"
#include "nrf_gpio.h"
#include "boards.h"
#include "uart_drv.h"
#endif

#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"

/* Private define section 0 --------------------------------------------------*/


/* The name of this module for debugger */
#define MODULE_NAME "MAIN"

#if (ENABLE_MAIN_LOG != 1u)
#undef UTIL_DBG_INFO
#define UTIL_DBG_INFO(...)
#undef UTIL_DBG_WARN
#define UTIL_DBG_WARN(...)
#undef UTIL_DBG_FLUSH
#define UTIL_DBG_FLUSH()
#endif

#define SYSTEM_INIT_TASK_STACK_SIZE_WORDS   768u
#define SYSTEM_TASK_PRIORITY                1u

/* Private typedef -----------------------------------------------------------*/
/* Private define section 1 --------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/** The system task handle */
static TaskHandle_t m_sys_init_task;

/* Private functions prototypes ----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

void SD_EVT_IRQHandler(void)
{
  BaseType_t yield_req = pdFALSE;

  vTaskNotifyGiveFromISR(m_sys_init_task, &yield_req);

  /* Switch the task if required. */
  portYIELD_FROM_ISR(yield_req);
}

/**
 * @brief Thread for initilaise whole device and
 *        handling the Application's BLE Stack events.
 *
 * @param[in]   arg   Pointer used for passing some arbitrary information (context) from the
 *                    osThreadCreate() call to the thread.
 * @retval None.
 */
static void vSystemInitTask(void *arg)
{
  UNUSED_PARAMETER(arg);
  ret_code_t ret_code  = NRF_SUCCESS;

  /* Generate CRC8 table */
  osa_uCRC8_initialize();

  /* Initialize uart library */
  ret_code = bsp_uarte_init();
  VALIDATE_CRITIC((ret_code != NRF_SUCCESS), ret_code, "Failed to initialize UART lib!");

  /* Initialize timems library */
  ret_code = timems_initialize();
  VALIDATE_CRITIC((ret_code != NRF_SUCCESS), ret_code, "Failed to initialize timems lib!");

  /* Initialize Pub/Sub library */
  ret_code = pubsub_initialize();
  VALIDATE_CRITIC((ret_code != NRF_SUCCESS), ret_code, "Failed to initialize Pub/Sub lib!");


//  ret_code_t err_code;
//
//  err_code = nrf_sdh_enable_request();
//  APP_ERROR_CHECK(err_code);
//
//  // Configure the BLE stack using the default settings.
//  // Fetch the start address of the application RAM.
//  uint32_t ram_start = 0;
//  err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
//  APP_ERROR_CHECK(err_code);
//
//  // Enable BLE stack.
//  err_code = nrf_sdh_ble_enable(&ram_start);
//  APP_ERROR_CHECK(err_code);

  while (true)
  {
    uint8_t p_softdevice_enabled = 0;
    sd_softdevice_is_enabled(&p_softdevice_enabled);
    if (p_softdevice_enabled)
    {
      nrf_sdh_evts_poll();                    /* let the handlers run first, incase the EVENT occured before creating this task */

      (void) ulTaskNotifyTake(pdTRUE,         /* Clear the notification value before exiting (equivalent to the binary semaphore). */
                              portMAX_DELAY); /* Block indefinitely (INCLUDE_vTaskSuspend has to be enabled).*/
    }
    else
    {
      osa_TsleepMS(100);
      char pb[200] = { 0 };
      uint8_t pOutDataSize = 0;
      if (bsp_uarte_get(pb, &pOutDataSize, sizeof(pb)) == NRF_SUCCESS)
      {
        bsp_uarte_send(pb, pOutDataSize);
      }
    }
  }
}

/**
 * @brief Function for application main entry.
 *
 * @param None.
 * @retval None.
 */
void main(void)
{
  /* Pre initialization: clock */
  ret_code_t err_code = nrf_drv_clock_init();
  APP_ERROR_CHECK(err_code);

  /* Initialize the log. */
#if (NRF_LOG_ENABLED != 0)
  dl_initialize();
#endif

  /* Create initializer and SDH dispather task */
  if (pdPASS != xTaskCreate(vSystemInitTask,
                            "SYS",
                            SYSTEM_INIT_TASK_STACK_SIZE_WORDS,
                            NULL,
                            SYSTEM_TASK_PRIORITY,
                            &m_sys_init_task)
      )
  {
    APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
  }

  /* Activate deep sleep mode */
  SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

  /* Start FreeRTOS scheduler */
  vTaskStartScheduler();

  /* If all is OK, we never reach this statement */
  while (true)
  {
    APP_ERROR_HANDLER(NRF_ERROR_FORBIDDEN);
  }
}

/* Exported functions ------------------------------------------------------- */

/**
 * @brief Application Stack owerflow hook.
 *
 * @note This function call by FreeRTOS at stack oweflow.
 *
 * @param[in] xTask - pionter to task handle.
 * @param[in] pcTaskName - pionter to task name.
 * @retval None.
 */
void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName)
{
  UNUSED_PARAMETER(xTask);

  /* Create static local variable for save the name off task */
  static volatile const char * failedTaskName = NULL;
  failedTaskName = pcTaskName;
  UNUSED_VARIABLE(failedTaskName);

  UTIL_DBG_WARN("Stack overflow at %s", pcTaskName);
  VALIDATE_CRITIC(true, RET_CODE_ERROR_INTERNAL, "Stack overflow!");
  while(true)
  {
    // Just wait...
  }
}

/**
 * @brief Application malloc failed hook.
 *
 * @param  None.
 * @retval None.
 */
void vApplicationMallocFailedHook(void)
{
  /*   Called if a call to pvPortMalloc() fails because there is insufficient
   *  free memory available in the FreeRTOS heap.
   *   pvPortMalloc() is called internally by FreeRTOS API functions that create
   *  tasks, queues, software timers, and semaphores.
   *   The size of the FreeRTOS heap is set by the
   *  configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h.
   */

  UTIL_DBG_WARN("No free HEAP!");
  VALIDATE_CRITIC(true, RET_CODE_ERROR_INTERNAL, "Malloc failed!");
  while(true)
  {
    // Just wait...
  }
}

#ifdef DEBUG

/**
 * @brief Application HardFault exception hook.
 *
 * @param  None.
 * @retval None.
 */
void HardFault_process(HardFault_stack_t * p_stack)
{
  UNUSED_VARIABLE(p_stack);
  VALIDATE_CRITIC(true, RET_CODE_ERROR_INTERNAL, "HardFault exception!");
  while(true)
  {
    // Just wait...
  }
}

#endif
/*! ***************************************************************************/
/*!                           End of file                                     */
/*! ***************************************************************************/