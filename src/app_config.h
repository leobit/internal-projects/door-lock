/* Copyright (c) 2015 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

#ifndef APP_CONFIG_H
#define APP_CONFIG_H

/* Pragma */
#define DO_PRAGMA(x) _Pragma (#x)

/* Mesages */
#define PRAGMA_MESSAGE(x)  DO_PRAGMA(message (x))
#define VALUE_TO_STRING(x) #x
#define VAR_NAME_VALUE(var) #var " = " VALUE_TO_STRING(var)
#define __XLINEX__(x) VALUE_TO_STRING(x)
#define TODO(x) DO_PRAGMA(message ("TODO: " #x " => " __XLINEX__(__LINE__) " line  in \""__FILE__"\""))

#define MEMBER_SIZE(type, member) sizeof(((type *)0)->member)

#if defined ( __ICCARM__ )

#define NO_OPTIMIZE #pragma optimize=none

#elif defined   ( __GNUC__ )

#define NO_OPTIMIZE #pragma GCC optimize ("O1")

#else

#warning "Undefined compiler wersion!"

#endif

/* Default strings */
#ifdef RELEASE
  #define INFO_DEVICE_NAME                      "Dash "
#else
  #define INFO_DEVICE_NAME                      "Dash D "                           /**< Name of device. Will be included in the advertising data. */
#endif

#define INFO_MANUFACTURER_NAME                  "Stages Cycling"                   /**< Manufacturer. Will be passed to Device Information Service. */
#define INFO_FULL_FIRMWARE_VERSION              "0.1.0"
#define INFO_SERIAL_NUM                         "123456789012"
#define INFO_DEVICE_MAC                         "123456789012"
#define INFO_DEVICE_VER                         "LEO 2 dev"

/******************************** [ LIB configuration ] ***********************/
#define APP_TIMER_ENABLED					1
#define NRF_QUEUE_ENABLED					1

/******************************** [ BLE configuration ] ***********************/
#define NRF_SDH_BLE_PERIPHERAL_LINK_COUNT   1     /*!< Maximum number of peripheral links. */
#define NRF_SDH_BLE_CENTRAL_LINK_COUNT      6     /*!< Maximum number of central links. */
#define NRF_SDH_BLE_TOTAL_LINK_COUNT        1     /*!< Maximum number of total concurrent connections using the default configuration. */
#define NRF_SDH_BLE_GAP_EVENT_LENGTH        6     /*!< The time set aside for this connection on every connection interval in 1.25 ms units. */
#define NRF_SDH_BLE_GATT_MAX_MTU_SIZE       23    /*!< Static maximum MTU size. */
#define NRF_SDH_BLE_GATTS_ATTR_TAB_SIZE     0x300 /*!< Attribute Table size in bytes. The size must be a multiple of 4. */
#define NRF_SDH_BLE_VS_UUID_COUNT           8     /*!< The number of vendor-specific UUIDs.*/
#define NRF_SDH_BLE_SERVICE_CHANGED         1     /*!< Include the Service Changed characteristic in the Attribute Table. */
#define APP_BLE_CONN_CFG_TAG                1     /*!< A tag identifying the SoftDevice BLE configuration. */


#define BLE_DB_DISCOVERY_ENABLED            1
#define BLE_BAS_ENABLED	                    1
#define BLE_BAS_C_ENABLED                   1
#define BLE_DIS_ENABLED	                    1
#define BLE_HRS_C_ENABLED                   1
#define BLE_ADVERTISING_ENABLED             1
#define BLE_DIS_C_STRING_MAX_LEN            30
#define BLE_DIS_C_ENABLED                   1
#define BLE_DIS_C_QUEUE_SIZE                16
#define BLE_ANCS_C_ENABLED                  1

/*************************** [ SFDevice configuration ] ***********************/
#define NRF_SDH_ANT_OBSERVER_PRIO_LEVELS    4  /*!< Total number of priority levels for ANT observers. */
#define NRF_SDH_BLE_OBSERVER_PRIO_LEVELS    4 /*!< Total number of priority levels for BLE observers. */
#define NRF_SDH_DISPATCH_MODEL              NRF_SDH_DISPATCH_MODEL_POLLING /*!< SoftDevice events are to be fetched manually. */
#define NRF_SDH_ENABLED 		    1
#define NRF_SDH_BLE_ENABLED	            1
#define NRF_SDH_SOC_ENABLED 	            1
#define NRF_BLE_CONN_PARAMS_ENABLED         1
#define NRF_SECTION_ITER_ENABLED            1
#define NRF_FSTORAGE_ENABLED                1
#define NRF_BLE_GATT_ENABLED		    1
#define PEER_MANAGER_ENABLED                1
#define FDS_ENABLED                         1

/******************************** [ HAL configuration ] ***********************/
#define RTC_ENABLED                     1
#define TWI_ENABLED			1
#define PPI_ENABLED			1

#define TIMER_ENABLED 1

/******************************** [ LOG modules ] *****************************/

/* Set to 1u to enable OSA HEAP logging. */
#define ENABLE_OSAH_LOG  1u

/* Set to 1u to enable OSA layer logging. */
#define ENABLE_OSAL_LOG  1u

/* Set to 1u to enable main.c file logging. */
#define ENABLE_MAIN_LOG  1u


/* CLOCK */
#define CLOCK_ENABLED 1

/* GPIOTE */
#define GPIOTE_ENABLED 1

/* TIMER */
#define TIMER0_ENABLED 0  /* Used by SOFTDEVICE */
#define TIMER1_ENABLED 1
#define TIMER2_ENABLED 0
#define TIMER3_ENABLED 0
#define TIMER4_ENABLED 0

/* RTC */
#define RTC0_ENABLED 0  /* Used by SOFTDEVICE */
#define RTC1_ENABLED 0  /* Used by FreeRTOS */
#define RTC2_ENABLED 1

/* RNG */
#define RNG_ENABLED 0  /* Used by SOFTDEVICE */

/* PWM */
#define PWM0_ENABLED 1
#define PWM1_ENABLED 0
#define PWM2_ENABLED 0

/* SPI */
#define SPI0_ENABLED 0
#define SPI1_ENABLED 0
#define SPI2_ENABLED 0

/* SPIS */
#define SPIS0_ENABLED 0
#define SPIS1_ENABLED 0
#define SPIS2_ENABLED 0  /* Enable SPIS0 for NXP connection */

/* UART */
#define UART_ENABLED 1
#define UART0_ENABLED 1

#ifdef NRF52
#define UART0_CONFIG_USE_EASY_DMA true
#define UART_EASY_DMA_SUPPORT     1
#define UART_LEGACY_SUPPORT       1
#endif //NRF52

/* TWI */
#define TWI0_ENABLED 1
#define TWI1_ENABLED 1

/* TWIS */
#define TWIS0_ENABLED 0
#define TWIS1_ENABLED 0

/* For more documentation see nrf_drv_twis.h file */
#define TWIS_ASSUME_INIT_AFTER_RESET_ONLY 0

/* For more documentation see nrf_drv_twis.h file */
#define TWIS_NO_SYNC_MODE 0

/* QDEC */
#define QDEC_ENABLED 0

/* ADC */
#define ADC_ENABLED 0

/* SAADC */
#define SAADC_ENABLED 1

/* PDM */
#define PDM_ENABLED 0

/* COMP */
#define COMP_ENABLED 0

/* LPCOMP */
#define LPCOMP_ENABLED 0

/* WDT */
#define WDT_ENABLED 0

/* SWI EGU */
#ifdef NRF52
    #define EGU_ENABLED 0
#endif

/* I2S */
#define I2S_ENABLED 0

// <0=> RC
// <1=> XTAL
// <2=> Synth
//#define CLOCK_CONFIG_LF_SRC 0 // TODO: change this to XTAL when DEV kit arrive.
//#define NRF_SDH_CLOCK_LF_SRC 0 // TODO: change this to XTAL when DEV kit arrive.
//#define NRF_SDH_CLOCK_LF_RC_TEMP_CTIV 4
//#define NRF_SDH_CLOCK_LF_RC_CTIV 16

#define NRF_SDH_CLOCK_LF_XTAL_ACCURACY 1
#define CLOCK_CONFIG_LF_SRC 1

/* If debug: define debug symbols */
#ifdef DEBUG

#define NRF_SECTION_ITER_ENABLED    1
#define NRF_LOG_ENABLED             1
#define NRF_LOG_BACKEND_RTT_ENABLED 1
#define NRF_BALLOC_ENABLED          1
#define NRF_CLI_ENABLED             1
#define POWER_ENABLED               1
#define NRF_CLI_ECHO_STATUS         1
#define NRF_FPRINTF_ENABLED         1
#define NRF_STRERROR_ENABLED        1
#define HARDFAULT_HANDLER_ENABLED   1

#endif

#endif // APP_CONFIG_H
